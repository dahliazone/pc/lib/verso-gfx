TEMPLATE = subdirs
win* {
        TEMPLATE = subdirs
}
CONFIG += ordered

libverso-base.subdir = ../verso-base
libverso-base.target = libverso-base-dep

libverso-gfx.file = src/verso-gfx-src.pro
libverso-gfx.target = libverso-gfx-dep
libverso-gfx.depends = libverso-base-dep

tests_libverso-gfx.file = test/verso-gfx-test.pro
tests_libverso-gfx.target = tests_libverso-gfx-dep
tests_libverso-gfx.depends = libverso-gfx-dep

examples_libverso-gfx.file = examples/verso-gfx-examples.pro
examples_libverso-gfx.depends = libverso-gfx-dep tests_libverso-gfx-dep

SUBDIRS = libverso-base libverso-gfx tests_libverso-gfx examples_libverso-gfx
