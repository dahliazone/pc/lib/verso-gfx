#pragma once

// TODO:

#include <Verso/System/UString.hpp>

namespace Verso {
namespace priv {


class MessageDialogWin32
{
public:
	static void run(const UString& title, const UString& message);
};


} // End namespace priv
} // End namespace Verso
