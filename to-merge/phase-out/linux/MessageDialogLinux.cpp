char MessageDialogLinux_symbol = 0;

#ifdef __linux__

#include <Verso/Display/MessageDialog.hpp>
#include <Verso/Display/linux/MessageDialogLinux.hpp>
#include <Verso/System/Logger.hpp>
#include <iostream>

namespace Verso {
namespace priv {


void MessageDialogLinux::run(const UString& title, const UString& message)
{
	VERSO_LOG_INFO("verso-gfx", title.c_str());
	VERSO_LOG_INFO_VARIABLE("verso-gfx", message.c_str(), "");
	VERSO_LOG_FLUSH();
}


} // End namespace priv
} // End namespace Verso


#endif // End #ifdef __linux__

