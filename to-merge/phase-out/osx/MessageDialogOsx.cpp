char MessageDialogOsx_symbol = 0;

#if __APPLE__ && __MACH__

#include <Verso/Display/MessageDialog.hpp>
#include <Verso/Display/osx/MessageDialogOsx.hpp>
#include <CoreFoundation/CoreFoundation.h>
#include <objc/objc.h>
#include <objc/objc-runtime.h>
#include <iostream>


extern "C" int NSRunAlertPanel(CFStringRef strTitle, CFStringRef strMsg,
                           CFStringRef strButton1, CFStringRef strButton2,
                           CFStringRef strButton3, ...);


namespace Verso {
namespace priv {


void MessageDialogOsx::run(const UString& title, const UString& message)
{
	id app = NULL;
	id pool = reinterpret_cast<id>(objc_getClass("NSAutoreleasePool"));
	if (!pool)
	{
		std::cout << "MessageDialogOsx::run(): Cannot show error message dialog! Unable to get NSAutoreleasePool!\nAborting\n";
		std::cout << title.c_str()<<":" << std::endl
		          << message.c_str() << std::endl;
		return;// -1;
	}

	pool = objc_msgSend(pool, sel_registerName("alloc"));
	if (!pool)
	{
		std::cout << "MessageDialogOsx::run(): Cannot show error message dialog! Unable to create NSAutoreleasePool...\nAborting...\n";
		std::cout << title.c_str()<<":" << std::endl
		          << message.c_str() << std::endl;
		return;// -1;
	}
	pool = objc_msgSend(pool, sel_registerName("init"));

	app = reinterpret_cast<id>(objc_msgSend(reinterpret_cast<id>(objc_getClass("NSApplication")),
	                   sel_registerName("sharedApplication")));

	//CFStringRef CFStringCreateWithBytes(CFAllocatorRef alloc, const UInt8 *bytes, CFIndex numBytes, CFStringEncoding encoding, Boolean isExternalRepresentation);
	//CFStringCreateWithBytes();

	char* titleBytes = reinterpret_cast<char*>(CFAllocatorAllocate(CFAllocatorGetDefault(), title.size(), 0));
	strcpy(titleBytes, title.c_str());
	CFStringRef titleStr = CFStringCreateWithCStringNoCopy(NULL, titleBytes,
	                                           kCFStringEncodingMacRoman, NULL);

	char* messageBytes = reinterpret_cast<char*>(CFAllocatorAllocate(CFAllocatorGetDefault(), message.size(), 0));
	strcpy(messageBytes, message.c_str());
	CFStringRef messageStr = CFStringCreateWithCStringNoCopy(NULL, messageBytes,
	                                           kCFStringEncodingMacRoman, NULL);

	NSRunAlertPanel(titleStr,
	                messageStr,
	                CFSTR("OK"), NULL, NULL);

	CFRelease(titleStr); /* default allocator also frees bytes */
	CFRelease(messageStr); /* default allocator also frees bytes */

	objc_msgSend(pool, sel_registerName("release"));
}


} // End namespace priv
} // End namespace Verso


#endif // End #ifdef __APPLE__ && __MACH__

