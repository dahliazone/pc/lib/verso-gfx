#include <Verso/Display/MessageDialog.hpp>

#ifdef _MSC_VER
    #include <Verso/Display/win32/MessageDialogWin32.hpp>

#elif __linux__
    #include <Verso/Display/linux/MessageDialogLinux.hpp>

#elif __APPLE__ && __MACH__
    #include <Verso/Display/osx/MessageDialogOsx.hpp>
#endif

namespace Verso {


void MessageDialog::run(const UString& title, const UString& message)
{
#ifdef _MSC_VER
	priv::MessageDialogWin32::run(title, message);

#elif __linux__
	priv::MessageDialogLinux::run(title, message);

#elif __APPLE__ && __MACH__
	priv::MessageDialogOsx::run(title, message);

#else
	UString error("Verso::MessageDialog::run(): Unknown platform!");
	std::cout << error.c_str() << std::endl;
	throw std::runtime_error(error);
#endif
}


} // End namespace Verso

