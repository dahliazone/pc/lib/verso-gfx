#include <Verso/Audio/Audio2d.hpp>
#include <Verso/Audio/Audio2dBass.hpp>
#include <Verso/Display/Display.hpp>
#include <Verso/Display/DisplayMode.hpp>
#include <Verso/Display/IWindow.hpp>
#include <Verso/Display/MsgBox.hpp>
#include <Verso/Display/PixelFormat.hpp>
#include <Verso/Display/SwapInterval.hpp>
#include <Verso/Display/Window2d.hpp>
#include <Verso/Display/WindowStyle.hpp>
#include <Verso/Input/GameController/GameControllerAxis.hpp>
#include <Verso/Input/GameController/GameControllerButton.hpp>
#include <Verso/Input/GameController/GameControllerEvent.hpp>
#include <Verso/Input/GameController/GameControllerEventType.hpp>
#include <Verso/Input/Joystick/JoystickAxis.hpp>
#include <Verso/Input/Joystick/JoystickButton.hpp>
#include <Verso/Input/Joystick/JoystickEvent.hpp>
#include <Verso/Input/Joystick/JoystickEventType.hpp>
#include <Verso/Input/Joystick/JoystickHat.hpp>
#include <Verso/Input/Joystick/JoystickHatDirection.hpp>
#include <Verso/Input/Joystick/JoystickTrackball.hpp>
#include <Verso/Input/Keyboard/KeyboardEvent.hpp>
#include <Verso/Input/Keyboard/KeyboardEventType.hpp>
#include <Verso/Input/Keyboard/KeyCodeConverterSdl2.hpp>
#include <Verso/Input/Keyboard/KeyCode.hpp>
#include <Verso/Input/Keyboard/Keymodifier.hpp>
#include <Verso/Input/Keyboard/KeyScancode.hpp>
#include <Verso/Input/Keyboard/KeySymbol.hpp>
#include <Verso/Input/Mouse/MouseAxis.hpp>
#include <Verso/Input/Mouse/MouseButton.hpp>
#include <Verso/Input/Mouse/MouseEvent.hpp>
#include <Verso/Input/Mouse/MouseEventType.hpp>
#include <Verso/Input/AxisState.hpp>
#include <Verso/Input/ButtonState.hpp>
#include <Verso/Input/Event.hpp>
#include <Verso/Input/InputController.hpp>
#include <Verso/Input/EventType.hpp>
#include <Verso/Input/InputManager.hpp>
#include <Verso/Input/InputNormalizer.hpp>
#include <Verso/Input/WeightedButtonState.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>
#include <Verso/System/IProgram.hpp>
#include <Verso/System/main.hpp>
#include <Verso/Time/Timer.hpp>
#include <Verso/Time/FrameTimer.hpp>
#include <Verso/Time/FrameTimestamp.hpp>
#include <Verso/Time/Timestamp.hpp>
#include <Verso/Audio.hpp>
#include <Verso/Display.hpp>
#include <Verso/Input.hpp>
#include <Verso/System.hpp>
#include <Verso/Gfx.hpp>


using namespace std;
using namespace Verso;


void versoGfxSimpleTestsDisplay()
{
	cout << "//// verso-gfx/Display /////////////////////////////////////////" << endl;

	Display display;
	cout << "Display = "<<display.toString() << endl;

	DisplayMode displayMode;
	cout << "DisplayMode = "<<displayMode << endl;
}


void versoGfxSimpleTests()
{
	cout << "//// RUNNING TEST FOR VERSO-GFX ////////////////////////////////" << endl << endl;

	versoGfxSimpleTestsDisplay();

	cout << "//// ALL TESTS DONE FOR VERSO-GFX //////////////////////////////" << endl << endl;
}

