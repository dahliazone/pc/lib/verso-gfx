#pragma once

// \TODO: check through code to match Verso standards!!!

/*
Links:
============================================================================
=== Keyboard ===============================================================
- Handling text input
  - https://wiki.libsdl.org/SDL_StartTextInput
  - https://wiki.libsdl.org/SDL_StopTextInput
  - https://wiki.libsdl.org/SDL_SetTextInputRect

============================================================================
=== Game controller ========================================================
- Enable / query / disable game controller events
  - https://wiki.libsdl.org/SDL_GameControllerEventState
- Add/remove game controller
  - https://wiki.libsdl.org/SDL_ControllerDeviceEvent

============================================================================
=== Joystick ===============================================================

============================================================================
=== Mouse ==================================================================
- get/set relative / non-relative mouse mode
  - https://wiki.libsdl.org/SDL_GetRelativeMouseMode
  - https://wiki.libsdl.org/SDL_SetRelativeMouseMode

- State in non-relative mouse mode
  - https://wiki.libsdl.org/SDL_GetMouseState

- State in relative mouse mode (only relative motion events delivered)
  - https://wiki.libsdl.org/SDL_GetRelativeMouseState

- get window that has mouse focus
  - https://wiki.libsdl.org/SDL_GetMouseFocus

- Move the mouse to certain location
  - https://wiki.libsdl.org/SDL_WarpMouseInWindow
  - https://wiki.libsdl.org/SDL_WarpMouseGlobal

- Mouse system cursor control:
  - https://wiki.libsdl.org/SDL_ShowCursor
  - https://wiki.libsdl.org/SDL_GetDefaultCursor
  - https://wiki.libsdl.org/SDL_GetCursor
  - https://wiki.libsdl.org/SDL_CreateSystemCursor
  - https://wiki.libsdl.org/SDL_CreateCursor
  - https://wiki.libsdl.org/SDL_CreateColorCursor
  - https://wiki.libsdl.org/SDL_SetCursor
  - https://wiki.libsdl.org/SDL_FreeCursor
============================================================================
*/

#include <Verso/Input/InputController.hpp>
#include <Verso/Input/InputController2.hpp>
#include <Verso/Input/InputNormalizer.hpp>

namespace Verso {


class Display;


class InputManager
{
private:
	bool created;
	::std::map<UString, InputController*> inputControllers;
	::std::map<UString, InputController2*> inputControllers2;
	::std::vector<SDL_Joystick*> sdlJoysticks;
	::std::vector<SDL_GameController*> sdlGameControllers;
	//vector<Joystick*> m
	InputNormalizer<Sint32> mouseXNormalizer;
	InputNormalizer<Sint32> mouseYNormalizer;
	InputNormalizer<Sint32> mouseMotionXNormalizer;
	InputNormalizer<Sint32> mouseMotionYNormalizer;
	InputNormalizer<Sint32> mouseWheelMotionXNormalizer;
	InputNormalizer<Sint32> mouseWheelMotionYNormalizer;
	InputNormalizer<Sint16> gameControllerNormalizer;
	InputNormalizer<Sint16> joystickAxisNormalizer;
	InputNormalizer<Sint16> joystickTrackballNormalizer;

private:
	InputManager();

	InputManager(const InputManager& original) = delete;

	InputManager(InputManager&& original) = delete;

	InputManager& operator =(const InputManager& original) = delete;

	InputManager& operator =(InputManager&& original) = delete;

public:
	VERSO_GFX_API static InputManager& instance();

	VERSO_GFX_API ~InputManager();

	VERSO_GFX_API void create();

	VERSO_GFX_API void destroy() VERSO_NOEXCEPT;

	VERSO_GFX_API bool isCreated() const;

	// (could be) Interface: InputManager

	//VERSO_GFX_API void updateDisplayResolution(const AppWindow* window) // \TODO: remove or implement

	VERSO_GFX_API void enableJoysticksAndGameControllers();

	VERSO_GFX_API void disableJoysticks();

	VERSO_GFX_API void disableGameControllers();

	VERSO_GFX_API void enableMidi();

	VERSO_GFX_API void disableMidi();

	VERSO_GFX_API size_t getGameControllerCount() const;

	VERSO_GFX_API size_t getNonGameControllerCount() const;

	VERSO_GFX_API UString getJoystickInfo() const;

	// Note, getMouseState() will not update the windowId, and x,y are relative to currently active window
	VERSO_GFX_API MouseState getMouseState() const;

	VERSO_GFX_API size_t getMouseActiveDisplayIndex(const std::vector<Display>& displays) const;

	VERSO_GFX_API InputController* createInputController(const UString& name);

	VERSO_GFX_API InputController* getInputController(const UString& name);

	VERSO_GFX_API InputController2* createInputController2(const UString& name);

	VERSO_GFX_API InputController2* getInputController2(const UString& name);

	VERSO_GFX_API bool pollEvent(Event& event);

	VERSO_GFX_API bool waitEvent(Event& event);

	VERSO_GFX_API void broadcastEvent(const Event& event);

	// Implementation specific
	VERSO_GFX_API bool processSdlEvent(const SDL_Event& sdlEvent, Event& event);

public: // static
	VERSO_GFX_API static KeySymbol sdlToKeySymbol(const SDL_Keysym& sdlKeySym);

	VERSO_GFX_API static ButtonState convertSdlToButtonState(Uint8 state);

	VERSO_GFX_API static MouseButton convertSdlToMouseButton(Uint8 button);

	VERSO_GFX_API static ButtonState convertSdlToMouseButtonState(uint32_t sdlButtonState, uint8_t sdlButton);

	VERSO_GFX_API void updateSdlMouseInfoToEvent(MouseEvent& mouse) const;

	VERSO_GFX_API static GameControllerAxis convertSdlToGameControllerAxis(Uint8 axis);

	VERSO_GFX_API static GameControllerButton convertSdlToGameControllerButton(Uint8 button);

	VERSO_GFX_API static JoystickAxis convertSdlToJoystickAxis(Uint8 axis);

	VERSO_GFX_API static JoystickTrackball convertSdlToJoystickTrackball(Uint8 trackball);

	VERSO_GFX_API static JoystickHat convertSdlToJoystickHat(Uint8 hat);

	VERSO_GFX_API static JoystickHatDirection convertSdlToJoystickHatDirection(Uint8 hat);

	VERSO_GFX_API static JoystickButton convertSdlToJoystickButton(Uint8 button);
};


} // End namespace Verso
