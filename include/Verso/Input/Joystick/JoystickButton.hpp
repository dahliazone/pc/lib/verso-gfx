#pragma once

#include <Verso/System/UString.hpp>
#include <ostream>

namespace Verso {


enum class JoystickButton : int8_t
{
	Button1,
	Button2,
	Button3,
	Button4,
	Button5,
	Button6,
	Button7,
	Button8,
	Button9,
	Button10,
	Button11,
	Button12,
	Button13,
	Button14,
	Button15,
	Button16,
	Button17,
	Button18,
	Button19,
	Button20,
	Button21,
	Button22,
	Button23,
	Button24,
	Button25,
	Button26,
	Button27,
	Button28,
	Button29,
	Button30,
	Size,
	Unset
};


inline UString joystickButtonToString(const JoystickButton& button)
{
	if (button >= JoystickButton::Button1 && button < JoystickButton::Size) {
		UString s = "Button ";
		s.append2(static_cast<int>(button) + 1);
		return s;
	}
	else if (button == JoystickButton::Size) {
		return "Size";
	}
	else if (button == JoystickButton::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


inline std::ostream& operator <<(std::ostream& ost, const JoystickButton& right)
{
	return ost << joystickButtonToString(right);
}


} // End namespace Verso
