#pragma once

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Input/Joystick/JoystickAxis.hpp>
#include <Verso/Input/Joystick/JoystickButton.hpp>
#include <Verso/Input/Joystick/JoystickEventType.hpp>
#include <Verso/Input/Joystick/JoystickHat.hpp>
#include <Verso/Input/Joystick/JoystickHatDirection.hpp>
#include <Verso/Input/Joystick/JoystickTrackball.hpp>
#include <Verso/Input/AxisState.hpp>
#include <Verso/Input/ButtonState.hpp>

namespace Verso {


struct JoystickEvent
{
	JoystickEventType type;
	uint32_t timestamp;
	int32_t joystickId;

	// Only defined for type == AxisMotion
	JoystickAxis axis;
	AxisState value; // the axis value (range: -1 to 1) // \TODO: int16_t value (range: -32768 to 32767)

	// Only defined for type == TrackballMotion
	JoystickTrackball trackball;
	AxisState xRel; // the relative motion in the X direction // \TODO: int16_t
	AxisState yRel; // the relative motion in the Y direction // TOOD: int16_t

	// Only defined for type == HATMotion
	JoystickHat hat;
	JoystickHatDirection hatDirection;

	// Only defined for type == Button
	JoystickButton button;
	ButtonState state;

public: // toString
	UString toString() const
	{
		UString s = joystickEventTypeToString(type)+", timestamp=";
		s.append2(timestamp);
		s += ", joystickId=";
		s.append2(joystickId);

		if (type == JoystickEventType::AxisMotion) {
			s += ", \""+joystickAxisToString(axis);
			s += "\"=";
			s.append2(value);
		}

		else if (type == JoystickEventType::TrackballMotion) {
			s += ", \""+joystickTrackballToString(trackball);
			s += "\"=(";
			s.append2(xRel);
			s += ", ";
			s.append2(yRel);
			s += ")";
		}

		else if (type == JoystickEventType::HatMotion) {
			s += ", \""+joystickHatToString(hat);
			s += "\"=";
			s += joystickHatDirectionToString(hatDirection);
		}

		else if (type == JoystickEventType::Button) {
			s += ", button="+joystickButtonToString(button)+", state="+buttonStateToString(state);
		}
		return s;
	}


	UString toStringDebug() const
	{
		UString str("JoystickEvent(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const JoystickEvent& right)
	{
		return ost << right.toStringDebug();
	}
};


} // End namespace Verso
