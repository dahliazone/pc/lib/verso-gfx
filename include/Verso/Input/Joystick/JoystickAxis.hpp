#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <ostream>

namespace Verso {


enum class JoystickAxis : int8_t
{
	Axis1 = 0,
	Axis2,
	Axis3,
	Axis4,
	Axis5,
	Axis6,
	Axis7,
	Axis8,
	Axis9,
	Axis10,
	Axis11,
	Axis12,
	Size,
	Unset
};


inline UString joystickAxisToString(const JoystickAxis& axis)
{
	if (axis >= JoystickAxis::Axis1 && axis < JoystickAxis::Size) {
		UString s = "Axis ";
		s.append2(static_cast<int>(axis));
		return s;
	}
	else if (axis == JoystickAxis::Size) {
		return "Size";
	}
	else if (axis == JoystickAxis::Unset) {
		return "Unset";
	}
	else {
		UString error("Invalid JoystickAxis(");
		error.append2(static_cast<int8_t>(axis));
		error += ") given.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}
}


inline std::ostream& operator <<(std::ostream& ost, const JoystickAxis& right)
{
	return ost << joystickAxisToString(right);
}


} // End namespace Verso
