#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <ostream>

namespace Verso {


enum class GameControllerAxis : int8_t
{
	Unset = 0,
	LeftX,
	LeftY,
	RightX,
	RightY,
	TriggerLeft,
	TriggerRight,
	Size
};


inline UString gameControllerAxisToString(const GameControllerAxis& axis)
{
	switch (axis) {
	case GameControllerAxis::Unset:
		return "Unset";
	case GameControllerAxis::LeftX:
		return "LeftX";
	case GameControllerAxis::LeftY:
		return "LeftY";
	case GameControllerAxis::RightX:
		return "RightX";
	case GameControllerAxis::RightY:
		return "RightY";
	case GameControllerAxis::TriggerLeft:
		return "TriggerLeft";
	case GameControllerAxis::TriggerRight:
		return "TriggerRight";
	case GameControllerAxis::Size:
		return "Size";
	default:
	{
		UString error("Invalid GameControllerAxis(");
		error.append2(static_cast<int>(axis));
		error += ") given.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}
	}
}


inline std::ostream& operator <<(std::ostream& ost, const GameControllerAxis& right)
{
	return ost << gameControllerAxisToString(right);
}


} // End namespace Verso
