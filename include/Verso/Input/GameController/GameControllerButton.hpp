#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <ostream>

namespace Verso {


enum class GameControllerButton : int8_t
{
	A,
	B,
	X,
	Y,
	Back,
	Guide,
	Start,
	LeftStick,
	RightStick,
	LeftShoulder,
	RightShoulder,
	DpadUp,
	DpadDown,
	DpadLeft,
	DpadRight,
	Size,
	Unset
};


inline UString gameControllerButtonToString(const GameControllerButton& button)
{
	switch (button) {
	case GameControllerButton::A:
		return "A";
	case GameControllerButton::B:
		return "B";
	case GameControllerButton::X:
		return "X";
	case GameControllerButton::Y:
		return "Y";
	case GameControllerButton::Back:
		return "Back";
	case GameControllerButton::Guide:
		return "Guide";
	case GameControllerButton::Start:
		return "Start";
	case GameControllerButton::LeftStick:
		return "Left Stick";
	case GameControllerButton::RightStick:
		return "Right Stick";
	case GameControllerButton::LeftShoulder:
		return "Left Shoulder";
	case GameControllerButton::RightShoulder:
		return "Right Shoulder";
	case GameControllerButton::DpadUp:
		return "Dpad Up";
	case GameControllerButton::DpadDown:
		return "Dpad Down";
	case GameControllerButton::DpadLeft:
		return "Dpad Left";
	case GameControllerButton::DpadRight:
		return "Dpad Right";
	case GameControllerButton::Size:
		return "Size";
	case GameControllerButton::Unset:
		return "Unset";
	default:
	{
		UString error("Invalid GameControllerButton(");
		error.append2(static_cast<int>(button));
		error += ") given.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}
	}
}


inline std::ostream& operator <<(std::ostream& ost, const GameControllerButton& right)
{
	return ost << gameControllerButtonToString(right);
}


} // End namespace Verso
