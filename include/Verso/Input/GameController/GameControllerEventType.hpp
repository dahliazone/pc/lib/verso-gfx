#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <ostream>

namespace Verso {


enum class GameControllerEventType : int8_t
{
	Unset = 0,
	AxisMotion,
	Button,
	Size,
};


inline UString gameControllerEventTypeToString(const GameControllerEventType& eventType)
{
	switch (eventType)
	{
	case GameControllerEventType::Unset:
		return "Unset";
	case GameControllerEventType::AxisMotion:
		return "AxisMotion";
	case GameControllerEventType::Button:
		return "Button";
	case GameControllerEventType::Size:
		return "Size";
	default:
	{
		UString error("Invalid GameControllerEventType(");
		error.append2(static_cast<int>(eventType));
		error += ") given.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}
	}
}


inline std::ostream& operator <<(std::ostream& ost, const GameControllerEventType& right)
{
	return ost << gameControllerEventTypeToString(right);
}


} // End namespace Verso
