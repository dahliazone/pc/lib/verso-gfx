#pragma once

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Input/Mouse/MouseAxis.hpp>
#include <Verso/Input/Mouse/MouseButton.hpp>
#include <Verso/Input/Mouse/MouseEventType.hpp>
#include <Verso/Input/AxisState.hpp>
#include <Verso/Input/ButtonState.hpp>

namespace Verso {


struct MouseEvent
{
	// For all types except None
	MouseEventType type;
	uint32_t timestamp;
	uint32_t windowId;
	uint32_t mouseId;
	int32_t x, y; // mouse location relative to window
	float xRel, yRel; // mouse location relative to window in relative coordinates [0..1]
	ButtonState mouseButtons[static_cast<size_t>(MouseButton::Size)];

	// Only defined for type == Motion
	int32_t motionX, motionY; // mouse relative movement
	AxisState motionXRel, motionYRel; // mouse relative movement in relative coordinates [0..1]

	// Only defined for type == Button
	MouseButton changedButton;
	ButtonState state;
	uint8_t clicks; // 1 for single-click, 2 for double-click, etc.

	// Only defined for type == Wheel
	int32_t wheelMotionX, wheelMotionY; // scrolled amount with mouse wheel (usually -1 or 1)
	float wheelMotionPreciseX, wheelMotionPreciseY; // scrolled amount with mouse wheel as float
	AxisState wheelMotionXRel, wheelMotionYRel; // scrolled amount with mouse wheel in relative coordinates [0..1]

public: // toString
	UString toString() const
	{
		UString s = "type="+mouseEventTypeToString(type)+", timestamp=";
		s.append2(timestamp);
		s += ", windowId=";
		s.append2(windowId);
		s += ", mouseId=";
		s.append2(mouseId);
		s += ", xRel=";
		s.append2(xRel);
		s += ", yRel=";
		s.append2(yRel);

		s += ", mouseButtons[ ";
		bool first = true;
		for (int8_t i = 0; i < static_cast<int8_t>(MouseButton::Size); ++i) {
			if (mouseButtons[i] == ButtonState::Pressed) {
				if (first == true) {
					first = false;
				} else {
					s += ", ";
				}
				s += mouseButtonToString(intToMouseButton(i));
			}
		}
		if (first == true)
			s += "None";
		s += " ]";

		if (type == MouseEventType::Motion) {
			s += ", motionRel(";
			s.append2(motionXRel);
			s += ", ";
			s.append2(motionYRel);
			s += ")";
		}

		else if (type == MouseEventType::Button) {
			s += ", changedButton="+mouseButtonToString(changedButton)+", state="+buttonStateToString(state)+", clicks=";
			s.append2(static_cast<unsigned int>(clicks));
		}

		else if (type == MouseEventType::Wheel) {
			s += ", wheelMotion(";
			s.append2(wheelMotionX);
			s += ", ";
			s.append2(wheelMotionY);
			s += ")";
			s += ", wheelMotionPrecise(";
			s.append2(wheelMotionPreciseX);
			s += ", ";
			s.append2(wheelMotionPreciseY);
			s += ")";
			s += ", wheelMotionRel=(";
			s.append2(wheelMotionXRel);
			s += ", ";
			s.append2(wheelMotionYRel);
			s += ")";
		}

		return s;
	}


	UString toStringDebug() const
	{
		UString str("MouseEvent(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const MouseEvent& right)
	{
		return ost << right.toStringDebug();
	}
};


} // End namespace Verso
