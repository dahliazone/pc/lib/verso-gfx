#pragma once

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Input/Mouse/MouseAxis.hpp>
#include <Verso/Input/Mouse/MouseButton.hpp>
#include <Verso/Input/ButtonState.hpp>

namespace Verso {


struct MouseState
{
	uint32_t windowId;
	int32_t x, y; // mouse location relative to window
	int32_t desktopX, desktopY; // mouse location relative to desktop
	ButtonState buttons[static_cast<size_t>(MouseButton::Size)];

public: // toString
	UString toString() const
	{
		UString s = "windowId=";
		s.append2(windowId);
		s += ", x=";
		s.append2(x);
		s += ", y=";
		s.append2(y);
		s += ", desktopX=";
		s.append2(desktopX);
		s += ", desktopY=";
		s.append2(desktopY);

		s += ", buttons[ ";
		bool first = true;
		for (int8_t i = 0; i < static_cast<int8_t>(MouseButton::Size); ++i) {
			if (buttons[i] == ButtonState::Pressed) {
				if (first == true) {
					first = false;
				} else {
					s += ", ";
				}
				s += mouseButtonToString(intToMouseButton(i));
			}
		}
		if (first == true)
			s += "None";
		s += " ]";

		return s;
	}


	UString toStringDebug() const
	{
		UString str("MouseState(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const MouseState& right)
	{
		return ost << right.toStringDebug();
	}
};


} // End namespace Verso
