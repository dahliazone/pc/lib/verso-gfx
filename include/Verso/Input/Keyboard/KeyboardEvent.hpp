#pragma once

#include <Verso/Input/Keyboard/KeyboardEventType.hpp>
#include <Verso/Input/Keyboard/KeySymbol.hpp>
#include <Verso/Input/Keyboard/KeyCodeConverterSdl2.hpp>
#include <Verso/Input/ButtonState.hpp>

namespace Verso {


struct KeyboardEvent
{
	// For all type except None
	KeyboardEventType type;
	uint32_t timestamp;
	uint32_t windowId;

	// Only defined for type == Key
	KeySymbol key;
	ButtonState state;
	uint8_t repeat; // non-zero if this is a key repeat

	// Only defined for type == TextEditing
	int32_t start; // the location to begin editing from
	int32_t length; // the number of characters to edit from the start point

	// Only defined for type == TextInput
	// none

	// Only defined for type == TextEditing or TextInput
	UString text; // the null-terminated editing text in UTF-8 encoding

public:
	KeyboardEvent() :
		type(KeyboardEventType::Unset),
		timestamp(0),
		windowId(0),
		key(),
		state(ButtonState::Unset),
		repeat(0),
		start(0),
		length(0),
		text()
	{
	}

public: // toString
	UString toString() const
	{
		UString s = "type="+keyboardEventTypeToString(type);
		s += ", timestamp=";
		s.append2(timestamp);
		s += ", windowId=";
		s.append2(windowId);

		if (type == KeyboardEventType::Key) {
			s += ", keyCode="+keyCodeToString(key.keyCode);
			s += ", keymodifiers="+keymodifiersToString(key.keymodifiers);
			s += ", scancode="+keyScancodeToString(key.scancode);
			s += ", state="+buttonStateToString(state);
			s += ", repeat=";
			s.append2(static_cast<uint32_t>(repeat));
		}
		else if (type == KeyboardEventType::TextEditing) {
			s += ", text=\""+text+"\", start=";
			s.append2(start);
			s += ", length=";
			s.append2(length);
		}
		else if (type == KeyboardEventType::TextInput) {
			s += ", text=\""+text+"\"";
		}
		return s;
	}


	UString toStringDebug() const
	{
		UString str("KeyboardEvent(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const KeyboardEvent& right)
	{
		return ost << right.toStringDebug();
	}
};


} // End namespace Verso
