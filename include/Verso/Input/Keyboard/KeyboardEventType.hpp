#pragma once

#include <Verso/System/UString.hpp>
#include <ostream>
#include <cstdint>

namespace Verso {


enum class KeyboardEventType : int8_t
{
	Unset = 0,
	Key,
	TextEditing,
	TextInput,
	Size,
};


inline UString keyboardEventTypeToString(const KeyboardEventType& eventType)
{
	if (eventType == KeyboardEventType::Unset) {
		return "Unset";
	}
	else if (eventType == KeyboardEventType::Key) {
		return "Key";
	}
	else if (eventType == KeyboardEventType::TextEditing) {
		return "TextEditing";
	}
	else if (eventType == KeyboardEventType::TextInput) {
		return "TextInput";
	}
	else if (eventType == KeyboardEventType::Size) {
		return "Size";
	}
	else {
		return "Unknown value";
	}
}


inline std::ostream& operator <<(std::ostream& ost, const KeyboardEventType& right)
{
	return ost << keyboardEventTypeToString(right);
}


} // End namespace Verso
