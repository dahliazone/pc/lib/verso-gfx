#pragma once

#include <Verso/Input/Keyboard/KeyCode.hpp>
#include <Verso/Input/Keyboard/KeyScancode.hpp>
#include <Verso/Input/Keyboard/Keymodifier.hpp>
#include <map>

namespace Verso {


// \TODO: https://wiki.libsdl.org/SDL_GetKeyFromScancode

// https://wiki.libsdl.org/SDL_GetKeyboardFocus
// https://wiki.libsdl.org/SDL_GetKeyboardState
// https://wiki.libsdl.org/SDL_GetModState
// https://wiki.libsdl.org/SDL_SetModState
// https://wiki.libsdl.org/SDL_IsTextInputActive
// https://wiki.libsdl.org/SDL_SetTextInputRect
// https://wiki.libsdl.org/SDL_StartTextInput
// https://wiki.libsdl.org/SDL_StopTextInput


class KeyCodeConverterSdl2
{
private:
	KeyCodeConverterSdl2();

public:
	VERSO_GFX_API static KeyCodeConverterSdl2& instance();

	VERSO_GFX_API ~KeyCodeConverterSdl2();

	VERSO_GFX_API KeyCode sdlToVersoKeyCode(SDL_Keycode sdlKeycode);
	VERSO_GFX_API KeyScancode sdlToVersoKeyScancode(SDL_Scancode sdlScancode);
	VERSO_GFX_API Keymodifiers sdlToVersoKeymodifiers(Uint16 sdlKeymod);

	VERSO_GFX_API KeyCode nameToKeyCode(const UString& name);
	VERSO_GFX_API UString keyCodeToName(KeyCode key);

	VERSO_GFX_API Keymodifier nameToKeymodifier(const UString& name);
	VERSO_GFX_API UString keymodifierToName(Keymodifier keymodifier);

	VERSO_GFX_API KeyScancode nameToKeyScancode(const UString& name);
	VERSO_GFX_API UString keyScancodeToName(KeyScancode keyScancode);

private:
	ButtonState convertSdlToKeyModifierButtonState(Uint16 sdlKeymod, Uint16 sdlKeymodToTest);

	void createSdlToVersoKeyCodesMap();
	void createSdlToVersoKeymodifiersMap();
	void createSdlToVersoKeyScancodesMap();

	void createNamesToKeyCodesMap();
	void createKeyCodesToNamesMap();

	void createNamesToKeymodifierMap();
	void createKeymodifierToNamesMap();

	void createNamesToKeyScancodeMap();
	void createKeyScancodeToNamesMap();

private:
	std::map<SDL_Keycode, KeyCode> sdlToVersoKeyCodesMap;
	std::map<SDL_Keymod, Keymodifier> sdlToVersoKeymodifiersMap;
	std::map<SDL_Scancode, KeyScancode> sdlToVersoScancodesMap;

	std::map<KeyCode, UString> keyCodeToNameMap;
	std::map<UString, KeyCode> nameToKeyCodeMap;

	std::map<Keymodifier, UString> keymodifierToNameMap;
	std::map<UString, Keymodifier> nameToKeymodifierMap;

	std::map<KeyScancode, UString> keyScancodeToNameMap;
	std::map<UString, KeyScancode> nameToKeyScancodeMap;
};


} // End namespace Verso
