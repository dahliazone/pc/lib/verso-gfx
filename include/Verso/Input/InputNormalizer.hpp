#pragma once

// \TODO: check through code to match Verso standards!!!

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Math/Range.hpp>
#include <ostream>

namespace Verso {


/*
== CASE 1 ==
range.min = -32000; range.max = 32000;
deadzone.min = -4000; deadzone.max = 4000;

					   -(inputvalue - deadzone.min) / (range.min - deadzone.min)
inputvalue = -8000; => -( (-8000) - (-4000) / (-32000) - (-4000)) => -( -4000 / -28000) => -0.143f // OK

					  (inputvalue - deadzone.max) / (range.max - deadzone.max)
inputvalue = 8000; => (8000 - 4000) / (32000 - 4000) => 4000 / 28000 => 0.143f // OK

== CASE 2 ==
range.min = -32000; range.max = 32000;
deadzone.min = 0; deadzone.max = 4000;

					   -(inputvalue - deadzone.min) / (range.min - deadzone.min)
inputvalue = -8000; => -( (-8000) - (-0) / (-32000) - (-0)) => -( -8000 / -32000) => -0.25f OK

					  (inputvalue - deadzone.max) / (range.max - deadzone.max)
inputvalue = 8000; => (8000 - 4000) / (32000 - 4000) => 4000 / 28000 => 0.143f // OK

== CASE 3 ==
range.min = -32000; range.max = 32000;
deadzone.min = 2000; deadzone.max = 4000;

					   -(inputvalue - deadzone.min) / (range.min - deadzone.min)
inputvalue = -8000; => -( (-8000) - (2000) / (-32000) - (2000)) => -( -10000 / -34000) => -0.294 OK

					  (inputvalue - deadzone.max) / (range.max - deadzone.max)
inputvalue = 8000; => ( (8000) - (4000) / (32000) - (4000)) => ( 4000 / 28000) => 0.143 OK

== CASE 4 ==
range.min = 0; range.max = 32000;
deadzone.min = 0; deadzone.max = 4000;
value = -8000; value = 8000;

== CASE 5 ==
range.min = -32000; range.max = 0;
deadzone.min = -4000; deadzone.max = 0;
value = -8000; value = 8000;
*/


template <typename T>
class InputNormalizer
{
public:
	Range<T> range;
	Range<T> deadzone;

public:
	InputNormalizer() :
		range(),
		deadzone()
	{
	}


	InputNormalizer(const Range<T>& range, const Range<T>& deadzone) :
		range(range),
		deadzone(deadzone)
	{
	}


	virtual ~InputNormalizer()
	{
	}


	float normalize(T value) const
	{
		// \TODO: is this the right way to normalize input: check also InputManager which uses this

		if (value < range.minValue) {
			value = range.minValue;
		}
		if (value > range.maxValue) {
			value = range.maxValue;
		}

		if (value < deadzone.minValue) {
			return -static_cast<float>(value - deadzone.minValue) / static_cast<float>(range.minValue - deadzone.minValue);
		}
		else if (value > deadzone.maxValue) {
			return static_cast<float>(value - deadzone.maxValue) / static_cast<float>(range.maxValue - deadzone.maxValue);
		}
		else {
			return 0.0f;
		}
	}


public: // toString
	UString toString() const
	{
		UString s = "[";
		s.append2(range.minValue);
		s += "..(";
		s.append2(deadzone.minValue);
		s += "..";
		s.append2(deadzone.maxValue);
		s += ")..";
		s.append2(range.maxValue);
		s += "]";
		return s;
	}


	UString toStringDebug() const
	{
		UString str("InputNormalizer(");
		str += toString();
		str += ")";
		return str;
	}


	friend std::ostream& operator <<(std::ostream& ost, const InputNormalizer<T>& right)
	{
		return ost << right.toString();
	}

};


typedef InputNormalizer<int> InputNormalizeri;
typedef InputNormalizer<unsigned int> InputNormalizeru;
typedef InputNormalizer<float> InputNormalizerf;


} // End namespace Verso
