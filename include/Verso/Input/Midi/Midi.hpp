#pragma once

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/Input/Midi/MidiDeviceType.hpp>
#include <rtmidi/RtMidi.h>
#include <vector>
#include <iomanip>
#include <cstdint>

namespace Verso {


class Midi
{
public:
	VERSO_GFX_API Midi();

	Midi(const Midi& original) = delete;

	VERSO_GFX_API Midi(Midi&& original) noexcept;

	Midi& operator =(const Midi& original) = delete;

	VERSO_GFX_API Midi& operator =(Midi&& original) noexcept;

	VERSO_GFX_API ~Midi();

public:
	VERSO_GFX_API void printMidiDevices() const;
	VERSO_GFX_API bool createMidi(MidiDeviceType deviceType);
	VERSO_GFX_API void createAll();
	VERSO_GFX_API void destroy();
	VERSO_GFX_API std::vector<std::uint8_t> getInitializedInputDevicesEnum() const;
	VERSO_GFX_API std::vector<std::uint8_t> getInitializedOutputDevicesEnum() const;
	VERSO_GFX_API std::vector<UString> getInitializedInputDevicesString() const;
	VERSO_GFX_API std::vector<UString> getInitializedOutputDevicesString() const;
	VERSO_GFX_API void printInitializedInputDevices() const;
	VERSO_GFX_API void printInitializedOutputDevices() const;

public: // static
	VERSO_GFX_API static std::vector<std::uint8_t>& getMidiDeviceTypesEnum(); // should be MidiDeviceType but VS2013 has a bug
	VERSO_GFX_API static std::vector<UString>& getMidiDeviceTypesString();
	VERSO_GFX_API static std::vector<RtMidiIn*>& getMidiIn();
	VERSO_GFX_API static std::vector<RtMidiOut*>& getMidiOut();
};


} // End namespace Verso
