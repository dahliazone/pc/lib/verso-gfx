#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


enum class MidiDeviceType : std::uint8_t {
	Lpd8 = 0,
	NanoKontrol2,
	DJ2Go,
	QinHengElectronicsCh345MidiAdapter,
	NiKompleteKontrolA61In,
	NiKompleteKontrolA61Out,
	FocusriteScarletIn,
	FocusriteScarletOut,
	FocusriteScarlet8i6,
	ArturiaKeyLabMkII61Midi,
	ArturiaKeyLabMkII61Daw,
	Size,
	Unset
};


inline UString midiDeviceTypeToString(const MidiDeviceType& midiDeviceType)
{
	switch (midiDeviceType)
	{
	case MidiDeviceType::Unset:
		return "Unset";
	case MidiDeviceType::Lpd8:
		return "Lpd8";
	case MidiDeviceType::NanoKontrol2:
		return "NanoKontrol2";
	case MidiDeviceType::DJ2Go:
		return "DJ2Go";
	case MidiDeviceType::QinHengElectronicsCh345MidiAdapter:
		return "QinHengElectronicsCh345MidiAdapter";
	case MidiDeviceType::NiKompleteKontrolA61In:
		return "KOMPLETE KONTROL A61 MIDI 1";
	case MidiDeviceType::NiKompleteKontrolA61Out:
		return "KOMPLETE KONTROL A61 MIDI 2";
	case MidiDeviceType::FocusriteScarletIn:
		return "2- Focusrite USB MIDI 0";
	case MidiDeviceType::FocusriteScarletOut:
		return "2- Focusrite USB MIDI 1";
	case MidiDeviceType::FocusriteScarlet8i6:
		return "Scarlett 8i6 USB"; // OSX
	case MidiDeviceType::ArturiaKeyLabMkII61Midi:
		return "KeyLab mkII 61 MIDI"; // OSX
	case MidiDeviceType::ArturiaKeyLabMkII61Daw:
		return "KeyLab mkII 61 DAW"; // OSX
	default:
		return "Unknown value";
	}
}


// returns MidiDeviceType::Unset for any erroneous string
inline MidiDeviceType stringToMidiDeviceType(const UString& str)
{
	if (str.equals("Lpd8")) {
		return MidiDeviceType::Lpd8;
	}
	else if (str.equals("NanoKontrol2")) {
		return MidiDeviceType::NanoKontrol2;
	}
	else if (str.equals("DJ2Go")) {
		return MidiDeviceType::DJ2Go;
	}
	else if (str.equals("QinHengElectronicsCh345MidiAdapter")) {
		return MidiDeviceType::QinHengElectronicsCh345MidiAdapter;
	}
	else if (str.equals("KOMPLETE KONTROL A61 MIDI 1")) {
		return MidiDeviceType::NiKompleteKontrolA61In;
	}
	else if (str.equals("KOMPLETE KONTROL A61 MIDI 2")) {
		return MidiDeviceType::NiKompleteKontrolA61Out;
	}
	else if (str.equals("2- Focusrite USB MIDI 0")) {
		return MidiDeviceType::FocusriteScarletIn;
	}
	else if (str.equals("2- Focusrite USB MIDI 1")) {
		return MidiDeviceType::FocusriteScarletOut;
	}
	else if (str.equals("Scarlett 8i6 USB")) { // OSX
		return MidiDeviceType::FocusriteScarlet8i6;
	}
	else if (str.equals("KeyLab mkII 61 MIDI")) { // OSX
		return MidiDeviceType::ArturiaKeyLabMkII61Midi;
	}
	else if (str.equals("KeyLab mkII 61 DAW")) { // OSX
		return MidiDeviceType::ArturiaKeyLabMkII61Daw;
	}
	else {
		return MidiDeviceType::Unset;
	}
}


} // End namespace Verso
