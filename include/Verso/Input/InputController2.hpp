#pragma once

// \TODO: check through code to match Verso standards!!!

#include <Verso/Input/Event.hpp>
#include <Verso/Input/Mouse/MouseState.hpp>
#include <Verso/Input/WeightedButtonState.hpp>
#include <forward_list>

namespace Verso {


typedef uint32_t AxisIndex;
typedef uint32_t ButtonIndex;


class ButtonData
{
public:
	WeightedButtonState state;
	bool pressedDown;
	bool releasedUp;
public:
	ButtonData() :
		state(),
		pressedDown(false),
		releasedUp(false)
	{
	}
};


class ControlDescription
{
public:
	UString name;
	UString description;

public:
	ControlDescription(const UString& name = UString(), const UString& description = UString()) :
		name(name),
		description(description)
	{
	}
};


class KeyMapping
{
public:
	KeyCode key;
	Keymodifiers modifiers;
public:
	KeyMapping(KeyCode key, Keymodifiers modifiers = static_cast<Keymodifiers>(Keymodifier::NoneOrAny)) :
		key(key),
		modifiers(modifiers)
	{
	}
};


// contains current state + list of events
class InputController2
{
private:
	// \TODO: Mouse field (temporary?)
	std::forward_list<MouseEvent> mouseEvents;
	  // \TODO: check if here's all the state provided by SDL2!!!
	int32_t mouseX, mouseY;
	int32_t mouseMotionX, mouseMotionY; // mouse relative movement
	AxisState mouseMotionXRel, mouseMotionYRel; // mouse relative movement in relative coordinates [0..1]
	ButtonState mouseButtonState[static_cast<size_t>(MouseButton::Size)];
	int32_t mouseWheelMotionX, mouseWheelMotionY; // scrolled amount with mouse wheel (usually -1 or 1)
	float mouseWheelMotionPreciseX, mouseWheelMotionPreciseY; // scrolled amount with mouse wheel as float
	AxisState mouseWheelMotionXRel, mouseWheelMotionYRel; // scrolled amount with mouse wheel in relative coordinates [0..1]

	UString name;

	//map<KeySymbol, map<ButtonIndex>> keyboardToButton;
	std::map<KeyCode, std::map<Keymodifiers, ButtonIndex>> keyboardToButton;
	//map<MouseAxis, AxisIndex> mouseAxisToAxis;
	std::map<MouseButton, ButtonIndex> mouseButtonToButton;
	std::map<GameControllerAxis, AxisIndex> gameControllerButtonToAxis;
	std::map<GameControllerButton, ButtonIndex> gameControllerButtonToButton;
	std::map<JoystickAxis, AxisIndex> joystickAxisToAxis;
	std::map<JoystickButton, ButtonIndex> joystickButtonToButton;

	std::vector<AxisState> axisDatas;
	std::vector<ControlDescription> axisDescriptions;
	std::vector<ButtonData> buttonDatas;
	std::vector<ControlDescription> buttonDescriptions;

// \TODO: map source kb, mouse,gamepad,midi to names.
// \TODO: think about what different kinds of string->action there is and extra params for them!

public:
	VERSO_GFX_API explicit InputController2(const UString& name);
	VERSO_GFX_API ~InputController2();

	VERSO_GFX_API void processEvent(const Event& event);
	VERSO_GFX_API void resetBoth();
	VERSO_GFX_API void resetPressedDown();
	VERSO_GFX_API void resetReleasedUp();
	
	// Retrieve input status
	VERSO_GFX_API AxisState getAxisState(AxisIndex axisIndex) const;

	VERSO_GFX_API ButtonData getButtonData(ButtonIndex buttonIndex) const;
	VERSO_GFX_API ButtonState getButtonState(ButtonIndex buttonIndex) const;
	VERSO_GFX_API WeightedButtonState getWeightedButtonState(ButtonIndex buttonIndex) const;
	VERSO_GFX_API bool getButtonPressedDown(ButtonIndex buttonIndex) const;
	VERSO_GFX_API bool getButtonReleasedUp(ButtonIndex buttonIndex) const;

	VERSO_GFX_API void setButtonState(ButtonIndex buttonIndex, WeightedButtonState state);
	VERSO_GFX_API void setButtonPressed(ButtonIndex buttonIndex, ButtonState pressed);


	// Events


	// Keyboard
	VERSO_GFX_API void mapKeyboardToButton(ButtonIndex buttonIndex, KeyMapping keyMapping, const UString& buttonName = UString(), const UString& buttonDescription = UString());
	/*
	// Mouse
	//VERSO_GFX_API AxisIndex mapMouseAxisToAxis(const UString& axisName, MouseAxis axis);
	VERSO_GFX_API ButtonIndex mapMouseButtonToButton(const UString& buttonName, MouseButton mouseButton);

	// Gamepad
	VERSO_GFX_API AxisIndex mapGameControllerButtonToAxis(const UString& axisName, GameControllerAxis gameControllerAxis);
	VERSO_GFX_API ButtonIndex mapGameControllerButtonToButton(const UString& buttonName, GameControllerButton gameControllerButton);

	// Joystick
	VERSO_GFX_API UString findAxisNameByAxisType(JoystickAxis joystickAxis);
	VERSO_GFX_API UString findButtonNameByButtonType(JoystickButton joystickButton);
	VERSO_GFX_API AxisIndex mapJoystickAxisToAxis(const UString& axisName, JoystickAxis joystickAxis);
	VERSO_GFX_API ButtonIndex mapJoystickButtonToButton(const UString& buttonName, JoystickButton joystickButton);
	VERSO_GFX_API ButtonIndex mapJoystickHatToButton(const UString& buttonName, JoystickHat joystickHat, JoystickHatDirection joystickDirection);
	*/
};


} // End namespace Verso
