#pragma once

#include <Verso/Audio/Audio2d.hpp>
#include <map>

namespace Verso {


class Audio2dSdl2 : public Audio2d
{
private:
	bool created;
	bool noSound;
	uint32_t frequency;
	bool stereo;
	AudioDevice audioDevice;

	std::map<UString, std::uint32_t> music;

private:
	VERSO_GFX_API Audio2dSdl2();

	// \TODO: Copy & move operators!

public: // static
	VERSO_GFX_API static Audio2dSdl2& instance();

public:
	VERSO_GFX_API virtual ~Audio2dSdl2() override;

	VERSO_GFX_API virtual std::vector<AudioDevice> getDevices() override;

	VERSO_GFX_API virtual void create(
			bool noSound = false,
			std::int64_t deviceId = -1, // -1 = default device, 0 = first real output device.
			uint32_t frequency = 44100,
			uint32_t samples = 512,
			bool stereo = true,
			void (*audioCallback) (void* userData, std::uint8_t* stream, int len) = nullptr,
			void* callbackUserData = nullptr);

	VERSO_GFX_API virtual void destroy() VERSO_NOEXCEPT override;

	VERSO_GFX_API bool isCreated() const;

	VERSO_GFX_API virtual void update() override;

	VERSO_GFX_API virtual void loadMusic(const UString& name, const UString& fileName) override;

	VERSO_GFX_API virtual void deleteMusic(const UString& name) override;

	VERSO_GFX_API virtual void playMusic(const UString& name, bool loop = false) override;

	VERSO_GFX_API virtual double getPositionSeconds(const UString& name) override;

	VERSO_GFX_API virtual double getDurationSeconds(const UString& name) override;

	VERSO_GFX_API virtual void rewind(const UString& name, double seconds) override;

	VERSO_GFX_API virtual void fadeInMusic(const UString& name, int loopCount = 0, float fadeTime = 1.0f) override;

	VERSO_GFX_API virtual void changeMusicFadeOutIn(const UString& newMusic, int loopCount = 0, float fadeTime = 2.0f) override;

	VERSO_GFX_API virtual void startMusic() override;

	VERSO_GFX_API virtual void pauseMusic() override;

	VERSO_GFX_API virtual void stopMusic() override;

	VERSO_GFX_API virtual void fadeOutMusic(float fadeTime = 1.0f) override;

	VERSO_GFX_API virtual int setMusicVolume(int volume) override;

	VERSO_GFX_API virtual void loadSfx(const UString& name, const UString& fileName) override;

	VERSO_GFX_API virtual void deleteSfx(const UString& name) override;

	VERSO_GFX_API virtual int playSfx(const UString& name, int channel, int loops) override;

	VERSO_GFX_API virtual int setChannelVolume(int channel, int volume) override;

	VERSO_GFX_API virtual void haltChannel(int channel) override;

	VERSO_GFX_API virtual AudioDevice getDevice() const override;
};


} // End namespace Verso
