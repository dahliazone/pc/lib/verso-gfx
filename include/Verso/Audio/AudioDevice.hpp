#pragma once

#include <Verso/Audio/AudioDeviceType.hpp>

namespace Verso {


class AudioDevice
{
public:
	std::int64_t deviceId;
	UString name;
	AudioDeviceType type;
	bool initialized;
	bool defaultDevice;
	bool output;
	bool input;
	bool loopback;
	UString driverFileName;
	int bitsPerSample;
	int channels;
	int sampleRate;
	bool integer; // true = integer, false = float
	bool unsignedint; // true = unsigned, false = signed, float = undefined
	bool littleEndian; // true = little endian, false = big endian
	int bufferLength;

public:
	AudioDevice(std::int64_t deviceId, const AudioDeviceType& type, const UString& name,
				bool initialized = false, bool defaultDevice = false,
				bool output = true, bool input = false, bool loopback = false,
				const UString& driverFileName = "") :
		deviceId(deviceId),
		name(name),
		type(type),
		initialized(initialized),
		defaultDevice(defaultDevice),
		output(output),
		input(input),
		loopback(loopback),
		driverFileName(driverFileName)
	{
	}

public: // toString
	UString toString() const
	{
		return name;
	}

	UString toStringFull() const
	{
		UString str("deviceId=");
		str.append2(deviceId);

		str += ", name=\"";
		str.append2(name);
		str += "\"";

		str += ", type=";
		str.append2(audioDeviceTypeToString(type));

		if (!driverFileName.isEmpty()) {
			str += ", driverFileName=\"";
			str += driverFileName;
			str += "\"";
		}

		str += ", format=";
		str.append2(bitsPerSample);
		str += " bit";
		if (integer) {
			if (unsignedint) {
				str += " unsigned";
			}
			else {
				str += " signed";
			}
			str += " integer";
		}
		else {
			str += " float";
		}

		if (littleEndian) {
			str += "little endian";
		}
		else {
			str += "big endian";
		}

		str.append2(sampleRate);
		str += " kHz";
		if (channels == 2) {
			str += " stereo";
		}
		else if (channels == 1) {
			str += " stereo";
		}
		else {
			str += " ";
			str.append2(channels);
			str += " channel";
		}

		str += ", bufferLength=";
		str.append2(bufferLength);

		str += ", initialized=";
		if (initialized == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", defaultDevice=";
		if (defaultDevice == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", output=";
		if (output == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", input=";
		if (input == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		str += ", loopback=";
		if (loopback == true) {
			str += "true";
		}
		else {
			str += "false";
		}

		return str;
	}

	UString toStringDebug() const
	{
		UString str("AudioDevice(");
		str += toString();
		str += ")";
		return str;
	}
};


} // End namespace Verso
