#pragma once

// Header required to include for generating Windows main function

#if _MSC_VER && !__INTEL_COMPILER
#include <SDL_main.h>
#endif
