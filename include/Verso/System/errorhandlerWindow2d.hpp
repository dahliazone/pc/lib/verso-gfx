#pragma once

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>

namespace Verso {


VERSO_GFX_API void errorHandlerWindow2dEnable(const UString& title);


} // End namespace Verso
