#pragma once

#include <Verso/System/win32/consoleRedirectWin32.hpp>

namespace Verso {


void redirectToExistingConsole()
{
#ifdef _WIN32
	priv::redirectToExistingConsoleWin32();
#endif
}


} // End namespace Verso
