#pragma once

#include <Verso/System/SubsystemSdl2.hpp>
#include <Verso/verso-gfx-common.hpp>

namespace Verso {


/**
 * Convient singleton wrapper for initializing and de-initializing SDL2
 * and it's subsystems. Class keeps track of what subsystems are
 * initialized and will de-initialize everything when the singleton
 * is destroyed.
 */
class InitWrapperSdl2
{
protected: // member variables
	bool sdlInitialized;

private:
	VERSO_GFX_API InitWrapperSdl2();

public:
	VERSO_GFX_API static InitWrapperSdl2& instance()
	{
		static InitWrapperSdl2 initWrapperSdl2;
		return initWrapperSdl2;
	}

	VERSO_GFX_API ~InitWrapperSdl2();

	/**
	 * Creates *one* SDL2 subsystem and also initialized SDL2 if it's not yet been initialized.
	 *
	 * Note! Do not call SDL_Init() by yourself!
	 *
	 * @param flag Subsystem to initialize.
	 */
	VERSO_GFX_API void createSubsystem(SubsystemSdl2 flag);

	/**
	 * Destroys the given subsystem.
	 *
	 * Note! You don't need to destroy the subsystems you create unless you
	 *       specifically wan't to do so while program is still running.
	 *
	 * Note! SDL_quit() will be automatically called only when this singleton is destroyed.
	 *       This method won't call it.
	 *
	 * @param flag Subsystem to de-initialize.
	 */
	VERSO_GFX_API void destroySubsystem(SubsystemSdl2 flag) VERSO_NOEXCEPT;

	/**
	 * Destroys SDL2 and all subsystems.
	 */
	VERSO_GFX_API void destroy();

	/**
	 * @return currently active subsystems.
	 */
	VERSO_GFX_API std::vector<SubsystemSdl2> getActiveSubsystems() const;

	/**
	 * @return the state of SDL2 initialization in textual form.
	 */
	VERSO_GFX_API UString toString() const;

	/**
	 * @return the state of SDL2 initialization in textual form with extra information.
	 */
	VERSO_GFX_API UString toStringDebug() const;
};


} // End namespace Verso
