#pragma once

#ifdef _WIN32

#include <Verso/verso-gfx-common.hpp>

namespace Verso {
namespace priv {


VERSO_GFX_API void redirectToExistingConsoleWin32();


} // End namespace priv
} // End namespace Verso

#endif // End _WIN32
