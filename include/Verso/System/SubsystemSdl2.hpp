#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/System/Logger.hpp>
#include <cstdint>

namespace Verso {


enum class SubsystemSdl2 : std::uint32_t {
	None = 0,
	Audio = 1,
	Events = 2,
	Everything = 4,
	GameController = 8,
	Haptic = 16,
	Joystick = 32,
	NoParachute = 64,
	Timer = 128,
	Video = 256
};


inline UString subsystemSdl2ToString(const SubsystemSdl2& subsystemSdl2)
{
	switch (subsystemSdl2)
	{
	case SubsystemSdl2::None:
		return "None";
	case SubsystemSdl2::Audio:
		return "Audio";
	case SubsystemSdl2::Events:
		return "Events";
	case SubsystemSdl2::Everything:
		return "Everything";
	case SubsystemSdl2::GameController:
		return "GameController";
	case SubsystemSdl2::Haptic:
		return "Haptic";
	case SubsystemSdl2::Joystick:
		return "Joystick";
	case SubsystemSdl2::NoParachute:
		return "NoParachute";
	case SubsystemSdl2::Timer:
		return "Timer";
	case SubsystemSdl2::Video:
		return "Video";
	default:
		VERSO_LOG_WARN("verso-gfx", "Unknown SubsystemSdl2 enum given '"<<static_cast<std::uint32_t>(subsystemSdl2)<<"'!");
		return "Unknown value";
	}
}


std::uint32_t subsystemSdl2ToSdl2Type(const SubsystemSdl2& subsystemSdl2);


} // End namespace Verso
