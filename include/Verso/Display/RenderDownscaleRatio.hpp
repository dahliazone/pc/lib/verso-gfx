#pragma once

#include <Verso/verso-gfx-common.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/Logger.hpp>
#include <Verso/Math/Vector2.hpp>
#include <ostream>
#include <cstdint>

namespace Verso {


class RenderDownscaleRatio
{
public:
	uint8_t ratio;
	float factor;

public:
	// ratio can be 1, 2, 4, 8
	VERSO_GFX_API RenderDownscaleRatio(uint8_t ratio = 1);

	// ratioStr can be "disabled" / "1:1", "1:2", "1:4" or "1:8"
	VERSO_GFX_API explicit RenderDownscaleRatio(const UString& ratioStr);

	VERSO_GFX_API void set(uint8_t ratio);

	VERSO_GFX_API void set(const UString& ratioStr);

public: // static
	VERSO_GFX_API static const RenderDownscaleRatio& Disabled();

	VERSO_GFX_API static const RenderDownscaleRatio& OneHalf();

	VERSO_GFX_API static const RenderDownscaleRatio& OneQuarter();

	VERSO_GFX_API static const RenderDownscaleRatio& OneEighth();

	VERSO_GFX_API static const std::vector<RenderDownscaleRatio>& getRenderDownscaleRatios();

	VERSO_GFX_API static RenderDownscaleRatio* findIdenticalFromList(
			const RenderDownscaleRatio& selected, std::vector<RenderDownscaleRatio>& renderDownscaleRatios);

public: // toString
	VERSO_GFX_API UString toString() const;

	VERSO_GFX_API UString toStringHuman() const;

	VERSO_GFX_API UString toStringRenderAtResolution(const Vector2i& baseResolution = Vector2i(0, 0)) const;

	VERSO_GFX_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const RenderDownscaleRatio& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const RenderDownscaleRatio& left, const RenderDownscaleRatio& right)
{
	return (left.ratio == right.ratio);
}


inline bool operator !=(const RenderDownscaleRatio& left, const RenderDownscaleRatio& right)
{
	return !(left == right);
}


inline bool operator <(const RenderDownscaleRatio& left, const RenderDownscaleRatio& right)
{
	return left.ratio < right.ratio;
}


inline bool operator >(const RenderDownscaleRatio& left, const RenderDownscaleRatio& right)
{
	return right < left;
}


inline bool operator <=(const RenderDownscaleRatio& left, const RenderDownscaleRatio& right)
{
	return !(right < left);
}


inline bool operator >=(const RenderDownscaleRatio& left, const RenderDownscaleRatio& right)
{
	return !(left < right);
}


} // End namespace Verso
