#pragma once

#include <Verso/System/UString.hpp>
#include <Verso/System/Exception/IllegalParametersException.hpp>

namespace Verso {


enum class WindowStyle : int16_t
{
	None = 0,
	Fullscreen = 1 << 0, // note: dangerous/buggy in Linux & OS X!
	FullscreenDesktop = 1 << 1,
	Hidden = 1 << 2,
	Borderless = 1 << 3,
	Resizable = 1 << 4,
	Minimized = 1 << 5,
	Maximized = 1 << 6,
	InputGrabbed = 1 << 7,
	AllowHighDpi = 1 << 8,
	ForceOnTop = 1 << 9,
	Default = Resizable
};


typedef int16_t WindowStyles;


inline WindowStyles operator |(WindowStyle left, WindowStyle right)
{
	return static_cast<WindowStyles>(static_cast<WindowStyles>(left) | static_cast<WindowStyles>(right));
}


inline WindowStyles operator |(WindowStyle left, WindowStyles right)
{
	return static_cast<WindowStyles>(static_cast<WindowStyles>(left) | right);
}


inline WindowStyles operator |(WindowStyles left, WindowStyle right)
{
	return static_cast<WindowStyles>(left | static_cast<WindowStyles>(right));
}


inline UString windowStylesToString(WindowStyles styles)
{
	if (styles == static_cast<WindowStyles>(WindowStyle::None)) {
		return "None";
	}

	UString result;
	int16_t stylesLeft = styles;
	bool first = true;
	if (styles & static_cast<WindowStyles>(WindowStyle::Fullscreen)) {
		result += "Fullscreen";
		stylesLeft |= static_cast<WindowStyles>(WindowStyle::Fullscreen);
		first = false;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Borderless)) {
		if (first == false) {
			result += " | ";
		}
		result += "Borderless";
		stylesLeft |= static_cast<WindowStyles>(WindowStyle::Borderless);
		first = false;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Resizable)) {
		if (first == false) {
			result += " | ";
		}
		result += "Resizable";
		stylesLeft |= static_cast<WindowStyles>(WindowStyle::Resizable);
		first = false;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::Hidden)) {
		if (first == false) {
			result += " | ";
		}
		result += "Hidden";
		stylesLeft |= static_cast<WindowStyles>(WindowStyle::Hidden);
		first = false;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::ForceOnTop)) {
		if (first == false) {
			result += " | ";
		}
		result += "ForceOnTop";
		stylesLeft |= static_cast<WindowStyles>(WindowStyle::ForceOnTop);
		first = false;
	}

	if (styles & static_cast<WindowStyles>(WindowStyle::AllowHighDpi)) {
		if (first == false) {
			result += " | ";
		}
		result += "AllowHighDpi";
		stylesLeft |= static_cast<WindowStyles>(WindowStyle::AllowHighDpi);
	}

	if (stylesLeft != 0) {
		UString error("Invalid WindowStyles(");
		error.append2(static_cast<int16_t>(styles));
		error += ") given. Flags (";
		error.append2(stylesLeft);
		error += ") left.";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}

	return result;
}


inline UString windowStyleToString(WindowStyle style)
{
	return windowStylesToString(static_cast<WindowStyles>(style));
}


} // End namespace Verso
