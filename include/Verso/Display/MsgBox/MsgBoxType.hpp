#pragma once

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class MsgBoxType : std::int32_t
{
	Unset,
	Error,
	Warning,
	Information
};


// returns MsgBoxType::Unset for any erroneous string
inline MsgBoxType stringToMsgBoxType(const UString& str)
{
	if (str.equals("Unset"))
		return MsgBoxType::Unset;
	else if (str.equals("Error"))
		return MsgBoxType::Error;
	else if (str.equals("Warning"))
		return MsgBoxType::Warning;
	else if (str.equals("Information"))
		return MsgBoxType::Information;
	else
		return MsgBoxType::Unset;
}


inline UString MsgBoxTypeToString(MsgBoxType MsgBoxType)
{
	switch (MsgBoxType) {
	case MsgBoxType::Unset:
		return "Unset";
	case MsgBoxType::Error:
		return "Error";
	case MsgBoxType::Warning:
		return "Warning";
	case MsgBoxType::Information:
		return "Information";
	default:
		return "unknown value";
	}
}


} // End namespace Verso
