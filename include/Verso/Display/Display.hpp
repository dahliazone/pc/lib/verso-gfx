#pragma once

#include <Verso/verso-gfx-common.hpp>
#include <Verso/Math/Rect.hpp>
#include <Verso/Display/RefreshRate.hpp>

namespace Verso {


class DisplayMode; // Handle cross linkage


class Display
{
public:
	int index;
	UString name;
	Recti rect;

public:
	VERSO_GFX_API Display();

	VERSO_GFX_API Display(const Display& original);

	VERSO_GFX_API Display(const Display&& original);

	VERSO_GFX_API Display(int index, const UString& name, const Recti& rect);

	VERSO_GFX_API ~Display();

	VERSO_GFX_API Display& operator =(const Display& original);

	VERSO_GFX_API Display& operator =(Display&& original) noexcept;

public:
	VERSO_GFX_API DisplayMode getDesktopMode();

	VERSO_GFX_API DisplayMode getCurrentMode();

	VERSO_GFX_API std::vector<DisplayMode> getFullscreenModes();

	VERSO_GFX_API std::vector<RefreshRate> getFullscreenRefreshRates();

public: // static
	VERSO_GFX_API static std::vector<Display> getDisplays();

	VERSO_GFX_API static std::vector<Display> getDisplay(const UString& name);

	VERSO_GFX_API static Display getDisplay(int displayIndex);

public:
	VERSO_GFX_API UString toString() const;

	VERSO_GFX_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const Display& right)
	{
		return ost << right.toString();
	}
};


} // End namespace Verso
