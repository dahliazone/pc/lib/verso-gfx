#pragma once

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class SystemCursorType : int8_t {
	Arrow = 0,     // arrow
	IBeam,     // i-beam
	Wait,      // wait
	Crosshair,  // crosshair
	WaitArrow, // small wait cursor (or wait if not available)
	SizeNwSe,  // double arrow pointing northwest and southeast
	SizeNeSw,  // double arrow pointing northeast and southwest
	SizeWe,    // double arrow pointing west and east
	SizeNs,    // double arrow pointing north and south
	SizeAll,   // four pointed arrow pointing north, south, east, and west
	No,        // slashed circle or crossbones
	Hand,      // hand
	EnumSize,      // Amount system cursors
	Unset,
	Default = Arrow
};


inline UString systemCursorTypeToString(const SystemCursorType& systemCursorType)
{
	if (systemCursorType ==  SystemCursorType::Arrow) {
		return "Arrow";
	}
	else if (systemCursorType == SystemCursorType::IBeam) {
		return "IBeam";
	}
	else if (systemCursorType == SystemCursorType::Wait) {
		return "Wait";
	}
	else if (systemCursorType == SystemCursorType::Crosshair) {
		return "Crosshair";
	}
	else if (systemCursorType == SystemCursorType::WaitArrow) {
		return "WaitArrow";
	}
	else if (systemCursorType == SystemCursorType::SizeNwSe) {
		return "SizeNwSe";
	}
	else if (systemCursorType == SystemCursorType::SizeNeSw) {
		return "SizeNeSw";
	}
	else if (systemCursorType == SystemCursorType::SizeWe) {
		return "SizeWe";
	}
	else if (systemCursorType == SystemCursorType::SizeNs) {
		return "SizeNs";
	}
	else if (systemCursorType == SystemCursorType::SizeAll) {
		return "SizeAll";
	}
	else if (systemCursorType == SystemCursorType::No) {
		return "No";
	}
	else if (systemCursorType == SystemCursorType::Hand) {
		return "Hand";
	}
	else if (systemCursorType == SystemCursorType::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}


} // End namespace Verso
