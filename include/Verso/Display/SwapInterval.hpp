#pragma once

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class SwapInterval : int8_t {
	Immediate = 0,
	Vsync = 1,
	AdaptiveVsync = -1,
	Unset = -2
};


inline UString swapIntervalToString(const SwapInterval& swapInterval)
{
	if (swapInterval == SwapInterval::Immediate) {
		return "Immediate";
	}
	else if (swapInterval == SwapInterval::Vsync) {
		return "Vsync";
	}
	else if (swapInterval == SwapInterval::AdaptiveVsync) {
		return "AdaptiveVsync";
	}
	else if (swapInterval == SwapInterval::Unset) {
		return "Unset";
	}
	else {
		return "Unknown value";
	}
}



} // End namespace Verso
