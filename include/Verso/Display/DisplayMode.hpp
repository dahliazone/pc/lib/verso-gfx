#pragma once

#include <Verso/Display/PixelFormat.hpp>
#include <Verso/Math/AspectRatio.hpp>
#include <Verso/Display/DisplayModeRating.hpp>
#include <Verso/Display/RefreshRate.hpp>

namespace Verso {

class Display; // Handle cross linkage


class DisplayMode
{
public:
	Vector2i resolution;
	size_t bitsPerPixel;
	PixelFormat pixelFormat;
	RefreshRate refreshRate;
	AspectRatio displayAspectRatio;
	DisplayModeRating rating;
	std::vector<UString> names;

private:
	static bool knownDisplayModesInitialized;
	static std::vector<DisplayMode> knownDisplayModes;

public:
	VERSO_GFX_API DisplayMode();

	VERSO_GFX_API DisplayMode(
			const Vector2i& resolution,
			size_t bitsPerPixel, const RefreshRate& refreshRate,
			const PixelFormat& pixelFormat = PixelFormat(),
			const DisplayModeRating& rating = DisplayModeRating::Normal,
			const std::vector<UString>& names = std::vector<UString>());

	VERSO_GFX_API DisplayMode(const DisplayMode& original);

	VERSO_GFX_API DisplayMode(DisplayMode&& original) noexcept;

	VERSO_GFX_API ~DisplayMode();

	VERSO_GFX_API DisplayMode& operator =(const DisplayMode& original);

	VERSO_GFX_API DisplayMode& operator =(DisplayMode&& original) noexcept;

public:
	VERSO_GFX_API void setResolution(const Vector2i& resolution);

	VERSO_GFX_API bool isFullscreenMode() const;

public: // static
	VERSO_GFX_API static DisplayMode getDesktopMode(int displayIndex = 0);

	VERSO_GFX_API static DisplayMode getCurrentMode(int displayIndex = 0);

	VERSO_GFX_API static DisplayMode getFullscreenMode(int displayIndex, const UString& name);

	VERSO_GFX_API static std::vector<DisplayMode> getFullscreenModes(int displayIndex = 0);

	VERSO_GFX_API static std::vector<RefreshRate> getFullscreenRefreshRates(int displayIndex = 0);

	VERSO_GFX_API static DisplayMode getWindowedMode(int displayIndex, const UString& name);

	VERSO_GFX_API static std::vector<DisplayMode> getWindowedModes(int displayIndex = 0, const DisplayModeRatings& ratings = static_cast<DisplayModeRatings>(DisplayModeRating::Default));

	VERSO_GFX_API static DisplayMode* findResolutionFromList(int width, int height, std::vector<DisplayMode>& displayModes);

	VERSO_GFX_API static DisplayMode* findIdenticalFromList(const DisplayMode& selected, std::vector<DisplayMode>& displayModesFullscreen);

private: // static
	static void displayModeAddHelper(const Vector2i& resolution,
									 const DisplayModeRating& rating = DisplayModeRating::Normal,
									 const UString& name1 = "", const UString& name2 = "",
									 const UString& name3 = "", const UString& name4 = "");

	static void setupKnownDisplayModes();

public:
	VERSO_GFX_API UString toString() const;

	VERSO_GFX_API UString toStringOnlyResolution() const;

	VERSO_GFX_API UString toStringWithoutHz() const;

	VERSO_GFX_API UString toStringOnlyNames() const;

	VERSO_GFX_API UString toStringDebug() const;


	friend std::ostream& operator <<(std::ostream& ost, const DisplayMode& right)
	{
		return ost << right.toString();
	}
};


inline bool operator ==(const DisplayMode& left, const DisplayMode& right)
{
	return (left.resolution == right.resolution) &&
			(left.bitsPerPixel == right.bitsPerPixel) &&
			(left.refreshRate == right.refreshRate);
}


inline bool operator !=(const DisplayMode& left, const DisplayMode& right)
{
	return !(left == right);
}


inline bool operator <(const DisplayMode& left, const DisplayMode& right)
{
	if (left.resolution == right.resolution) {
		if (left.bitsPerPixel == right.bitsPerPixel) {
			return left.refreshRate < right.refreshRate;
		}
		else {
			return left.bitsPerPixel < right.bitsPerPixel;
		}
	}
	else {
		return left.resolution < right.resolution;
	}
}


inline bool operator >(const DisplayMode& left, const DisplayMode& right)
{
	return right < left;
}


inline bool operator <=(const DisplayMode& left, const DisplayMode& right)
{
	return !(right < left);
}


inline bool operator >=(const DisplayMode& left, const DisplayMode& right)
{
	return !(left < right);
}


} // End namespace Verso
