#include <Verso/System/InitWrapperSdl2.hpp>
#include <Verso/Input/InputManager.hpp>
#include <Verso/System/assert.hpp>
#include <Verso/System/Exception.hpp>

namespace Verso {


InitWrapperSdl2::InitWrapperSdl2() :
	sdlInitialized(false)
{
}


InitWrapperSdl2::~InitWrapperSdl2()
{
	destroy();
}


void InitWrapperSdl2::createSubsystem(SubsystemSdl2 flag)
{
	Uint32 sdlFlag = subsystemSdl2ToSdl2Type(flag);

	if (sdlInitialized == false) {
		if (SDL_Init(sdlFlag) != 0) {
			UString error("Could not initialize SDL2 with flag(");
			error.append2(subsystemSdl2ToString(flag));
			error.append2("). SDL2 error: ");
			error.append(SDL_GetError());
			VERSO_ERROR("verso-gfx", error.c_str(), "");
		}
		sdlInitialized = true;
	}

	else {
		if (SDL_WasInit(sdlFlag) != sdlFlag) {
			if (SDL_InitSubSystem(sdlFlag) != 0) {
				UString error("Could not create SDL2 subsystem with flag(");
				error.append2(subsystemSdl2ToString(flag));
				error.append2("). SDL2 error: ");
				error.append(SDL_GetError());
				VERSO_ERROR("verso-gfx", error.c_str(), "");
			}
		}
	}
}


void InitWrapperSdl2::destroySubsystem(SubsystemSdl2 flag) VERSO_NOEXCEPT
{
	VERSO_ASSERT("verso-gfx", sdlInitialized == true);
	SDL_QuitSubSystem(subsystemSdl2ToSdl2Type(flag));
}


void InitWrapperSdl2::destroy()
{
	if (sdlInitialized == true) {
		InputManager::instance().destroy();

		SDL_Quit();

		sdlInitialized = false;
	}
}


std::vector<SubsystemSdl2> InitWrapperSdl2::getActiveSubsystems() const
{
	std::vector<SubsystemSdl2> subsystems;
	Uint32 subsystemInited = SDL_WasInit(SDL_INIT_EVERYTHING);

	if (subsystemInited & SDL_INIT_AUDIO) {
		subsystems.push_back(SubsystemSdl2::Audio);
	}

	if (subsystemInited & SDL_INIT_EVENTS) {
		subsystems.push_back(SubsystemSdl2::Events);
	}

	if (subsystemInited & SDL_INIT_EVERYTHING) {
		subsystems.push_back(SubsystemSdl2::Everything);
	}

	if (subsystemInited & SDL_INIT_GAMECONTROLLER) {
		subsystems.push_back(SubsystemSdl2::GameController);
	}

	if (subsystemInited & SDL_INIT_HAPTIC) {
		subsystems.push_back(SubsystemSdl2::Haptic);
	}

	if (subsystemInited & SDL_INIT_JOYSTICK) {
		subsystems.push_back(SubsystemSdl2::Joystick);
	}

	if (subsystemInited & SDL_INIT_NOPARACHUTE) {
		subsystems.push_back(SubsystemSdl2::NoParachute);
	}

	if (subsystemInited & SDL_INIT_TIMER) {
		subsystems.push_back(SubsystemSdl2::Timer);
	}

	if (subsystemInited & SDL_INIT_VIDEO) {
		subsystems.push_back(SubsystemSdl2::Video);
	}

	return subsystems;
}


UString InitWrapperSdl2::toString() const
{
	UString out("Active subsystems: ");
	std::vector<SubsystemSdl2> active = getActiveSubsystems();
	if (active.size() == 0) {
		out += "None";
	}
	else {
		for (size_t i=0; i<active.size(); ++i) {
			out += subsystemSdl2ToString(active[i]);
			if (i != active.size()-1)
				out += ", ";
		}
	}
	return out;
}


UString InitWrapperSdl2::toStringDebug() const
{
	UString str("InitWrapperSdl2(");
	str += toString();
	str += ")";
	return str;
}


} // End namespace Verso

