#include <Verso/Display/Display.hpp>
#include <Verso/Display/DisplayMode.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>
#include <Verso/System/Exception.hpp>
#include <SDL2/SDL.h>

namespace Verso {


Display::Display() :
	index(-1),
	name(""),
	rect(0, 0, Vector2i())
{
}


Display::Display(const Display& original) :
	index(original.index),
	name(original.name),
	rect(original.rect)
{
}


Display::Display(const Display&& original) :
	index(std::move(original.index)),
	name(std::move(original.name)),
	rect(std::move(original.rect))
{
}


Display::Display(int index, const UString& name, const Recti& rect) :
	index(index),
	name(name),
	rect(rect)
{
}


Display::~Display()
{
}


Display& Display::operator =(const Display& original)
{
	if (this != &original) {
		index = original.index;
		name = original.name;
		rect = original.rect;
	}
	return *this;
}


Display& Display::operator =(Display&& original) noexcept
{
	if (this != &original) {
		index = std::move(original.index);
		name = std::move(original.name);
		rect = std::move(original.rect);
	}
	return *this;
}


DisplayMode Display::getDesktopMode()
{
	if (index >= 0) {
		return DisplayMode::getDesktopMode(index);
	}
	else {
		UString error("Cannot retrieve refresh rates for invalid display #");
		error.append2(index);
		VERSO_ERROR("verso-gfx", error.c_str(), toStringDebug().c_str());
	}
}


DisplayMode Display::getCurrentMode()
{
	if (index >= 0) {
		return DisplayMode::getCurrentMode(index);
	}
	else {
		UString error("Cannot retrieve refresh rates for invalid display #");
		error.append2(index);
		VERSO_ERROR("verso-gfx", error.c_str(), toStringDebug().c_str());
	}
}


std::vector<DisplayMode> Display::getFullscreenModes()
{
	if (index >= 0) {
		return DisplayMode::getFullscreenModes(index);
	}
	else {
		UString error("Cannot retrieve refresh rates for invalid display #");
		error.append2(index);
		VERSO_ERROR("verso-gfx", error.c_str(), toStringDebug().c_str());
	}
}


std::vector<RefreshRate> Display::getFullscreenRefreshRates()
{
	if (index >= 0) {
		return DisplayMode::getFullscreenRefreshRates(index);
	}
	else {
		UString error("Cannot retrieve refresh rates for invalid display #");
		error.append2(index);
		VERSO_ERROR("verso-gfx", error.c_str(), toStringDebug().c_str());
	}
}


// Static methods

std::vector<Display> Display::getDisplays()
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);

	int count = SDL_GetNumVideoDisplays();
	if (count < 0) {
		UString error = "Could not get number of displays. Reason: ";
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	std::vector<Display> displays;
	SDL_Rect bounds;
	for (int i = 0; i<count; ++i) {
		if (SDL_GetDisplayBounds(i, &bounds) < 0) {
			UString error("Could not get display bounds for display #");
			error.append2(i);
			error += ". Reason: ";
			error += SDL_GetError();
			VERSO_ERROR("verso-gfx", error.c_str(), "");
		}
		const char* name = SDL_GetDisplayName(i);
		if (name == nullptr) {
			UString error("Could not get display name for display #");
			error.append2(i);
			error += ". Reason: ";
			error += SDL_GetError();
			VERSO_ERROR("verso-gfx", error.c_str(), "");
		}
		displays.push_back(Display(i, name, Recti(bounds.x, bounds.y, bounds.w, bounds.h)));
	}

	return displays;
}


std::vector<Display> Display::getDisplay(const UString& name)
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);

	int count = SDL_GetNumVideoDisplays();
	if (count < 0) {
		UString error = "Could not get number of displays. Reason: ";
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	std::vector<Display> displays;
	SDL_Rect bounds;
	for (int i = 0; i<count; ++i) {
		if (SDL_GetDisplayBounds(i, &bounds) < 0) {
			UString error("Could not get display bounds for display #");
			error.append2(i);
			error += ". Reason: ";
			error += SDL_GetError();
			VERSO_ERROR("verso-gfx", error.c_str(), "");
		}
		const char* tmpName = SDL_GetDisplayName(i);
		if (tmpName == nullptr) {
			UString error("Could not get display name for display #");
			error.append2(i);
			error += ". Reason: ";
			error += SDL_GetError();
			VERSO_ERROR("verso-gfx", error.c_str(), "");
		}
		if (name.equals(tmpName)) {
			displays.push_back(Display(i, tmpName, Recti(bounds.x, bounds.y, bounds.w, bounds.h)));
		}
	}

	return displays;
}


Display Display::getDisplay(int displayIndex)
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);

	int count = SDL_GetNumVideoDisplays();
	if (count < 0) {
		UString error = "Could not get number of displays. Reason: ";
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}
	if (static_cast<int>(displayIndex) >= count) {
		UString error("Given displayIndex(");
		error.append2(displayIndex);
		error += ") is equal or larger than amount of displays(";
		error.append2(count);
		error += ")";
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), "");
	}

	SDL_Rect bounds;
	if (SDL_GetDisplayBounds(static_cast<int>(displayIndex), &bounds) < 0) {
		UString error("Could not get display bounds for display #");
		error.append2(displayIndex);
		error += ". Reason: ";
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	const char* name = SDL_GetDisplayName(static_cast<int>(displayIndex));
	if (name == nullptr) {
		UString error("Could not get display name for display #");
		error.append2(displayIndex);
		error += ". Reason: ";
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	return Display(displayIndex, name, Recti(bounds.x, bounds.y, bounds.w, bounds.h));
}


// Non-static toString(s)

UString Display::toString() const
{
	UString str("");
	str.append2(index);
	str += ". ";
	str.append2(name);
	return str;
}


UString Display::toStringDebug() const
{
	UString str("Display(");
	str += toString();
	str += " ";
	str.append2(rect.toString());
	str += ")";
	return str;
}


} // End namespace Verso

