#include <Verso/Display/RenderDownscaleRatio.hpp>

namespace Verso {


RenderDownscaleRatio::RenderDownscaleRatio(uint8_t ratio)
{
	set(ratio);
}


RenderDownscaleRatio::RenderDownscaleRatio(const UString& ratioStr)
{
	set(ratioStr);
}


void RenderDownscaleRatio::set(uint8_t ratio)
{
	if (ratio == 1) {
		this->ratio = ratio;
	}
	else if (ratio == 2) {
		this->ratio = ratio;
	}
	else if (ratio == 4) {
		this->ratio = ratio;
	}
	else if (ratio == 8) {
		this->ratio = ratio;
	}
	else {
		UString error("Unknown ratio! Valid ratios are 1, 2, 4, and 8.");
		UString resources("ratio=");
		error.append2(static_cast<int>(ratio));
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), resources.c_str());
	}

	this->factor = 1.0f / static_cast<float>(this->ratio);
}


void RenderDownscaleRatio::set(const UString& ratioStr)
{
	if (ratioStr.equals("disabled")) {
		set(1);
	}
	else if (ratioStr.beginsWith("1:")) {
		UString valueString(ratioStr.substring(2, static_cast<int>(ratioStr.size())));
		int value = valueString.toValue<int>();
		set(static_cast<uint8_t>(value));
	}
	else {
		UString error("Ratio string \"");
		error.append2(ratioStr);
		error.append2("\" must be either \"disabled\" or start with \"1:\"");
		VERSO_ILLEGALFORMAT("verso-gfx", error.c_str(), "");
	}
}


// static


const RenderDownscaleRatio& RenderDownscaleRatio::Disabled()
{
	static const RenderDownscaleRatio ratioNone(1);
	return ratioNone;
}


const RenderDownscaleRatio& RenderDownscaleRatio::OneHalf()
{
	static const RenderDownscaleRatio ratioNone(2);
	return ratioNone;
}


const RenderDownscaleRatio& RenderDownscaleRatio::OneQuarter()
{
	static const RenderDownscaleRatio ratioNone(4);
	return ratioNone;
}


const RenderDownscaleRatio& RenderDownscaleRatio::OneEighth()
{
	static const RenderDownscaleRatio ratioNone(8);
	return ratioNone;
}


const std::vector<RenderDownscaleRatio>& RenderDownscaleRatio::getRenderDownscaleRatios()
{
	static std::vector<RenderDownscaleRatio> ratios(
		{ Disabled(), OneHalf(), OneQuarter(), OneEighth() });
	return ratios;
}


RenderDownscaleRatio* RenderDownscaleRatio::findIdenticalFromList(
		const RenderDownscaleRatio& selected, std::vector<RenderDownscaleRatio>& renderDownscaleRatio)
{
	for (size_t i = 0; i < renderDownscaleRatio.size(); ++i) {
		if (renderDownscaleRatio[i] == selected) {
			return &renderDownscaleRatio[i];
		}
	}
	return nullptr;
}


// toString


UString RenderDownscaleRatio::toString() const
{
	if (ratio == 1 || ratio == 2 || ratio == 4 || ratio == 8) {
		UString str("1:");
		str.append2(static_cast<int>(ratio));
		return str;
	}
	else {
		return "unknown value";
	}
}


UString RenderDownscaleRatio::toStringHuman() const
{
	if (ratio == 1) {
		return "disabled";
	}
	else {
		return toString();
	}
}

UString RenderDownscaleRatio::toStringRenderAtResolution(const Vector2i& baseResolution) const
{
	UString str("Render at ");
	if (ratio == 1) {
		str += "full";
	}
	else if (ratio == 2 || ratio == 4 || ratio == 8) {
		str += toString();
	}
	else {
		return "unknown value";
	}
	str += " resolution";

	str += " (";
	str.append2(baseResolution.x / ratio);
	str += "x";
	str.append2(baseResolution.y / ratio);
	str += ")";

	return str;
}


UString RenderDownscaleRatio::toStringDebug() const
{
	return UString("RenderDownscaleRatio(") + toString() + ")";
}


} // End namespace Verso

