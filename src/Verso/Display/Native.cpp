#include <Verso/Display/Native.hpp>
#include <SDL_syswm.h>

namespace Verso {


namespace Native {


void* getWindowHandle(SDL_Window* window)
{
	SDL_SysWMinfo wmInfo;
	SDL_VERSION(&wmInfo.version); // create info structure with SDL version info
	if (SDL_GetWindowWMInfo(window, &wmInfo)) {
#if defined(SDL_VIDEO_DRIVER_WINDOWS)
		return reinterpret_cast<void*>(wmInfo.info.win.window);
#elif defined(__APPLE__) && defined(SDL_VIDEO_DRIVER_COCOA)
		return reinterpret_cast<void*>(wmInfo.info.cocoa.window);
#else
		return nullptr;
#endif
	}
	else {
		return nullptr;
	}
}


} // End namespace Native


} // End namespace Verso

