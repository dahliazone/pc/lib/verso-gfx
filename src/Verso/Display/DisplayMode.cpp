#include <Verso/Display/DisplayMode.hpp>
#include <Verso/Display/Display.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>
#include <Verso/System/Exception.hpp>
#include <SDL2/SDL.h>
#include <algorithm>

namespace Verso {


bool DisplayMode::knownDisplayModesInitialized = false;
std::vector<DisplayMode> DisplayMode::knownDisplayModes = std::vector<DisplayMode>();


DisplayMode::DisplayMode() :
	resolution(0, 0),
	bitsPerPixel(0),
	pixelFormat(),
	refreshRate(0),
	displayAspectRatio(),
	rating(DisplayModeRating::Normal),
	names()
{
	setupKnownDisplayModes();
}


DisplayMode::DisplayMode(
		const Vector2i& resolution,
		size_t bitsPerPixel, const RefreshRate& refreshRate,
		const PixelFormat& pixelFormat,
		const DisplayModeRating& rating,
		const std::vector<UString>& names) :
	resolution(resolution),
	bitsPerPixel(bitsPerPixel),
	pixelFormat(pixelFormat),
	refreshRate(refreshRate),
	displayAspectRatio(AspectRatio(resolution, "")),
	rating(rating),
	names(names)
{// \TODO: fix aspect ratio textual
	setupKnownDisplayModes();
}


DisplayMode::DisplayMode(const DisplayMode& original) :
	resolution(original.resolution),
	bitsPerPixel(original.bitsPerPixel),
	pixelFormat(original.pixelFormat),
	refreshRate(original.refreshRate),
	displayAspectRatio(original.displayAspectRatio),
	rating(original.rating),
	names(original.names)
{
}


DisplayMode::DisplayMode(DisplayMode&& original) noexcept :
	resolution(std::move(original.resolution)),
	bitsPerPixel(std::move(original.bitsPerPixel)),
	pixelFormat(std::move(original.pixelFormat)),
	refreshRate(std::move(original.refreshRate)),
	displayAspectRatio(std::move(original.displayAspectRatio)),
	rating(std::move(original.rating)),
	names(std::move(original.names))
{
}


DisplayMode::~DisplayMode()
{
}


DisplayMode& DisplayMode::operator =(const DisplayMode& original)
{
	if (this != &original) {
		resolution = original.resolution;
		bitsPerPixel = original.bitsPerPixel;
		pixelFormat = original.pixelFormat;
		refreshRate = original.refreshRate;
		displayAspectRatio = original.displayAspectRatio;
		rating = original.rating;
		names = original.names;
	}
	return *this;
}


DisplayMode& DisplayMode::operator =(DisplayMode&& original) noexcept
{
	if (this != &original) {
		resolution = std::move(original.resolution);
		bitsPerPixel = std::move(original.bitsPerPixel);
		pixelFormat = std::move(original.pixelFormat);
		refreshRate = std::move(original.refreshRate);
		displayAspectRatio = std::move(original.displayAspectRatio);
		rating = std::move(original.rating);
		names = std::move(original.names);
	}
	return *this;
}


void DisplayMode::setResolution(const Vector2i& resolution)
{
	this->resolution = resolution;
	this->displayAspectRatio = AspectRatio(resolution, ""); // \TODO: fix aspect ratio textual
}


bool DisplayMode::isFullscreenMode() const
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);

	const std::vector<DisplayMode>& displayModes = getFullscreenModes();
	return find(displayModes.begin(), displayModes.end(), *this) != displayModes.end();
}


// Static methods

DisplayMode DisplayMode::getDesktopMode(int displayIndex)
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);
	setupKnownDisplayModes();

	SDL_DisplayMode dm;
	if (SDL_GetDesktopDisplayMode(displayIndex, &dm) != 0) {
		SDL_Log("SDL_GetDesktopDisplayMode failed: %s", SDL_GetError());
		return DisplayMode(Vector2i(0, 0), 0, RefreshRate(0));
	}

	DisplayModeRating rating = DisplayModeRating::Normal;
	std::vector<UString> names;
	DisplayMode* dmInfo = findResolutionFromList(dm.w, dm.h, knownDisplayModes);
	if (dmInfo != nullptr) {
		rating = dmInfo->rating;
		names = dmInfo->names;
	}

	return DisplayMode(Vector2i(dm.w, dm.h),
					   SDL_BITSPERPIXEL(dm.format), RefreshRate(dm.refresh_rate),
					   PixelFormat::sdlPixelFormatToPixelFormat(dm.format),
					   rating, names);
}


DisplayMode DisplayMode::getCurrentMode(int displayIndex)
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);
	setupKnownDisplayModes();

	SDL_DisplayMode dm;
	if (SDL_GetCurrentDisplayMode(displayIndex, &dm) != 0) {
		UString error("Could not get display mode for video display #");
		error.append2(displayIndex);
		error += ". Reason: ";
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}
	else {
		DisplayModeRating rating = DisplayModeRating::Normal;
		std::vector<UString> names;
		DisplayMode* dmInfo = findResolutionFromList(dm.w, dm.h, knownDisplayModes);
		if (dmInfo != nullptr) {
			rating = dmInfo->rating;
			names = dmInfo->names;
		}

		return DisplayMode(
					Vector2i(dm.w, dm.h),
					SDL_BITSPERPIXEL(dm.format), RefreshRate(dm.refresh_rate),
					PixelFormat::sdlPixelFormatToPixelFormat(dm.format),
					rating, names);
	}
}


DisplayMode DisplayMode::getFullscreenMode(int displayIndex, const UString& name)
{
	std::vector<DisplayMode> modes = getFullscreenModes(displayIndex);
	for (auto& mode : modes) {
		if (name.equals(mode.toString())) {
			return mode;
		}
	}

	UString error("Cannot find fullscreen DisplayMode by name: ");
	error.append2(name);
	VERSO_OBJECTNOTFOUND("verso-gfx", error.c_str(), "");
}


std::vector<DisplayMode> DisplayMode::getFullscreenModes(int displayIndex)
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);
	setupKnownDisplayModes();

	int count = SDL_GetNumDisplayModes(displayIndex);
	if (count < 0) {
		UString error("Could not get number display modes for video display #");
		error.append2(displayIndex);
		error += ". Reason: ";
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	std::vector<DisplayMode> displayModes;
	SDL_DisplayMode dm;
	for (int modeIndex = 0; modeIndex<count; ++modeIndex) {
		if (SDL_GetDisplayMode(displayIndex, modeIndex, &dm) != 0) {
			UString error("Could not retrieve display mode #");
			error.append2(modeIndex);
			error += " for video display #";
			error.append2(displayIndex);
			error += ". Reason: ";
			error += SDL_GetError();
			VERSO_ERROR("verso-gfx", error.c_str(), "");
		}

		DisplayModeRating rating = DisplayModeRating::Normal;
		std::vector<UString> names;
		DisplayMode* dmInfo = findResolutionFromList(dm.w, dm.h, knownDisplayModes);
		if (dmInfo != nullptr) {
			rating = dmInfo->rating;
			names = dmInfo->names;
		}

		displayModes.push_back(DisplayMode(
								   Vector2i(dm.w, dm.h),
								   SDL_BITSPERPIXEL(dm.format), RefreshRate(dm.refresh_rate),
								   PixelFormat::sdlPixelFormatToPixelFormat(dm.format),
								   rating, names));
	}

	std::sort(displayModes.begin(), displayModes.end(), std::greater<DisplayMode>());

	return displayModes;
}


std::vector<RefreshRate> DisplayMode::getFullscreenRefreshRates(int displayIndex)
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);

	int count = SDL_GetNumDisplayModes(displayIndex);
	if (count < 0) {
		UString error("Could not get number display modes for video display #");
		error.append2(displayIndex);
		error += ". Reason: ";
		error += SDL_GetError();
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	std::vector<RefreshRate> refreshRates;
	SDL_DisplayMode dm;
	for (int modeIndex = 0; modeIndex < count; ++modeIndex) {
		if (SDL_GetDisplayMode(displayIndex, modeIndex, &dm) != 0) {
			UString error("Could not retrieve display mode #");
			error.append2(modeIndex);
			error += " for video display #";
			error.append2(displayIndex);
			error += ". Reason: ";
			error += SDL_GetError();
			VERSO_ERROR("verso-gfx", error.c_str(), "");
		}
		RefreshRate rr(dm.refresh_rate);
		bool found = false;
		for (size_t i = 0; i < refreshRates.size(); ++i) {
			if (refreshRates[i] == rr) {
				found = true;
			}
		}
		if (found == false) {
			refreshRates.push_back(rr);
		}
	}

	std::sort(refreshRates.begin(), refreshRates.end(), std::greater<RefreshRate>());

	return refreshRates;
}


DisplayMode DisplayMode::getWindowedMode(int displayIndex, const UString& name)
{
	std::vector<DisplayMode> modes = getWindowedModes(displayIndex);
	for (auto& mode : modes) {
		if (name.equals(mode.toString())) {
			return mode;
		}
	}

	UString error("Cannot find windowed DisplayMode by name: ");
	error.append2(name);
	VERSO_OBJECTNOTFOUND("verso-gfx", error.c_str(), "");
}


std::vector<DisplayMode> DisplayMode::getWindowedModes(int displayIndex, const DisplayModeRatings& ratings)
{
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Video);
	setupKnownDisplayModes();

	Display display = Display::getDisplay(displayIndex);
	DisplayMode desktopMode = display.getDesktopMode();

	// Make sure all windowed modes has desktop refresh rate and Normal rating
	std::vector<DisplayMode> displayModes;
	std::vector<DisplayMode> displayModesFullscreen = getFullscreenModes(displayIndex);
	for (size_t i = 0; i < displayModesFullscreen.size(); ++i) {
		displayModesFullscreen[i].bitsPerPixel = desktopMode.bitsPerPixel;
		displayModesFullscreen[i].refreshRate = desktopMode.refreshRate;
		displayModesFullscreen[i].pixelFormat = desktopMode.pixelFormat;
		displayModesFullscreen[i].rating = DisplayModeRating::Normal;

		// Check for duplicates
		bool found = false;
		for (size_t j = 0; j < displayModes.size(); ++j) {
			if (displayModesFullscreen[i].resolution == displayModes[j].resolution) {
				found = true;
				break;
			}
		}
		if (found == false) {
			displayModes.push_back(displayModesFullscreen[i]);
		}
	}

	for (size_t i = 0; i < knownDisplayModes.size(); ++i) {
		// Check for duplicates
		bool found = false;
		for (const auto& dm : displayModes) {
			if (dm.resolution == knownDisplayModes[i].resolution) {
				found = true;
			}
		}

		if (found == false && knownDisplayModes[i].resolution.x <= desktopMode.resolution.x) {
			if ((static_cast<std::int8_t>(ratings) & static_cast<std::int8_t>(knownDisplayModes[i].rating)) != 0) {
				displayModes.push_back(
							DisplayMode(
								knownDisplayModes[i].resolution,
								desktopMode.bitsPerPixel,
								desktopMode.refreshRate,
								desktopMode.pixelFormat,
								knownDisplayModes[i].rating,
								knownDisplayModes[i].names));
			}
		}
	}

	std::sort(displayModes.begin(), displayModes.end(), std::greater<DisplayMode>());

	return displayModes;
}


DisplayMode* DisplayMode::findResolutionFromList(int width, int height, std::vector<DisplayMode>& displayModes)
{
	for (size_t i = 0; i < displayModes.size(); ++i) {
		if (displayModes[i].resolution.x == width && displayModes[i].resolution.y == height) {
			return &displayModes[i];
		}
	}
	return nullptr;
}


DisplayMode* DisplayMode::findIdenticalFromList(const DisplayMode& selected, std::vector<DisplayMode>& displayModes)
{
	for (size_t i = 0; i < displayModes.size(); ++i) {
		if (displayModes[i] == selected) {
			return &displayModes[i];
		}
	}
	return nullptr;
}


// Private static methods

void DisplayMode::displayModeAddHelper(
		const Vector2i& resolution,
		const DisplayModeRating& rating,
		const UString& name1, const UString& name2,
		const UString& name3, const UString& name4)
{
	std::vector<UString> names;
	if (!name1.isEmpty()) {
		names.push_back(name1);
	}
	if (!name2.isEmpty()) {
		names.push_back(name2);
	}
	if (!name3.isEmpty()) {
		names.push_back(name3);
	}
	if (!name4.isEmpty()) {
		names.push_back(name4);
	}

	knownDisplayModes.push_back(
				DisplayMode(
					resolution,
					0, RefreshRate(0),
					PixelFormat(), rating, names));
}


void DisplayMode::setupKnownDisplayModes()
{
	if (knownDisplayModesInitialized == false) {
		knownDisplayModesInitialized = true;

		// \TODO: Add aspect ratio information to display modes which are normally scaled when shown

		// 4:3 (1.33:1)
		displayModeAddHelper(Vector2i(160, 120), DisplayModeRating::Esoteric, "QQVGA");
		displayModeAddHelper(Vector2i(192, 144), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(320, 240), DisplayModeRating::Extra, "QVGA");
		displayModeAddHelper(Vector2i(480, 360), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(640, 480), DisplayModeRating::Extra, "VGA");
		displayModeAddHelper(Vector2i(800, 600), DisplayModeRating::Extra, "SVGA");
		displayModeAddHelper(Vector2i(960, 720), DisplayModeRating::Esoteric, "Standard TV 720p anamorphic");
		displayModeAddHelper(Vector2i(1024, 768), DisplayModeRating::Extra, "XGA");
		displayModeAddHelper(Vector2i(1152, 864), DisplayModeRating::Extra, "XGA+");
		displayModeAddHelper(Vector2i(1200, 900), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1280, 960), DisplayModeRating::Extra, "SXGA-", "UVGA");
		displayModeAddHelper(Vector2i(1400, 1050), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1440, 1080), DisplayModeRating::Esoteric, "HDV 1080i anamorphic");
		displayModeAddHelper(Vector2i(1600, 1200), DisplayModeRating::Extra, "UXGA");
		displayModeAddHelper(Vector2i(1920, 1440), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2048, 1536), DisplayModeRating::Extra, "QXGA");
		displayModeAddHelper(Vector2i(2560, 1920), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2880, 2160), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(3200, 2400), DisplayModeRating::Extra, "QUXGA");
		displayModeAddHelper(Vector2i(4096, 3072), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(5120, 3840), DisplayModeRating::Esoteric, "");

		// 5:4 (1.25:1)
		displayModeAddHelper(Vector2i(750, 600), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(960, 768), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1280, 1024), DisplayModeRating::Extra, "SXGA");
		displayModeAddHelper(Vector2i(1500, 1200), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2560, 2048), DisplayModeRating::Extra, "QSXGA");
		displayModeAddHelper(Vector2i(5120, 4096), DisplayModeRating::Esoteric, "");

		// 16:9 (1.77∶1)
		displayModeAddHelper(Vector2i(256, 144), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(640, 360), DisplayModeRating::Esoteric, "nHD");
		displayModeAddHelper(Vector2i(848, 480), DisplayModeRating::Esoteric, "480p (16:9)");
		displayModeAddHelper(Vector2i(854, 480), DisplayModeRating::Esoteric, "FWVGA", "480p (16:9)");
		displayModeAddHelper(Vector2i(960, 540), DisplayModeRating::Esoteric, "qHD");
		displayModeAddHelper(Vector2i(1024, 576), DisplayModeRating::Esoteric, "WSVGA");
		displayModeAddHelper(Vector2i(1136, 640), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1280, 720), DisplayModeRating::Normal, "720p Widescreen", "HD", "WXGA", "HDV 720p");
		displayModeAddHelper(Vector2i(1366, 768), DisplayModeRating::Extra, "FWXGA");
		displayModeAddHelper(Vector2i(1536, 864), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1600, 900), DisplayModeRating::Extra, "HD+");
		displayModeAddHelper(Vector2i(1680, 945), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1920, 1080), DisplayModeRating::Normal, "1080p", "Full-HD", "FHD");
		displayModeAddHelper(Vector2i(2048, 1152), DisplayModeRating::Extra, "QWXGA");
		displayModeAddHelper(Vector2i(2400, 1350), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2560, 1440), DisplayModeRating::Normal, "QHD", "WQHD");
		displayModeAddHelper(Vector2i(2880, 1620), DisplayModeRating::Extra, "WQXGA+");
		displayModeAddHelper(Vector2i(3200, 1800), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(3840, 2160), DisplayModeRating::Normal, "4K UHD", "UHDTV1");
		displayModeAddHelper(Vector2i(4096, 2304), DisplayModeRating::Extra, "Apple Retina 4K");
		displayModeAddHelper(Vector2i(5120, 2880), DisplayModeRating::Extra, "5K", "Apple Retina 5K");
		displayModeAddHelper(Vector2i(6016, 3384), DisplayModeRating::Extra, "Apple Pro Display XDR 6K");
		displayModeAddHelper(Vector2i(7680, 4320), DisplayModeRating::Extra, "8K UHD", "UHDTV2");
		displayModeAddHelper(Vector2i(8192, 4608), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(15360, 8640), DisplayModeRating::Esoteric, "");

		// 16:10 (1.60:1)
		displayModeAddHelper(Vector2i(320, 200), DisplayModeRating::Esoteric, "CGA");
		displayModeAddHelper(Vector2i(384, 240), DisplayModeRating::Esoteric, "WQVGA");
		displayModeAddHelper(Vector2i(768, 480), DisplayModeRating::Esoteric, "WVGA");
		displayModeAddHelper(Vector2i(1024, 640), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1152, 720), DisplayModeRating::Extra, "");
		displayModeAddHelper(Vector2i(1280, 800), DisplayModeRating::Extra, "WXGA");
		displayModeAddHelper(Vector2i(1440, 900), DisplayModeRating::Extra, "WXGA+");
		displayModeAddHelper(Vector2i(1680, 1050), DisplayModeRating::Extra, "WSXGA+");
		displayModeAddHelper(Vector2i(1728, 1080), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1920, 1200), DisplayModeRating::Extra, "WUXGA");
		displayModeAddHelper(Vector2i(2304, 1440), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2560, 1600), DisplayModeRating::Extra, "WQXGA");
		displayModeAddHelper(Vector2i(2880, 1800), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(3072, 1920), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(3840, 2400), DisplayModeRating::Extra, "WQUXGA");
		displayModeAddHelper(Vector2i(4096, 2560), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(5120, 3200), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(6912, 4320), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(8192, 5120), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(16200, 10125), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(15360, 9600), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(16384, 10240), DisplayModeRating::Esoteric, "");

		// 1:1
		displayModeAddHelper(Vector2i(256, 256), DisplayModeRating::Extra, "");
		displayModeAddHelper(Vector2i(512, 512), DisplayModeRating::Extra, "");
		displayModeAddHelper(Vector2i(1024, 1024), DisplayModeRating::Extra, "");
		displayModeAddHelper(Vector2i(2048, 2048), DisplayModeRating::Extra, "");
		displayModeAddHelper(Vector2i(4096, 4096), DisplayModeRating::Extra, "");
		displayModeAddHelper(Vector2i(8192, 8192), DisplayModeRating::Extra, "");
		displayModeAddHelper(Vector2i(16384, 16384), DisplayModeRating::Extra, "");

		// 32:27 (1.185:1)
		displayModeAddHelper(Vector2i(640, 540), DisplayModeRating::Esoteric, "DVCPRO HD");
		displayModeAddHelper(Vector2i(1280, 1080), DisplayModeRating::Esoteric, "DVCPRO HD");

		// 3:2 (1.50)
		displayModeAddHelper(Vector2i(240, 160), DisplayModeRating::Esoteric, "HQVGA");
		displayModeAddHelper(Vector2i(360, 240), DisplayModeRating::Esoteric, "WQVGA");
		displayModeAddHelper(Vector2i(480, 320), DisplayModeRating::Esoteric, "HVGA");
		displayModeAddHelper(Vector2i(720, 480), DisplayModeRating::Esoteric, "480p (3:2)", "WVGA");
		displayModeAddHelper(Vector2i(960, 640), DisplayModeRating::Esoteric, "DVGA");
		displayModeAddHelper(Vector2i(1152, 768), DisplayModeRating::Esoteric, "WXGA");
		displayModeAddHelper(Vector2i(1280, 864), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(1440, 960), DisplayModeRating::Esoteric, "FWXGA+");
		displayModeAddHelper(Vector2i(1600, 1024), DisplayModeRating::Esoteric, "WSXGA");
		displayModeAddHelper(Vector2i(1920, 1280), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2160, 1440), DisplayModeRating::Esoteric, "FHD+");
		displayModeAddHelper(Vector2i(2400, 1600), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2880, 1920), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(3240, 2160), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(3840, 2560), DisplayModeRating::Esoteric, "");

		// 25:16 (1.5625)
		displayModeAddHelper(Vector2i(3200, 2048), DisplayModeRating::Esoteric, "WQSXGA");

		// 5:3 (1.60)
		displayModeAddHelper(Vector2i(400, 240), DisplayModeRating::Esoteric, "WQVGA");
		displayModeAddHelper(Vector2i(800, 480), DisplayModeRating::Esoteric, "480p (5:3)", "WVGA");
		displayModeAddHelper(Vector2i(1024, 600), DisplayModeRating::Esoteric, "WSVGA");
		displayModeAddHelper(Vector2i(1280, 768), DisplayModeRating::Esoteric, "WXGA");

		// 30:17 (~1.76)
		displayModeAddHelper(Vector2i(480, 272), DisplayModeRating::Esoteric, "PSP display");

		// 9:5 (1.8:1)
		displayModeAddHelper(Vector2i(432, 240), DisplayModeRating::Esoteric, "FWQVGA");

		// (1.85:1)
		displayModeAddHelper(Vector2i(888, 480), DisplayModeRating::Esoteric, "480p (1.85:1)");
		displayModeAddHelper(Vector2i(1998, 1080), DisplayModeRating::Esoteric, "DCI 2K (flat cropped)");
		displayModeAddHelper(Vector2i(3996, 2160), DisplayModeRating::Esoteric, "DCI 4K (flat cropped)");

		// 256:135, ~19:10 (~1.8962∶1)
		displayModeAddHelper(Vector2i(2048, 1080), DisplayModeRating::Esoteric, "DCI 2K (native resolution)");
		displayModeAddHelper(Vector2i(4096, 2160), DisplayModeRating::Esoteric, "DCI 4k (full frame)");
		displayModeAddHelper(Vector2i(5120, 2700), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(8192, 4320), DisplayModeRating::Esoteric, "");

		// 18:9 (2.00:1)
		displayModeAddHelper(Vector2i(960, 480), DisplayModeRating::Esoteric, "FWVGA");
		displayModeAddHelper(Vector2i(1440, 720), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2160, 1080), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(2880, 1440), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(4320, 2160), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(5120, 2560), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(5760, 2880), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(8640, 4320), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(15360, 7680), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(16384, 8192), DisplayModeRating::Esoteric, "");

		// 37:18, 18.5:9 (2.05:1)
		displayModeAddHelper(Vector2i(2960, 1440), DisplayModeRating::Esoteric, "");

		// 38:18 (2.1:1)
		displayModeAddHelper(Vector2i(3040, 1440), DisplayModeRating::Esoteric, "");

		// 39:18, 19.5:9 (2.16:1)
		displayModeAddHelper(Vector2i(3120, 1440), DisplayModeRating::Esoteric, "");

		// (2.35:1)

		// 64:27 (2.370:1)
		displayModeAddHelper(Vector2i(2560, 1080), DisplayModeRating::Esoteric, "UW-HD");
		displayModeAddHelper(Vector2i(5120, 2160), DisplayModeRating::Esoteric, "UW 5K");
		displayModeAddHelper(Vector2i(7680, 3240), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(10240, 4320), DisplayModeRating::Esoteric, "UW 10K");
		displayModeAddHelper(Vector2i(15360, 6480), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(16640, 7020), DisplayModeRating::Esoteric, "");

		// 43:18 (2.38:1)
		displayModeAddHelper(Vector2i(3440, 1440), DisplayModeRating::Esoteric, "UW-UHD");

		// 1024:429 (~2.39:1)
		displayModeAddHelper(Vector2i(2048, 858), DisplayModeRating::Esoteric, "DCI 2K (CinemaScope cropped)");
		displayModeAddHelper(Vector2i(4096, 1716), DisplayModeRating::Esoteric, "DCI 4K (CinemaScope cropped)");
		displayModeAddHelper(Vector2i(8192, 3432), DisplayModeRating::Esoteric, "DCI 8K (CinemaScope cropped)");

		// 12:5 (2.40:1)
		displayModeAddHelper(Vector2i(3840, 1600), DisplayModeRating::Extra, "UW 4K (Ultra-wide 4K)", "WQHD+ (Wide Quad HD+)", "UW-QHD+ (Ultra-wide Quad HD+)");
		displayModeAddHelper(Vector2i(5760, 2400), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(7680, 3200), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(10368, 4320), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(15360, 6400), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(16200, 6750), DisplayModeRating::Esoteric, "");

		// 32:9 (3.5:1, 3.55:1)
		displayModeAddHelper(Vector2i(3840, 1080), DisplayModeRating::Extra, "32:9 4K", "Super ultrawide 1080p", "DFHD (Dual Full HD)");
		displayModeAddHelper(Vector2i(5120, 1440), DisplayModeRating::Extra, "32:9 5K", "Super ultrawide 1440p");
		displayModeAddHelper(Vector2i(5360, 4320), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(7680, 2160), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(15360, 4320), DisplayModeRating::Esoteric, "");
		displayModeAddHelper(Vector2i(16640, 4680), DisplayModeRating::Esoteric, "");

		// 32:10 (3.20)
		displayModeAddHelper(Vector2i(3840, 1200), DisplayModeRating::Extra, "29:9 4K", "Super ultrawide 1200p");

		// 36:10 (3.6:1)
		displayModeAddHelper(Vector2i(4320, 1200), DisplayModeRating::Extra, "36:10 4K", "Ultra-widescreen 1200p");
		displayModeAddHelper(Vector2i(5760, 1600), DisplayModeRating::Extra, "36:10 5K", "Ultra-widescreen 1600p");
		displayModeAddHelper(Vector2i(16200, 4500), DisplayModeRating::Esoteric, "");

		// 16:9 Triple monitor
		displayModeAddHelper(Vector2i(3840, 720), DisplayModeRating::Extra, "Triple 720p");
		displayModeAddHelper(Vector2i(5760, 1080), DisplayModeRating::Extra, "Triple 1080p");
		displayModeAddHelper(Vector2i(7680, 1440), DisplayModeRating::Extra, "Triple 1440p");
		displayModeAddHelper(Vector2i(11520, 2160), DisplayModeRating::Extra, "Triple 4K UHD");

		// 16:10 Triple monitor
		displayModeAddHelper(Vector2i(3840, 800), DisplayModeRating::Extra, "Triple WXGA");
		displayModeAddHelper(Vector2i(5760, 1200), DisplayModeRating::Extra, "Triple WUXGA");
		displayModeAddHelper(Vector2i(7680, 1600), DisplayModeRating::Extra, "Triple WQXGA");
		displayModeAddHelper(Vector2i(11520, 2400), DisplayModeRating::Extra, "Triple WQUXGA");

		// \TODO: Interlaced / tv-formats
		// \TODO: Check the following:
		//1920, 540, // 1080i PAL/NTSC
		//704, 576, // 576p25, 576p50
		//720, 576, // 576p25, 576p50
		//768, 576, // PAL
		//352, 240, //Video CD
		//333, 480, //VHS, Video8, Umatic
		//350, 480, //Betamax
		//420, 480, //Super Betamax, Betacam
		//460, 480, //Betacam SP, Umatic SP, NTSC (Over-The-Air TV)
		//580, 480, //Super VHS, Hi8, LaserDisc
		//700, 480, //Enhanced Definition Betamax, Analog broadcast limit (NTSC)
		//768, 576, // Analog broadcast limit (PAL, SECAM)
		//500, 480 : Digital8
		//720, 480 : D-VHS, DVD, miniDV, Digital Betacam (NTSC)
		//720, 480 : Widescreen DVD (anamorphic) (NTSC)
		//720, 576 : D-VHS, DVD, miniDV, Digital8, Digital Betacam (PAL/SECAM)
		//720, 576 : Widescreen DVD (anamorphic) (PAL/SECAM)
		//1280, 720 : D-VHS, HD DVD, Blu-ray, HDV (miniDV)
		//1920, 1080 : HDV (miniDV), AVCHD, HD DVD, Blu-ray, HDCAM SR
		//15360, 8640 : 16K Digital Cinema
		//61440, 34560 : 64K Digital Cinema
	}
}


// Non-static toString(s)

UString DisplayMode::toString() const
{
	UString str;
	str.append2(resolution.x);
	str += "x";
	str.append2(resolution.y);
	if (refreshRate.hz != 0) {
		str += " @ ";
		str += refreshRate.toString();
	}
	if (names.size() > 0) {
		str += " (";
		str += toStringOnlyNames();
		str += ")";
	}
	return str;
}


UString DisplayMode::toStringOnlyResolution() const
{
	UString str;
	str.append2(resolution.x);
	str += "x";
	str.append2(resolution.y);
	return str;
}


UString DisplayMode::toStringWithoutHz() const
{
	UString str;
	str.append2(resolution.x);
	str += "x";
	str.append2(resolution.y);
	if (names.size() > 0) {
		str += " (";
		str += toStringOnlyNames();
		str += ")";
	}
	return str;
}


UString DisplayMode::toStringOnlyNames() const
{
	UString str;
	for (size_t i = 0; i < names.size(); ++i) {
		str += names[i];
		if (i != names.size() - 1) {
			str += ", ";
		}
	}
	return str;
}


UString DisplayMode::toStringDebug() const
{
	UString str("DisplayMode(");
	str += toString();
	str += ", ";
	str.append2(bitsPerPixel);
	str += "bpp, ";
	if (pixelFormat != PixelFormat::Unknown) {
		str += " (";
		str += pixelFormat.toString();
		str += ")";
	}
	if (rating != DisplayModeRating::Normal) {
		str += " [";
		str += displayModeRatingToString(rating);
		str += "]";
	}
	str += ")";
	return str;
}


} // End namespace Verso

