#include <Verso/Display/PixelFormat.hpp>
#include <SDL2/SDL.h>

namespace Verso {


PixelFormat::PixelFormat() :
	pixelFormatEnum(PixelFormat::Unknown)
{
}


PixelFormat::PixelFormat(PixelFormatEnum pixelFormatEnum) :
	pixelFormatEnum(pixelFormatEnum)
{
}


PixelFormat PixelFormat::sdlPixelFormatToPixelFormat(std::uint32_t sdlPixelFormat)
{
	switch (sdlPixelFormat) {
	case SDL_PIXELFORMAT_UNKNOWN:
		return  PixelFormat(PixelFormatEnum::Unknown);
	case SDL_PIXELFORMAT_INDEX1LSB:
		return  PixelFormat(PixelFormatEnum::Index1Lsb);
	case SDL_PIXELFORMAT_INDEX1MSB:
		return  PixelFormat(PixelFormatEnum::Index1Msb);
	case SDL_PIXELFORMAT_INDEX4LSB:
		return  PixelFormat(PixelFormatEnum::Index4Lsb);
	case SDL_PIXELFORMAT_INDEX4MSB:
		return  PixelFormat(PixelFormatEnum::Index4Msb);
	case SDL_PIXELFORMAT_INDEX8:
		return  PixelFormat(PixelFormatEnum::Index8);
	case SDL_PIXELFORMAT_RGB332:
		return  PixelFormat(PixelFormatEnum::Rgb332);
	case SDL_PIXELFORMAT_RGB444:
		return  PixelFormat(PixelFormatEnum::Rgb444);
	case SDL_PIXELFORMAT_RGB555:
		return  PixelFormat(PixelFormatEnum::Rgb555);
	case SDL_PIXELFORMAT_BGR555:
		return  PixelFormat(PixelFormatEnum::Bgr555);
	case SDL_PIXELFORMAT_ARGB4444:
		return  PixelFormat(PixelFormatEnum::Argb4444);
	case SDL_PIXELFORMAT_RGBA4444:
		return  PixelFormat(PixelFormatEnum::Rgba4444);
	case SDL_PIXELFORMAT_ABGR4444:
		return  PixelFormat(PixelFormatEnum::Abgr4444);
	case SDL_PIXELFORMAT_BGRA4444:
		return  PixelFormat(PixelFormatEnum::Bgra4444);
	case SDL_PIXELFORMAT_ARGB1555:
		return  PixelFormat(PixelFormatEnum::Argb1555);
	case SDL_PIXELFORMAT_RGBA5551:
		return  PixelFormat(PixelFormatEnum::Rgba5551);
	case SDL_PIXELFORMAT_ABGR1555:
		return  PixelFormat(PixelFormatEnum::Abrr1555);
	case SDL_PIXELFORMAT_BGRA5551:
		return  PixelFormat(PixelFormatEnum::Bgra5551);
	case SDL_PIXELFORMAT_RGB565:
		return  PixelFormat(PixelFormatEnum::Rgb565);
	case SDL_PIXELFORMAT_BGR565:
		return  PixelFormat(PixelFormatEnum::Bgr565);
	case SDL_PIXELFORMAT_RGB24:
		return  PixelFormat(PixelFormatEnum::Rgb24);
	case SDL_PIXELFORMAT_BGR24:
		return  PixelFormat(PixelFormatEnum::Bgr24);
	case SDL_PIXELFORMAT_RGB888:
		return  PixelFormat(PixelFormatEnum::Rgb888);
	case SDL_PIXELFORMAT_RGBX8888:
		return  PixelFormat(PixelFormatEnum::Rgbx8888);
	case SDL_PIXELFORMAT_BGR888:
		return  PixelFormat(PixelFormatEnum::Bgr888);
	case SDL_PIXELFORMAT_BGRX8888:
		return  PixelFormat(PixelFormatEnum::Bgrx8888);
	case SDL_PIXELFORMAT_ARGB8888:
		return  PixelFormat(PixelFormatEnum::Argb8888);
	case SDL_PIXELFORMAT_RGBA8888:
		return  PixelFormat(PixelFormatEnum::Rgba8888);
	case SDL_PIXELFORMAT_ABGR8888:
		return  PixelFormat(PixelFormatEnum::Abgr8888);
	case SDL_PIXELFORMAT_BGRA8888:
		return  PixelFormat(PixelFormatEnum::Bgra8888);
	case SDL_PIXELFORMAT_ARGB2101010:
		return  PixelFormat(PixelFormatEnum::Argb2101010);
	case SDL_PIXELFORMAT_YV12:
		return  PixelFormat(PixelFormatEnum::Yv12);
	case SDL_PIXELFORMAT_IYUV:
		return  PixelFormat(PixelFormatEnum::Iyuv);
	case SDL_PIXELFORMAT_YUY2:
		return  PixelFormat(PixelFormatEnum::Yuy2);
	case SDL_PIXELFORMAT_UYVY:
		return  PixelFormat(PixelFormatEnum::Uyvy);
	case SDL_PIXELFORMAT_YVYU:
		return  PixelFormat(PixelFormatEnum::Yvyu);
	default:
		return  PixelFormat(PixelFormatEnum::Unknown);
	}
}


UString PixelFormat::toString() const
{
	switch (pixelFormatEnum) {
	case Unknown:
		return "unknown";
	case Index1Lsb:
		return "index1Lsb";
	case Index1Msb:
		return "index1Msb";
	case Index4Lsb:
		return "index4Lsb";
	case Index4Msb:
		return "index4Msb";
	case Index8:
		return "index8";
	case Rgb332:
		return "rgb332";
	case Rgb444:
		return "rgb444";
	case Rgb555:
		return "rgb555";
	case Bgr555:
		return "bgr555";
	case Argb4444:
		return "argb4444";
	case Rgba4444:
		return "rgba4444";
	case Abgr4444:
		return "abgr4444";
	case Bgra4444:
		return "bgra4444";
	case Argb1555:
		return "argb1555";
	case Rgba5551:
		return "rgba5551";
	case Abrr1555:
		return "abrr1555";
	case Bgra5551:
		return "bgra5551";
	case Rgb565:
		return "rgb565";
	case Bgr565:
		return "bgr565";
	case Rgb24:
		return "rgb24";
	case Bgr24:
		return "bgr24";
	case Rgb888:
		return "rgb888";
	case Rgbx8888:
		return "rgbx8888";
	case Bgr888:
		return "bgr888";
	case Bgrx8888:
		return "bgrx8888";
	case Argb8888:
		return "argb8888";
	case Rgba8888:
		return "rgba8888";
	case Abgr8888:
		return "abgr8888";
	case Bgra8888:
		return "bgra8888";
	case Argb2101010:
		return "argb2101010";
	case Yv12:
		return "yv12";
	case Iyuv:
		return "iyuv";
	case Yuy2:
		return "yuy2";
	case Uyvy:
		return "uyvy";
	case Yvyu:
		return "yvyu";
	default:
		return "Unknown";
	}
}


UString PixelFormat::toStringDebug() const
{
	return UString("PixelFormat(")+toString()+")";
}


} // End namespace Verso


