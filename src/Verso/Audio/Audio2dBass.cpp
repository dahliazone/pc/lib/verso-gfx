
#include <Verso/Audio/Audio2dBass.hpp>
#include <Verso/System/string_convert.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/Logger.hpp>
#include <bass.h>

namespace Verso {


Audio2dBass::Audio2dBass() :
	created(false),
	noSound(false),
	frequency(0),
	stereo(false),
	audioDevice(0, AudioDeviceType::Unset, ""),
	music()
{
}


Audio2dBass& Audio2dBass::instance()
{
	static Audio2dBass audio2dBass;
	return audio2dBass;
}


Audio2dBass::~Audio2dBass()
{
	Audio2dBass::destroy();
}


std::vector<AudioDevice> Audio2dBass::getDevices()
{
	BASS_SetConfig(BASS_CONFIG_UNICODE, TRUE);

	std::vector<AudioDevice> devices;
	BASS_DEVICEINFO info;

	for (int i = 1; BASS_GetDeviceInfo(static_cast<unsigned int>(i), &info); i++) {
		if (info.flags & BASS_DEVICE_ENABLED) { // device is enabled
			bool defaultDevice = false;

			if (info.flags & BASS_DEVICE_DEFAULT) {
				defaultDevice = true;
			}

			if (info.flags & BASS_DEVICE_DEFAULTCOM) {
				// TODO: handle default communication device
			}

			bool initialized = false;
			if (info.flags & BASS_DEVICE_INIT) {
				initialized = true;
			}

			bool loopback = false;
			if (info.flags & BASS_DEVICE_LOOPBACK) {
				loopback = true;
			}

			AudioDeviceType type = AudioDeviceType::Unset;
			if (info.flags & BASS_DEVICE_TYPE_DIGITAL) {
				type = AudioDeviceType::Digital;
			}
			else if (info.flags & BASS_DEVICE_TYPE_DISPLAYPORT) {
				type = AudioDeviceType::DisplayPort;
			}
			else if (info.flags & BASS_DEVICE_TYPE_HANDSET) {
				type = AudioDeviceType::Handset;
			}
			else if (info.flags & BASS_DEVICE_TYPE_HDMI) {
				type = AudioDeviceType::Hdmi;
			}
			else if (info.flags & BASS_DEVICE_TYPE_HEADPHONES) {
				type = AudioDeviceType::Headphones;
			}
			else if (info.flags & BASS_DEVICE_TYPE_HEADSET) {
				type = AudioDeviceType::Headset;
			}
			else if (info.flags & BASS_DEVICE_TYPE_LINE) {
				type = AudioDeviceType::Line;
			}
			else if (info.flags & BASS_DEVICE_TYPE_MICROPHONE) {
				type = AudioDeviceType::Microphone;
			}
			else if (info.flags & BASS_DEVICE_TYPE_NETWORK) {
				type = AudioDeviceType::Network;
			}
			else if (info.flags & BASS_DEVICE_TYPE_SPDIF) {
				type = AudioDeviceType::Spdif;
			}
			else if (info.flags & BASS_DEVICE_TYPE_SPEAKERS) {
				type = AudioDeviceType::Speakers;
			}

			AudioDeviceTypeIo typeIo = audioDeviceTypeGetIo(type);

			UString deviceName("Unknown");
			if (info.name != nullptr) {
				deviceName.set(info.name);
			}
			UString driverName("Unknown");
			if (info.driver != nullptr) {
				driverName.set(info.driver);
			}
			devices.push_back(
						AudioDevice(i, type, deviceName, initialized, defaultDevice,
									typeIo.output, typeIo.input, loopback, driverName));
		}
	}

	return devices;
}


void Audio2dBass::create(
		bool noSound, std::int64_t deviceId, uint32_t frequency, uint32_t samples, bool stereo)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == false, "Already created!");

	BASS_SetConfig(BASS_CONFIG_UNICODE, TRUE);

	DWORD flags = 0;
	if (stereo == false) {
		flags |= BASS_DEVICE_MONO;
	}

	if (noSound == true) {
		deviceId = 0; // 0 = no sound
	}

	// BASS_DEVICE_16BITS      Limit the output to 16-bit.
	// BASS_DEVICE_MONO        Mono output.
	// BASS_DEVICE_STEREO      Limit the output to stereo, saving some CPU if the device has more speakers available.
	// BASS_DEVICE_FREQ        Set the device's output rate to freq, otherwise leave it as it is.
	// BASS_DEVICE_SOFTWARE    Disable hardware/fastpath output.


	// int freq                DSP frequency (samples per second); see Remarks for details
	// SDL_AudioFormat format  audio data format; see Remarks for details
	// Uint8 channels          number of separate sound channels: see Remarks for details
	// Uint8 silence           audio buffer silence value (calculated)
	// Uint16 samples          audio buffer size in samples (power of 2); see Remarks for details
	// Uint32 size             audio buffer size in bytes (calculated)


	if (!BASS_Init(deviceId, static_cast<DWORD>(frequency), flags, nullptr, nullptr)) {
		UString error("Can't create audio device. Reason: BASS error: " + bassErrorCodeToString(BASS_ErrorGetCode()));
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	std::vector<AudioDevice> audioDevices = getDevices();
	for (auto & audioDevice : audioDevices) {
		if (audioDevice.deviceId == deviceId) {
			this->audioDevice = audioDevice;
		}
	}

	this->noSound = noSound;
	this->frequency = frequency;
	this->stereo = stereo;
	created = true;
}


void Audio2dBass::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	BASS_Free();
	created = false;
}


bool Audio2dBass::isCreated() const
{
	return created;
}


void Audio2dBass::update()
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}
}


void Audio2dBass::loadMusic(const UString& name, const UString& fileName)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	VERSO_LOG_INFO("verso-gfx", "loading music from \""<<fileName<<"\"");

	DWORD channel = BASS_StreamCreateFile(FALSE, fileName.c_str(), 0, 0, BASS_STREAM_PRESCAN);
	if (!channel) {
		VERSO_FILENOTFOUND("verso-gfx", "Music file could not be loaded.", fileName.c_str());
	}

	QWORD lenghtInBytes = BASS_ChannelGetLength(channel, BASS_POS_BYTE);
	double lenghtInSeconds = BASS_ChannelBytes2Seconds(channel, lenghtInBytes);
	VERSO_LOG_INFO("verso-gfx", "Music length "<<lenghtInSeconds<<"s ("<<lenghtInBytes<<" bytes)");

	// Add the music to the music-map
	std::pair<std::map<UString, std::uint32_t>::iterator, bool> returnValue;
	returnValue = music.insert(std::make_pair(name, channel));
	VERSO_ASSERT("verso-gfx", returnValue.second == true);
}


void Audio2dBass::deleteMusic(const UString& name)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	(void)name;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dBass::playMusic(const UString& name, bool loop)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	auto it = music.find(name);
	VERSO_ASSERT("verso-gfx", it != music.end());
	if (BASS_ChannelPlay(it->second, loop) == FALSE) {
		UString error("Failed to play music. Reason: BASS error "+bassErrorCodeToString(BASS_ErrorGetCode()));
		VERSO_ERROR("verso-gfx", error.c_str(), name.c_str());
	}

	// \TODO: Start using these for timing!
	//BASS_ChannelSetPosition(music_channel, jump_to, BASS_POS_BYTE);
	//QWORD bytepos = BASS_ChannelGetPosition(music_channel, BASS_POS_BYTE);
	//double pos = BASS_ChannelBytes2Seconds(music_channel, bytepos);
	//millis = static_cast<float>(pos)*1000*demo_speed_x;
}


double Audio2dBass::getPositionSeconds(const UString& name)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");

	auto it = music.find(name);
	VERSO_ASSERT("verso-gfx", it != music.end());

	QWORD pos = BASS_ChannelGetPosition(it->second, BASS_POS_BYTE);
	return BASS_ChannelBytes2Seconds(it->second, pos);
}


double Audio2dBass::getDurationSeconds(const UString& name)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return 0.0f;
	}

	auto it = music.find(name);
	VERSO_ASSERT("verso-gfx", it != music.end());

	QWORD durationBytes = BASS_ChannelGetLength(it->second, BASS_POS_BYTE);
	return BASS_ChannelBytes2Seconds(it->second, durationBytes);
}


void Audio2dBass::rewind(const UString& name, double seconds)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	auto it = music.find(name);
	VERSO_ASSERT("verso-gfx", it != music.end());

	QWORD bytes = BASS_ChannelSeconds2Bytes(it->second, seconds);
	if (BASS_ChannelSetPosition(it->second, bytes, BASS_POS_BYTE) == FALSE) {
		UString error("Failed to seek music to position=");
		error.append2(seconds);
		error += "s (";
		error.append2(bytes);
		error += " bytes). Reason: BASS error "+bassErrorCodeToString(BASS_ErrorGetCode());
		VERSO_ERROR("verso-gfx", error.c_str(), name.c_str());
	}
}


void Audio2dBass::fadeInMusic(const UString& name, int loopCount, float fadeTime)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	(void)name; (void)loopCount; (void)fadeTime;
	VERSO_FAIL("verso-gfx"); // \TODO: implement

	//VERSO_ASSERT("verso-gfx", fadeTime >= 0);
	//std::map<UString, std::uint32_t>::iterator it = music.find(name);
	//VERSO_ASSERT("verso-gfx", it != music.end());

	//int resultValue = Mix_fadeInMusic(it->second, loopCount,
	//								  static_cast<unsigned int>(fadeTime * 1000.0f));
	//VERSO_ASSERT("verso-gfx", resultValue != -1);
}


void Audio2dBass::changeMusicFadeOutIn(const UString& newMusic, int loopCount, float fadeTime)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	(void)newMusic; (void)loopCount; (void)fadeTime;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dBass::startMusic()
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	if (BASS_Start() == FALSE) {
		UString error("Failed to start music. Reason: BASS error "+bassErrorCodeToString(BASS_ErrorGetCode()));
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}
}


void Audio2dBass::pauseMusic()
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	if (BASS_Pause() == FALSE) {
		UString error("Failed to pause music. Reason: BASS error "+bassErrorCodeToString(BASS_ErrorGetCode()));
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}
}


void Audio2dBass::stopMusic()
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	if (BASS_Stop() == FALSE) {
		UString error("Failed to stop music. Reason: BASS error "+bassErrorCodeToString(BASS_ErrorGetCode()));
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}
}


void Audio2dBass::fadeOutMusic(float fadeTime)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	(void)fadeTime;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


int Audio2dBass::setMusicVolume(int volume)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (this->noSound == true) {
		return 0;
	}

	(void)volume;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dBass::loadSfx(const UString& name, const UString& fileName)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	(void)name; (void)fileName;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dBass::deleteSfx(const UString& name)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	(void)name;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


int Audio2dBass::playSfx(const UString& name, int channel, int loops)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return -1;
	}

	(void)name; (void)channel; (void)loops;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


int Audio2dBass::setChannelVolume(int channel, int volume)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (this->noSound == true) {
		return 0;
	}

	(void)channel; (void)volume;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dBass::haltChannel(int channel)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	if (noSound == true) {
		return;
	}

	(void)channel;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


AudioDevice Audio2dBass::getDevice() const
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == true, "Instance must be created before using it!");
	return audioDevice;
}


// Private methods

UString Audio2dBass::bassErrorCodeToString(int bassErrorCode)
{
	switch (bassErrorCode) {
	case BASS_OK:
		return "BASS_OK";
	case BASS_ERROR_MEM:
		return "BASS_ERROR_MEM";
	case BASS_ERROR_FILEOPEN:
		return "BASS_ERROR_FILEOPEN";
	case BASS_ERROR_DRIVER:
		return "BASS_ERROR_DRIVER";
	case BASS_ERROR_BUFLOST:
		return "BASS_ERROR_BUFLOST";
	case BASS_ERROR_HANDLE:
		return "BASS_ERROR_HANDLE";
	case BASS_ERROR_FORMAT:
		return "BASS_ERROR_FORMAT";
	case BASS_ERROR_POSITION:
		return "BASS_ERROR_POSITION";
	case BASS_ERROR_INIT:
		return "BASS_ERROR_INIT";
	case BASS_ERROR_START:
		return "BASS_ERROR_START";
	//case BASS_ERROR_SSL: // \TODO: Maybe only supported in a newer version or some platforms?
	//	return "BASS_ERROR_SSL";
	case BASS_ERROR_ALREADY:
		return "BASS_ERROR_ALREADY";
	case BASS_ERROR_NOCHAN:
		return "BASS_ERROR_NOCHAN";
	case BASS_ERROR_ILLTYPE:
		return "BASS_ERROR_ILLTYPE";
	case BASS_ERROR_ILLPARAM:
		return "BASS_ERROR_ILLPARAM";
	case BASS_ERROR_NO3D:
		return "BASS_ERROR_NO3D";
	case BASS_ERROR_NOEAX:
		return "BASS_ERROR_NOEAX";
	case BASS_ERROR_DEVICE:
		return "BASS_ERROR_DEVICE";
	case BASS_ERROR_NOPLAY:
		return "BASS_ERROR_NOPLAY";
	case BASS_ERROR_FREQ:
		return "BASS_ERROR_FREQ";
	case BASS_ERROR_NOTFILE:
		return "BASS_ERROR_NOTFILE";
	case BASS_ERROR_NOHW:
		return "BASS_ERROR_NOHW";
	case BASS_ERROR_EMPTY:
		return "BASS_ERROR_EMPTY";
	case BASS_ERROR_NONET:
		return "BASS_ERROR_NONET";
	case BASS_ERROR_CREATE:
		return "BASS_ERROR_CREATE";
	case BASS_ERROR_NOFX:
		return "BASS_ERROR_NOFX";
	case BASS_ERROR_NOTAVAIL:
		return "BASS_ERROR_NOTAVAIL";
	case BASS_ERROR_DECODE:
		return "BASS_ERROR_DECODE";
	case BASS_ERROR_DX:
		return "BASS_ERROR_DX";
	case BASS_ERROR_TIMEOUT:
		return "BASS_ERROR_TIMEOUT";
	case BASS_ERROR_FILEFORM:
		return "BASS_ERROR_FILEFORM";
	case BASS_ERROR_SPEAKER:
		return "BASS_ERROR_SPEAKER";
	case BASS_ERROR_VERSION:
		return "BASS_ERROR_VERSION";
	case BASS_ERROR_CODEC:
		return "BASS_ERROR_CODEC";
	case BASS_ERROR_ENDED:
		return "BASS_ERROR_ENDED";
	case BASS_ERROR_BUSY:
		return "BASS_ERROR_BUSY";
	case BASS_ERROR_UNKNOWN:
		return "BASS_ERROR_UNKNOWN";
	default:
		{
			UString error("Unknown BASS error code(");
			error.append2(bassErrorCode);
			error += ")";
			return error;
		}
	}
}


} // End namespace Verso

