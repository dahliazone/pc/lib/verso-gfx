#include <Verso/Audio/Audio2dSdl2.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>
#include <Verso/System/string_convert.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/Logger.hpp>
#include <SDL2/SDL.h>

namespace Verso {


Audio2dSdl2::Audio2dSdl2() :
	created(false),
	noSound(false),
	frequency(0),
	stereo(false),
	audioDevice(0, AudioDeviceType::Unset, ""),
	music()
{
}


Audio2dSdl2& Audio2dSdl2::instance()
{
	static Audio2dSdl2 Audio2dSdl2;
	return Audio2dSdl2;
}


Audio2dSdl2::~Audio2dSdl2()
{
	destroy();
}


UString SdlAudioSpecToString(const SDL_AudioSpec& audioSpec)
{
	UString info("");
	info.append2(audioSpec.freq);
	info += " kHz, ";

	info.append2(static_cast<int>(SDL_AUDIO_BITSIZE(audioSpec.format)));
	info += " bit ";

	info.append2(audioSpec.format);
	if (audioSpec.channels == 1) {
		info += " mono";
	}
	else if (audioSpec.channels == 2) {
		info += " stereo";
	}
	else {
		info += " channels=";
		info.append2(audioSpec.channels);
	}

	if (SDL_AUDIO_ISBIGENDIAN(audioSpec.format)) {
		info += " big endian";
	}
	else {
		info += " little endian";
	}

	if (SDL_AUDIO_ISFLOAT(audioSpec.format)) {
		info += " float";
	}
	else {
		if (SDL_AUDIO_ISSIGNED(audioSpec.format)) {
			info += " signed int";
		}
		else {
			info += " unsigned int";
		}
	}

	info += ", silence=";
	info.append2(audioSpec.silence);

	info += ", audio buffer size in samples frames=";
	info.append2(audioSpec.samples);

	return info;
}


std::vector<AudioDevice> Audio2dSdl2::getDevices()
{
	std::vector<AudioDevice> devices;

	SDL_AudioSpec defaultSpec;
	char** name = new char*[1];
	int result = SDL_GetDefaultAudioInfo(name, &defaultSpec, false);
	if (result != 0) {
		UString error("Can't get default audio device. Reason: SDL2 error: ");
		error.append(SDL_GetError());
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}
	UString defaultAudioDeviceName;
	if (*name != nullptr) {
		defaultAudioDeviceName.set(*name);
	}
	SDL_free(*name);
	delete[] name;

	VERSO_LOG_INFO("verso-gfx", "Found default audio device: \""<<defaultAudioDeviceName<<"\" with " + SdlAudioSpecToString(defaultSpec));

	const int count = SDL_GetNumAudioDevices(0);
	for (int i = 0; i < count; ++i) {
		UString deviceName(SDL_GetAudioDeviceName(i, 0));
		bool defaultDevice = false;
		if (deviceName.equals(defaultAudioDeviceName)) {
			defaultDevice = true;
		}
		devices.emplace_back(i, AudioDeviceType::Speakers, deviceName, false, defaultDevice, true, false, false, "");
	}

	return devices;
}


void Audio2dSdl2::create(
	bool noSound, std::int64_t deviceId, uint32_t frequency, uint32_t samples, bool stereo,
	void (*audioCallback) (void* userData, std::uint8_t* stream, int len),
	void* callbackUserData)
{
	VERSO_ASSERT_MSG("verso-gfx", isCreated() == false, "Already created!");

	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Audio);

	UString deviceName;
	const char* deviceNameCStr = nullptr;
	int iscapture = 0;
	SDL_AudioSpec desiredFormat;
	SDL_zero(desiredFormat);
	desiredFormat.freq = frequency;
	desiredFormat.format = AUDIO_S16SYS;
	if (stereo == true) {
		desiredFormat.channels = 2;
	}
	else {
		desiredFormat.channels = 1;
	}
	desiredFormat.samples = samples;
	desiredFormat.callback = audioCallback;
	desiredFormat.userdata = callbackUserData;

	SDL_AudioSpec obtainedFormat;
	int allowedChanges = 0;
	//allowedChanges |= SDL_AUDIO_ALLOW_FREQUENCY_CHANGE;
	//allowedChanges |= SDL_AUDIO_ALLOW_FORMAT_CHANGE;
	//allowedChanges |= SDL_AUDIO_ALLOW_CHANNELS_CHANGE;
	//allowedChanges |= SDL_AUDIO_ALLOW_SAMPLES_CHANGE;
	//allowedChanges |= SDL_AUDIO_ALLOW_ANY_CHANGE;

	std::vector<AudioDevice> audioDevices = getDevices();
	if (deviceId != -1) {
		for (auto& audioDevice : audioDevices) {
			if (audioDevice.deviceId == deviceId) {
				deviceName = audioDevice.name;
				deviceNameCStr = deviceName.c_str();
			}
		}
	}
	else {
		deviceNameCStr = nullptr; // nullptr open the default device that what requested with deviceId == -1
	}

	if (deviceNameCStr != nullptr) {
		VERSO_LOG_INFO_VARIABLE("verso-gfx", "Opening device", deviceNameCStr);
	}
	else {
		UString deviceStr("null -> so should be: ");
		for (auto& audioDevice : audioDevices) {
			if (audioDevice.defaultDevice == true) {
				deviceStr += audioDevice.name;
			}
		}
		VERSO_LOG_INFO_VARIABLE("verso-gfx", "Opening device", deviceStr.c_str());
	}

	SDL_AudioDeviceID sdlDeviceId = SDL_OpenAudioDevice(deviceNameCStr, iscapture, &desiredFormat, &obtainedFormat, allowedChanges);
	if (sdlDeviceId < 1) {
		UString error("Can't create audio device. Reason: SDL2 error: ");
		error.append(SDL_GetError());
		VERSO_ERROR("verso-gfx", error.c_str(), "");
	}

	for (auto& audioDevice : audioDevices) {
		if (audioDevice.deviceId == deviceId) {
			this->audioDevice = audioDevice;
			this->audioDevice.deviceId = sdlDeviceId;
			this->audioDevice.bitsPerSample = static_cast<int>(SDL_AUDIO_BITSIZE(obtainedFormat.format));
			this->audioDevice.channels = static_cast<int>(obtainedFormat.channels);
			this->audioDevice.sampleRate = obtainedFormat.freq;
			this->audioDevice.integer = SDL_AUDIO_ISINT(obtainedFormat.format);
			this->audioDevice.unsignedint = SDL_AUDIO_ISUNSIGNED(obtainedFormat.format);
			this->audioDevice.littleEndian = SDL_AUDIO_ISLITTLEENDIAN(obtainedFormat.format);
			this->audioDevice.bufferLength = obtainedFormat.samples;
		}
	}

	VERSO_LOG_INFO_VARIABLE("verso-gfx", "device name", this->audioDevice.name);
	VERSO_LOG_INFO("verso-gfx", "Audio2dSdl2 obtained format:");
	VERSO_LOG_INFO_VARIABLE("verso-gfx", "bitsPerSample", this->audioDevice.bitsPerSample);
	VERSO_LOG_INFO_VARIABLE("verso-gfx", "channels", this->audioDevice.channels);
	VERSO_LOG_INFO_VARIABLE("verso-gfx", "sampleRate", this->audioDevice.sampleRate);
	VERSO_LOG_INFO_VARIABLE("verso-gfx", "type", (this->audioDevice.integer ? "int" : "float"));
	VERSO_LOG_INFO_VARIABLE("verso-gfx", "subtype", (this->audioDevice.unsignedint ? "unsigned" : "signed"));
	VERSO_LOG_INFO_VARIABLE("verso-gfx", "bufferLength", this->audioDevice.bufferLength);

	this->noSound = noSound;
	this->frequency = this->audioDevice.sampleRate;
	this->stereo = stereo;
	created = true;
}


void Audio2dSdl2::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	SDL_CloseAudio();
	created = false;
}


bool Audio2dSdl2::isCreated() const
{
	return created;
}


void Audio2dSdl2::update()
{
}


void Audio2dSdl2::loadMusic(const UString& name, const UString& fileName)
{
	/*
	VERSO_LOG_INFO("verso-gfx", "loading music from \""<<fileName<<"\"");

	DWORD channel = BASS_StreamCreateFile(FALSE, fileName.c_str(), 0, 0, BASS_STREAM_PRESCAN);
	if (!channel) {
		VERSO_FILENOTFOUND("verso-gfx", "Music file could not be loaded.", fileName.c_str());
	}

	QWORD lenghtInBytes = BASS_ChannelGetLength(channel, BASS_POS_BYTE);
	double lenghtInSeconds = BASS_ChannelBytes2Seconds(channel, lenghtInBytes);
	VERSO_LOG_INFO("verso-gfx", "Music length "<<lenghtInSeconds<<"s ("<<lenghtInBytes<<" bytes)");

	// Add the music to the music-map
	std::pair<std::map<UString, std::uint32_t>::iterator, bool> returnValue;
	returnValue = music.insert(std::make_pair(name, channel));
	VERSO_ASSERT("verso-gfx", returnValue.second == true);
	*/
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dSdl2::deleteMusic(const UString& name)
{
	(void)name;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dSdl2::playMusic(const UString& name, bool loop)
{
	//auto it = music.find(name);
	//VERSO_ASSERT("verso-gfx", it != music.end());
	//BASS_ChannelPlay(it->second, loop);
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


double Audio2dSdl2::getPositionSeconds(const UString& name)
{
	//auto it = music.find(name);
	//VERSO_ASSERT("verso-gfx", it != music.end());

	//QWORD pos = BASS_ChannelGetPosition(it->second, BASS_POS_BYTE);
	//return BASS_ChannelBytes2Seconds(it->second, pos);
	VERSO_FAIL("verso-gfx"); // \TODO: implement
	return 0.0f;
}


double Audio2dSdl2::getDurationSeconds(const UString& name)
{
	//auto it = music.find(name);
	//VERSO_ASSERT("verso-gfx", it != music.end());

	//QWORD durationBytes = BASS_ChannelGetLength(it->second, BASS_POS_BYTE);
	//return BASS_ChannelBytes2Seconds(it->second, durationBytes);

	VERSO_FAIL("verso-gfx"); // \TODO: implement
	return 0.0f;
}


void Audio2dSdl2::rewind(const UString& name, double seconds)
{
	/*auto it = music.find(name);
	VERSO_ASSERT("verso-gfx", it != music.end());

	QWORD bytes = BASS_ChannelSeconds2Bytes(it->second, seconds);
	if (BASS_ChannelSetPosition(it->second, bytes, BASS_POS_BYTE) == FALSE) {
		UString error("Failed to seek music to position=");
		error.append2(seconds);
		error += "s (";
		error.append2(bytes);
		error += " bytes). Reason: BASS error "+bassErrorCodeToString(BASS_ErrorGetCode());
		VERSO_ILLEGALPARAMETERS("verso-gfx", error.c_str(), name.c_str());
	}*/
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dSdl2::fadeInMusic(const UString& name, int loopCount, float fadeTime)
{
	(void)name; (void)loopCount; (void)fadeTime;
	VERSO_FAIL("verso-gfx"); // \TODO: implement

	//VERSO_ASSERT("verso-gfx", fadeTime >= 0);
	//std::map<UString, std::uint32_t>::iterator it = music.find(name);
	//VERSO_ASSERT("verso-gfx", it != music.end());

	//int resultValue = Mix_fadeInMusic(it->second, loopCount,
	//								  static_cast<unsigned int>(fadeTime * 1000.0f));
	//VERSO_ASSERT("verso-gfx", resultValue != -1);
}


void Audio2dSdl2::changeMusicFadeOutIn(const UString& newMusic, int loopCount, float fadeTime)
{
	(void)newMusic; (void)loopCount; (void)fadeTime;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dSdl2::startMusic()
{
	SDL_PauseAudioDevice(audioDevice.deviceId, 0);
}


void Audio2dSdl2::pauseMusic()
{
	SDL_PauseAudioDevice(audioDevice.deviceId, 1);
}


void Audio2dSdl2::stopMusic()
{
	//BASS_Stop();
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dSdl2::fadeOutMusic(float fadeTime)
{
	(void)fadeTime;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


int Audio2dSdl2::setMusicVolume(int volume)
{
	(void)volume;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dSdl2::loadSfx(const UString& name, const UString& fileName)
{
	(void)name; (void)fileName;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dSdl2::deleteSfx(const UString& name)
{
	(void)name;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


int Audio2dSdl2::playSfx(const UString& name, int channel, int loops)
{
	(void)name; (void)channel; (void)loops;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


int Audio2dSdl2::setChannelVolume(int channel, int volume)
{
	(void)channel; (void)volume;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


void Audio2dSdl2::haltChannel(int channel)
{
	(void)channel;
	VERSO_FAIL("verso-gfx"); // \TODO: implement
}


AudioDevice Audio2dSdl2::getDevice() const
{
	return audioDevice;
}


} // End namespace Verso
