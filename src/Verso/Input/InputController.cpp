
#include <Verso/Input/InputController.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/Logger.hpp>

// \TODO: check through code to match Verso standards!!!

namespace Verso {


InputController::InputController(const UString& name) :
	mouseEvents(),
	mouseX(-1),
	mouseY(-1),
	mouseMotionX(0),
	mouseMotionY(0),
	mouseMotionXRel(0.0f),
	mouseMotionYRel(0.0f),
	mouseButtonState(),
	mouseWheelMotionX(0),
	mouseWheelMotionY(0),
	mouseWheelMotionPreciseX(0.0f),
	mouseWheelMotionPreciseY(0.0f),
	mouseWheelMotionXRel(0.0f),
	mouseWheelMotionYRel(0.0f),

	name(name),
	keyboardToButton(),
	//mouseAxisToAxis(),
	mouseButtonToButton(),
	gameControllerButtonToAxis(),
	gameControllerButtonToButton(),
	joystickAxisToAxis(),
	joystickButtonToButton(),
	nextAxisHandle(1),
	axisHandles(),
	nextButtonHandle(1),
	buttonHandles(),
	axis(),
	buttons(),
	buttonsPressedDown(),
	buttonsReleasedUp()
{
	axis.push_back(0.0f);
	buttons.push_back(WeightedButtonState());
	buttonsPressedDown.push_back(false);
	buttonsReleasedUp.push_back(false);
}


InputController::~InputController()
{
}


AxisHandle InputController::getAxisHandle(const UString& axisName)
{
	auto it = axisHandles.find(axisName);
	if (it == axisHandles.end()) {
		axisHandles[axisName] = nextAxisHandle;
		axis.push_back(0.0f);
		return nextAxisHandle++;
	}
	else {
		return it->second;
	}
}


ButtonHandle InputController::getButtonHandle(const UString& buttonName)
{
	auto it = buttonHandles.find(buttonName);
	if (it == buttonHandles.end()) {
		buttonHandles[buttonName] = nextButtonHandle;
		buttons.push_back(WeightedButtonState());
		buttonsPressedDown.push_back(false);
		buttonsReleasedUp.push_back(false);
		return nextButtonHandle++;
	}
	else {
		return it->second;
	}
}


void InputController::processEvent(const Event& event)
{
	//VERSO_LOG_DEBUG("verso-gfx", "Processing event: "<<event.toString());

	// \TODO: abstract event input system?
	// \TODO: include windowId & timestamp

	switch (event.type)
	{
		case EventType::Keyboard:
		{
			const KeyboardEvent& keyboard = event.keyboard;
			if (keyboard.type == KeyboardEventType::Key) {
				//VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "Processing key: ", keySymbolToString(keyboard.key));

				// Check for released keymodifiers
				if (keyboard.state == ButtonState::Released) {
					for (auto it = keyboardToButton.begin(); it != keyboardToButton.end(); ++it) {
						std::map<Keymodifiers, ButtonHandle>& keyModsMap = it->second;
						for (auto itMod = keyModsMap.begin(); itMod != keyModsMap.end(); ++itMod) {
							ButtonHandle buttonHandle = itMod->second;
							if (getButtonState(buttonHandle) == ButtonState::Pressed || getButtonPressedDown(buttonHandle) == true) {
								if (keymodifiersContainKeyCode(itMod->first, keyboard.key.keyCode)) {
									buttons[buttonHandle] = buttonStateToWeightedButtonState(ButtonState::Released);
									buttonsPressedDown[buttonHandle] = false;
									buttonsReleasedUp[buttonHandle] = true;
								}
							}
						}
					}
				}

				auto it = keyboardToButton.find(keyboard.key.keyCode);
				if  (it != keyboardToButton.end()) {
					//VERSO_LOG_DEBUG("verso-gfx", "  => Found keyCode! now trying to match modifiers...");

					std::map<Keymodifiers, ButtonHandle>& keyModsMap = it->second;
					for (auto itMod = keyModsMap.begin(); itMod != keyModsMap.end(); ++itMod) {
						//VERSO_LOG_DEBUG("verso-gfx", "       => checking:"
						//                <<keymodifiersToString(keyboard.key.keymodifiers, false)<<"\"("<<(keyboard.key.keymodifiers)
						//                <<") against \""
						//                <<keymodifiersToString(itMod->first, false)<<"\"("<<(itMod->first)<<"): ");
						if (keyModifiersMatchRules(keyboard.key.keymodifiers, itMod->first)) {
							//VERSO_LOG_DEBUG("verso-gfx", "          %%%=> MATCHES!");
							ButtonHandle buttonHandle = itMod->second;
							buttons[buttonHandle] = buttonStateToWeightedButtonState(keyboard.state);
							if (keyboard.state == ButtonState::Released) {
								buttonsPressedDown[buttonHandle] = false;
								buttonsReleasedUp[buttonHandle] = true;
							}
							else {
								buttonsPressedDown[buttonHandle] = true;
								buttonsReleasedUp[buttonHandle] = false;
							}
							//uint8_t repeat; // non-zero if this is a key repeat // \TODO: use here also?
						}
						//else
						//	VERSO_LOG_DEBUG("verso-gfx", "          %%%=> NOPE!");
					}
				}
				//else {
				//	VERSO_LOG_DEBUG("verso-gfx", "  => didn't find it from:");
				//	for (auto it = keyboardToButton.begin(); it != keyboardToButton.end(); ++it) {
				//		for (auto itMod = it->second.begin(); itMod != it->second.end(); ++itMod) {
				//			KeyCode keyCode = it->first;
				//			Keymodifiers keymods = itMod->first;
				//			VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "", keyCodeAndKeymodifiersToString(keyCode, keymods).c_str());
				//		}
				//	}
				//}
			}
			break;
		}

		case EventType::Mouse:
		{
			// \TODO: Take windowId and mouseId to account here!
			const MouseEvent& mouse = event.mouse;
			if (mouse.type == MouseEventType::Motion) {
				mouseX = mouse.x; //int32_t
				mouseY = mouse.y;
				mouseMotionX = mouse.motionX;
				mouseMotionY = mouse.motionY;
				mouseMotionXRel = mouse.motionXRel;
				mouseMotionYRel = mouse.motionYRel;
			}

			else if (mouse.type == MouseEventType::Button) {
				for (int i = 0; i < static_cast<int>(MouseButton::Size); ++i) {
					mouseButtonState[i] = mouse.mouseButtons[i];
				}
			}

			else if (mouse.type == MouseEventType::Wheel) {
				mouseWheelMotionX = mouse.wheelMotionX;
				mouseWheelMotionY = mouse.wheelMotionY;
				mouseWheelMotionPreciseX = mouse.wheelMotionPreciseX;
				mouseWheelMotionPreciseY = mouse.wheelMotionPreciseY;
				mouseWheelMotionXRel = mouse.wheelMotionXRel;
				mouseWheelMotionYRel = mouse.wheelMotionYRel;
			}
			break;
		}

		case EventType::GameController:
		{
			//const GameControllerEvent& gameController = event.gameController;
			//VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_case__EventType_GameController
			break;
		}

		case EventType::Joystick:
		{
			const JoystickEvent& joystick = event.joystick;

			if (joystick.type == JoystickEventType::AxisMotion) {
				auto it = joystickAxisToAxis.find(joystick.axis);
				if  (it != joystickAxisToAxis.end()) {
					AxisHandle axisHandle = it->second;
					axis[axisHandle] = joystick.value;
				}
			}

			else if (joystick.type == JoystickEventType::Button) {
				auto it = joystickButtonToButton.find(joystick.button);
				if  (it != joystickButtonToButton.end()) {
					ButtonHandle buttonHandle = it->second;
					buttons[buttonHandle] = buttonStateToWeightedButtonState(joystick.state); // \TODO: support weighted buttons!
					if (joystick.state == ButtonState::Released) {
						buttonsPressedDown[buttonHandle] = false;
						buttonsReleasedUp[buttonHandle] = true;
					}
					else {
						buttonsPressedDown[buttonHandle] = true;
						buttonsReleasedUp[buttonHandle] = false;
					}
				}
			}
			break;
		}

		case EventType::GainedFocus:
			break;

		case EventType::LostFocus:
			break;

		case EventType::Resized:
			break;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Unknown EventType="<<event.type);
			break;
		}
	}
}


void InputController::resetBoth()
{
	VERSO_ASSERT("verso-gfx", buttonsPressedDown.size() == buttonsReleasedUp.size());
	for (size_t i=0; i<buttonsPressedDown.size(); ++i) {
		buttonsPressedDown[i] = false;
		buttonsReleasedUp[i] = false;
	}
}


void InputController::resetPressedDown()
{
	for (size_t i=0; i<buttonsPressedDown.size(); ++i) {
		buttonsPressedDown[i] = false;
	}
}


void InputController::resetReleasedUp()
{
	for (size_t i=0; i<buttonsReleasedUp.size(); ++i) {
		buttonsReleasedUp[i] = false;
	}
}


AxisState InputController::getAxisState(AxisHandle axisHandle) const
{
	if (axisHandle >= nextAxisHandle) {
		UString resources("axisHandle=");
		resources.append2(axisHandle);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'axisHandle' is not valid", resources.c_str());
	}
	return axis[axisHandle];
}


ButtonState InputController::getButtonState(ButtonHandle buttonHandle) const
{
	return getWeightedButtonState(buttonHandle).state;
}


WeightedButtonState InputController::getWeightedButtonState(ButtonHandle buttonHandle) const
{
	if (buttonHandle >= nextButtonHandle) {
		UString resources("buttonHandle=");
		resources.append2(buttonHandle);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'buttonHandle' is not valid", resources.c_str());
	}
	return buttons[buttonHandle];
}


bool InputController::getButtonPressedDown(ButtonHandle buttonHandle) const
{
	if (buttonHandle >= nextButtonHandle) {
		UString resources("buttonHandle=");
		resources.append2(buttonHandle);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'buttonHandle' is not valid", resources.c_str());
	}
	return buttonsPressedDown[buttonHandle];
}


bool InputController::getButtonReleasedUp(ButtonHandle buttonHandle) const
{
	if (buttonHandle >= nextButtonHandle) {
		UString resources("buttonHandle=");
		resources.append2(buttonHandle);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'buttonHandle' is not valid", resources.c_str());
	}
	return buttonsReleasedUp[buttonHandle];
}


void InputController::setButtonState(ButtonHandle buttonHandle, WeightedButtonState state)
{
	if (buttonHandle >= this->nextButtonHandle) {
		UString resources("buttonHandle=");
		resources.append2(buttonHandle);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'buttonHandle' is not valid", resources.c_str());
	}
	this->buttons[buttonHandle] = state;
}


void InputController::setButtonPressed(ButtonHandle buttonHandle, ButtonState pressed)
{
	if (buttonHandle >= this->nextButtonHandle) {
		UString resources("buttonHandle=");
		resources.append2(buttonHandle);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'buttonHandle' is not valid", resources.c_str());
	}
	this->buttons[buttonHandle] = WeightedButtonState(pressed);
}


ButtonHandle InputController::mapKeyboardToButton(const UString& buttonName, KeyCode key, Keymodifiers modifiers, bool alwaysNoneOrAnyNumCapsMode)
{
	ButtonHandle buttonHandle = getButtonHandle(buttonName);

	if (alwaysNoneOrAnyNumCapsMode == true) {
		modifiers |= static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode);
	}

	auto it = keyboardToButton.find(key);
	if  (it == keyboardToButton.end()) {
		std::map<Keymodifiers, ButtonHandle> newMap;
		newMap[modifiers] = buttonHandle;
		keyboardToButton[key] = newMap;
	}
	else {
		std::map<Keymodifiers, ButtonHandle>& current = it->second;
		auto itMod = current.find(modifiers);
		if (itMod == current.end()) {
			current[modifiers] = buttonHandle;
		}
		else {
			VERSO_LOG_WARN("verso-gfx", "Replacing assigned 'key'+'modifier' with new 'buttonName'");
			VERSO_LOG_WARN_VARIABLE("verso-gfx", "buttonName", buttonName.c_str());
			VERSO_LOG_WARN_VARIABLE("verso-gfx", "key", keyCodeToString(key).c_str());
			VERSO_LOG_WARN_VARIABLE("verso-gfx", "modifiers", keymodifiersToString(modifiers).c_str());
			current[modifiers] = buttonHandle;
		}
	}

	return buttonHandle;
}


//AxisHandle InputController::mapMouseAxisToAxis(const UString& axisName, MouseAxis mouseAxis) {}


ButtonHandle InputController::mapMouseButtonToButton(const UString& buttonName, MouseButton mouseButton)
{
	(void)mouseButton; (void)buttonName;
	VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_method__InputController_mapMouseButtonToButton
}


AxisHandle InputController::mapGameControllerButtonToAxis(const UString& axisName, GameControllerAxis gameControllerAxis)
{
	(void)gameControllerAxis; (void)axisName;
	VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_method__InputController_mapGameControllerButtonToAxis
}


ButtonHandle InputController::mapGameControllerButtonToButton(const UString& buttonName, GameControllerButton gameControllerButton)
{
	(void)gameControllerButton; (void)buttonName;
	VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_method__InputController_mapGameControllerButtonToButton
}


UString InputController::findAxisNameByAxisType(JoystickAxis joystickAxis)
{
	//JoystickAxis -> AxisHandle -> UString
	//std::map<JoystickAxis, AxisHandle> joystickAxisToAxis;
	//std::map<UString, AxisHandle> axisHandles;

	const auto& itAxis = joystickAxisToAxis.find(joystickAxis);
	if (itAxis == joystickAxisToAxis.end()) {
		UString resources("JoystickAxis="+joystickAxisToString(joystickAxis));
		VERSO_OBJECTNOTFOUND("verso-gfx", "Given JoystickAxis not mapped.", resources.c_str());
	}
	const AxisHandle& axisHandle = itAxis->second;

	for (auto itAxisHandle = axisHandles.begin(); itAxisHandle != axisHandles.end(); ++itAxisHandle) {
		if (itAxisHandle->second == axisHandle) {
			return itAxisHandle->first;
		}
	}

	UString resources("JoystickAxis=" + joystickAxisToString(joystickAxis));
	VERSO_OBJECTNOTFOUND("verso-gfx", "Given JoystickAxis not mapped.", resources.c_str());
}


UString InputController::findButtonNameByButtonType(JoystickButton joystickButton)
{
	const auto& itButton = joystickButtonToButton.find(joystickButton);
	if (itButton == joystickButtonToButton.end()) {
		UString resources("JoystickButton="+joystickButtonToString(joystickButton));
		VERSO_OBJECTNOTFOUND("verso-gfx", "Given JoystickButton not mapped.", resources.c_str());
	}
	const ButtonHandle& buttonHandle = itButton->second;

	for(auto itButtonHandle = buttonHandles.begin(); itButtonHandle != buttonHandles.end(); ++itButtonHandle) {
		if (itButtonHandle->second == buttonHandle) {
			return itButtonHandle->first;
		}
	}

	UString resources("JoystickButton=" + joystickButtonToString(joystickButton));
	VERSO_OBJECTNOTFOUND("verso-gfx", "Given JoystickButton not mapped.", resources.c_str());
}


AxisHandle InputController::mapJoystickAxisToAxis(const UString& axisName, JoystickAxis joystickAxis)
{
	AxisHandle axisHandle = getAxisHandle(axisName);

	auto it = joystickAxisToAxis.find(joystickAxis);
	if  (it == joystickAxisToAxis.end()) {
		joystickAxisToAxis[joystickAxis] = axisHandle;
	}
	else {
		VERSO_LOG_WARN("verso-gfx", "Replacing already defined joystick axis to a new name");
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "axisName", axisName);
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "joystickAxis", joystickAxisToString(joystickAxis));
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "previous axisName", findAxisNameByAxisType(joystickAxis));
		joystickAxisToAxis[joystickAxis] = axisHandle;
	}

	return axisHandle;
}


ButtonHandle InputController::mapJoystickButtonToButton(const UString& buttonName, JoystickButton joystickButton)
{
	ButtonHandle buttonHandle = getButtonHandle(buttonName);

	auto it = joystickButtonToButton.find(joystickButton);
	if  (it == joystickButtonToButton.end()) {
		joystickButtonToButton[joystickButton] = buttonHandle;
	}
	else {
		VERSO_LOG_WARN("verso-gfx", "Replacing already defined joystick button to a new name");
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "buttonName", buttonName);
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "joystickButton", joystickButtonToString(joystickButton));
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "previous buttonName", findButtonNameByButtonType(joystickButton));
		joystickButtonToButton[joystickButton] = buttonHandle;
	}

	return buttonHandle;
}


ButtonHandle InputController::mapJoystickHatToButton(const UString& buttonName, JoystickHat joystickHat, JoystickHatDirection joystickDirection)
{
	(void)joystickHat; (void)joystickDirection; (void)buttonName;
	VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_method__InputController_mapJoystickHatToButton
}


} // End namespace Verso

