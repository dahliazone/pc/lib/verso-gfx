#include <Verso/Input/Keyboard/KeyCodeConverterSdl2.hpp>
#include <Verso/System/assert.hpp>
#include <Verso/System/Exception/ErrorException.hpp>
#include <SDL2/SDL.h>

namespace Verso {


KeyCodeConverterSdl2::KeyCodeConverterSdl2() :
	sdlToVersoKeyCodesMap(),
	sdlToVersoKeymodifiersMap(),
	sdlToVersoScancodesMap(),
	keyCodeToNameMap(),
	nameToKeyCodeMap(),
	keymodifierToNameMap(),
	nameToKeymodifierMap(),
	keyScancodeToNameMap(),
	nameToKeyScancodeMap()
{
	createSdlToVersoKeyCodesMap();
	createSdlToVersoKeymodifiersMap();
	createSdlToVersoKeyScancodesMap();

	createNamesToKeyCodesMap();
	createKeyCodesToNamesMap();

	createNamesToKeymodifierMap();
	createKeymodifierToNamesMap();

	createNamesToKeyScancodeMap();
	createKeyScancodeToNamesMap();
}


KeyCodeConverterSdl2& KeyCodeConverterSdl2::instance()
{
	static KeyCodeConverterSdl2 keyCodeConverterSdl2;
	return keyCodeConverterSdl2;
}


KeyCodeConverterSdl2::~KeyCodeConverterSdl2()
{
	nameToKeyCodeMap.clear();
}


KeyCode KeyCodeConverterSdl2::sdlToVersoKeyCode(SDL_Keycode sdlKeycode)
{
	return sdlToVersoKeyCodesMap[sdlKeycode];
}


KeyScancode KeyCodeConverterSdl2::sdlToVersoKeyScancode(SDL_Scancode sdlScancode)
{
	return sdlToVersoScancodesMap[sdlScancode];
}


Keymodifiers KeyCodeConverterSdl2::sdlToVersoKeymodifiers(Uint16 sdlKeymod)
{
	return static_cast<Keymodifiers>(sdlKeymod);
}


KeyCode KeyCodeConverterSdl2::nameToKeyCode(const UString& name)
{
	std::map<UString, KeyCode>::const_iterator it =
		nameToKeyCodeMap.find(name);
	VERSO_ASSERT("verso-gfx", it != nameToKeyCodeMap.end());
	VERSO_ASSERT("verso-gfx", it->second != KeyCode::Unset);

	return it->second;
}


UString KeyCodeConverterSdl2::keyCodeToName(KeyCode key)
{
	auto it = keyCodeToNameMap.find(key);
	if (it != keyCodeToNameMap.end()) {
		return keyCodeToNameMap[key];
	}
	else {
		UString str;
		str += "Unknown(";
		str.append2(static_cast<std::uint32_t>(key));
		str += ")";
		return str;
	}
}


Keymodifier KeyCodeConverterSdl2::nameToKeymodifier(const UString& name)
{
	std::map<UString, Keymodifier>::const_iterator it =
		nameToKeymodifierMap.find(name);
	VERSO_ASSERT("verso-gfx", it != nameToKeymodifierMap.end());
	VERSO_ASSERT("verso-gfx", it->second != Keymodifier::Unset);

	return it->second;
}


UString KeyCodeConverterSdl2::keymodifierToName(Keymodifier keymodifier)
{
	return keymodifierToNameMap[keymodifier];
}


KeyScancode KeyCodeConverterSdl2::nameToKeyScancode(const UString& name)
{
	std::map<UString, KeyScancode>::const_iterator it =
		nameToKeyScancodeMap.find(name);
	VERSO_ASSERT("verso-gfx", it != nameToKeyScancodeMap.end());
	VERSO_ASSERT("verso-gfx", it->second != KeyScancode::Unset);

	return it->second;
}


UString KeyCodeConverterSdl2::keyScancodeToName(KeyScancode keyScancode)
{
	auto it = keyScancodeToNameMap.find(keyScancode);
	if (it != keyScancodeToNameMap.end()) {
		return keyScancodeToNameMap[keyScancode];
	}
	else {
		UString str;
		str += "Unknown(";
		str.append2(static_cast<std::uint32_t>(keyScancode));
		str += ")";
		return str;
	}
}


ButtonState KeyCodeConverterSdl2::convertSdlToKeyModifierButtonState(Uint16 sdlKeymod, Uint16 sdlKeymodToTest)
{
	if (sdlKeymod & sdlKeymodToTest) {
		return ButtonState::Pressed;
	}
	else {
		return ButtonState::Released;
	}
}


void KeyCodeConverterSdl2::createSdlToVersoKeyCodesMap()
{
	sdlToVersoKeyCodesMap[SDLK_UNKNOWN] = KeyCode::Unknown;
	sdlToVersoKeyCodesMap[SDLK_RETURN] = KeyCode::Return;
	sdlToVersoKeyCodesMap[SDLK_ESCAPE] = KeyCode::Escape;
	sdlToVersoKeyCodesMap[SDLK_BACKSPACE] = KeyCode::Backspace;
	sdlToVersoKeyCodesMap[SDLK_TAB] = KeyCode::Tab;
	sdlToVersoKeyCodesMap[SDLK_SPACE] = KeyCode::Space;
	sdlToVersoKeyCodesMap[SDLK_EXCLAIM] = KeyCode::Exclaim;
	sdlToVersoKeyCodesMap[SDLK_QUOTEDBL] = KeyCode::QuoteDbl;
	sdlToVersoKeyCodesMap[SDLK_HASH] = KeyCode::Hash;
	sdlToVersoKeyCodesMap[SDLK_PERCENT] = KeyCode::Percent;
	sdlToVersoKeyCodesMap[SDLK_DOLLAR] = KeyCode::Dollar;
	sdlToVersoKeyCodesMap[SDLK_AMPERSAND] = KeyCode::Ampersand;
	sdlToVersoKeyCodesMap[SDLK_QUOTE] = KeyCode::Quote;
	sdlToVersoKeyCodesMap[SDLK_LEFTPAREN] = KeyCode::LeftParen;
	sdlToVersoKeyCodesMap[SDLK_RIGHTPAREN] = KeyCode::RightParen;
	sdlToVersoKeyCodesMap[SDLK_ASTERISK] = KeyCode::Asterisk;
	sdlToVersoKeyCodesMap[SDLK_PLUS] = KeyCode::Plus;
	sdlToVersoKeyCodesMap[SDLK_COMMA] = KeyCode::Comma;
	sdlToVersoKeyCodesMap[SDLK_MINUS] = KeyCode::Minus;
	sdlToVersoKeyCodesMap[SDLK_PERIOD] = KeyCode::Period;
	sdlToVersoKeyCodesMap[SDLK_SLASH] = KeyCode::Slash;
	sdlToVersoKeyCodesMap[SDLK_0] = KeyCode::N0;
	sdlToVersoKeyCodesMap[SDLK_1] = KeyCode::N1;
	sdlToVersoKeyCodesMap[SDLK_2] = KeyCode::N2;
	sdlToVersoKeyCodesMap[SDLK_3] = KeyCode::N3;
	sdlToVersoKeyCodesMap[SDLK_4] = KeyCode::N4;
	sdlToVersoKeyCodesMap[SDLK_5] = KeyCode::N5;
	sdlToVersoKeyCodesMap[SDLK_6] = KeyCode::N6;
	sdlToVersoKeyCodesMap[SDLK_7] = KeyCode::N7;
	sdlToVersoKeyCodesMap[SDLK_8] = KeyCode::N8;
	sdlToVersoKeyCodesMap[SDLK_9] = KeyCode::N9;
	sdlToVersoKeyCodesMap[SDLK_COLON] = KeyCode::Colon;
	sdlToVersoKeyCodesMap[SDLK_SEMICOLON] = KeyCode::Semicolon;
	sdlToVersoKeyCodesMap[SDLK_LESS] = KeyCode::Less;
	sdlToVersoKeyCodesMap[SDLK_EQUALS] = KeyCode::Equals;
	sdlToVersoKeyCodesMap[SDLK_GREATER] = KeyCode::Greater;
	sdlToVersoKeyCodesMap[SDLK_QUESTION] = KeyCode::Question;
	sdlToVersoKeyCodesMap[SDLK_AT] = KeyCode::At;
	sdlToVersoKeyCodesMap[SDLK_LEFTBRACKET] = KeyCode::LeftBracket;
	sdlToVersoKeyCodesMap[SDLK_BACKSLASH] = KeyCode::Backslash;
	sdlToVersoKeyCodesMap[SDLK_RIGHTBRACKET] = KeyCode::RightBracket;
	sdlToVersoKeyCodesMap[SDLK_CARET] = KeyCode::Caret;
	sdlToVersoKeyCodesMap[SDLK_UNDERSCORE] = KeyCode::Underscore;
	sdlToVersoKeyCodesMap[SDLK_BACKQUOTE] = KeyCode::Backquote;
	sdlToVersoKeyCodesMap[SDLK_a] = KeyCode::A;
	sdlToVersoKeyCodesMap[SDLK_b] = KeyCode::B;
	sdlToVersoKeyCodesMap[SDLK_c] = KeyCode::C;
	sdlToVersoKeyCodesMap[SDLK_d] = KeyCode::D;
	sdlToVersoKeyCodesMap[SDLK_e] = KeyCode::E;
	sdlToVersoKeyCodesMap[SDLK_f] = KeyCode::F;
	sdlToVersoKeyCodesMap[SDLK_g] = KeyCode::G;
	sdlToVersoKeyCodesMap[SDLK_h] = KeyCode::H;
	sdlToVersoKeyCodesMap[SDLK_i] = KeyCode::I;
	sdlToVersoKeyCodesMap[SDLK_j] = KeyCode::J;
	sdlToVersoKeyCodesMap[SDLK_k] = KeyCode::K;
	sdlToVersoKeyCodesMap[SDLK_l] = KeyCode::L;
	sdlToVersoKeyCodesMap[SDLK_m] = KeyCode::M;
	sdlToVersoKeyCodesMap[SDLK_n] = KeyCode::N;
	sdlToVersoKeyCodesMap[SDLK_o] = KeyCode::O;
	sdlToVersoKeyCodesMap[SDLK_p] = KeyCode::P;
	sdlToVersoKeyCodesMap[SDLK_q] = KeyCode::Q;
	sdlToVersoKeyCodesMap[SDLK_r] = KeyCode::R;
	sdlToVersoKeyCodesMap[SDLK_s] = KeyCode::S;
	sdlToVersoKeyCodesMap[SDLK_t] = KeyCode::T;
	sdlToVersoKeyCodesMap[SDLK_u] = KeyCode::U;
	sdlToVersoKeyCodesMap[SDLK_v] = KeyCode::V;
	sdlToVersoKeyCodesMap[SDLK_w] = KeyCode::W;
	sdlToVersoKeyCodesMap[SDLK_x] = KeyCode::X;
	sdlToVersoKeyCodesMap[SDLK_y] = KeyCode::Y;
	sdlToVersoKeyCodesMap[SDLK_z] = KeyCode::Z;
	sdlToVersoKeyCodesMap[SDLK_CAPSLOCK] = KeyCode::CapsLock;
	sdlToVersoKeyCodesMap[SDLK_F1] = KeyCode::F1;
	sdlToVersoKeyCodesMap[SDLK_F2] = KeyCode::F2;
	sdlToVersoKeyCodesMap[SDLK_F3] = KeyCode::F3;
	sdlToVersoKeyCodesMap[SDLK_F4] = KeyCode::F4;
	sdlToVersoKeyCodesMap[SDLK_F5] = KeyCode::F5;
	sdlToVersoKeyCodesMap[SDLK_F6] = KeyCode::F6;
	sdlToVersoKeyCodesMap[SDLK_F7] = KeyCode::F7;
	sdlToVersoKeyCodesMap[SDLK_F8] = KeyCode::F8;
	sdlToVersoKeyCodesMap[SDLK_F9] = KeyCode::F9;
	sdlToVersoKeyCodesMap[SDLK_F10] = KeyCode::F10;
	sdlToVersoKeyCodesMap[SDLK_F11] = KeyCode::F11;
	sdlToVersoKeyCodesMap[SDLK_F12] = KeyCode::F12;
	sdlToVersoKeyCodesMap[SDLK_PRINTSCREEN] = KeyCode::PrintScreen;
	sdlToVersoKeyCodesMap[SDLK_SCROLLLOCK] = KeyCode::ScrollLock;
	sdlToVersoKeyCodesMap[SDLK_PAUSE] = KeyCode::Pause;
	sdlToVersoKeyCodesMap[SDLK_INSERT] = KeyCode::Insert;
	sdlToVersoKeyCodesMap[SDLK_HOME] = KeyCode::Home;
	sdlToVersoKeyCodesMap[SDLK_PAGEUP] = KeyCode::PageUp;
	sdlToVersoKeyCodesMap[SDLK_DELETE] = KeyCode::Del;
	sdlToVersoKeyCodesMap[SDLK_END] = KeyCode::End;
	sdlToVersoKeyCodesMap[SDLK_PAGEDOWN] = KeyCode::PageDown;
	sdlToVersoKeyCodesMap[SDLK_RIGHT] = KeyCode::Right;
	sdlToVersoKeyCodesMap[SDLK_LEFT] = KeyCode::Left;
	sdlToVersoKeyCodesMap[SDLK_DOWN] = KeyCode::Down;
	sdlToVersoKeyCodesMap[SDLK_UP] = KeyCode::Up;
	sdlToVersoKeyCodesMap[SDLK_NUMLOCKCLEAR] = KeyCode::NumLockClear;
	sdlToVersoKeyCodesMap[SDLK_KP_DIVIDE] = KeyCode::KpDivide;
	sdlToVersoKeyCodesMap[SDLK_KP_MULTIPLY] = KeyCode::KpMultiply;
	sdlToVersoKeyCodesMap[SDLK_KP_MINUS] = KeyCode::KpMinus;
	sdlToVersoKeyCodesMap[SDLK_KP_PLUS] = KeyCode::KpPlus;
	sdlToVersoKeyCodesMap[SDLK_KP_ENTER] = KeyCode::KpEnter;
	sdlToVersoKeyCodesMap[SDLK_KP_1] = KeyCode::Kp1;
	sdlToVersoKeyCodesMap[SDLK_KP_2] = KeyCode::Kp2;
	sdlToVersoKeyCodesMap[SDLK_KP_3] = KeyCode::Kp3;
	sdlToVersoKeyCodesMap[SDLK_KP_4] = KeyCode::Kp4;
	sdlToVersoKeyCodesMap[SDLK_KP_5] = KeyCode::Kp5;
	sdlToVersoKeyCodesMap[SDLK_KP_6] = KeyCode::Kp6;
	sdlToVersoKeyCodesMap[SDLK_KP_7] = KeyCode::Kp7;
	sdlToVersoKeyCodesMap[SDLK_KP_8] = KeyCode::Kp8;
	sdlToVersoKeyCodesMap[SDLK_KP_9] = KeyCode::Kp9;
	sdlToVersoKeyCodesMap[SDLK_KP_0] = KeyCode::Kp0;
	sdlToVersoKeyCodesMap[SDLK_KP_PERIOD] = KeyCode::KpPeriod;
	sdlToVersoKeyCodesMap[SDLK_APPLICATION] = KeyCode::Application;
	sdlToVersoKeyCodesMap[SDLK_POWER] = KeyCode::Power;
	sdlToVersoKeyCodesMap[SDLK_KP_EQUALS] = KeyCode::KpEquals;
	sdlToVersoKeyCodesMap[SDLK_F13] = KeyCode::F13;
	sdlToVersoKeyCodesMap[SDLK_F14] = KeyCode::F14;
	sdlToVersoKeyCodesMap[SDLK_F15] = KeyCode::F15;
	sdlToVersoKeyCodesMap[SDLK_F16] = KeyCode::F16;
	sdlToVersoKeyCodesMap[SDLK_F17] = KeyCode::F17;
	sdlToVersoKeyCodesMap[SDLK_F18] = KeyCode::F18;
	sdlToVersoKeyCodesMap[SDLK_F19] = KeyCode::F19;
	sdlToVersoKeyCodesMap[SDLK_F20] = KeyCode::F20;
	sdlToVersoKeyCodesMap[SDLK_F21] = KeyCode::F21;
	sdlToVersoKeyCodesMap[SDLK_F22] = KeyCode::F22;
	sdlToVersoKeyCodesMap[SDLK_F23] = KeyCode::F23;
	sdlToVersoKeyCodesMap[SDLK_F24] = KeyCode::F24;
	sdlToVersoKeyCodesMap[SDLK_EXECUTE] = KeyCode::Execute;
	sdlToVersoKeyCodesMap[SDLK_HELP] = KeyCode::Help;
	sdlToVersoKeyCodesMap[SDLK_MENU] = KeyCode::Menu;
	sdlToVersoKeyCodesMap[SDLK_SELECT] = KeyCode::Select;
	sdlToVersoKeyCodesMap[SDLK_STOP] = KeyCode::Stop;
	sdlToVersoKeyCodesMap[SDLK_AGAIN] = KeyCode::Again;
	sdlToVersoKeyCodesMap[SDLK_UNDO] = KeyCode::Undo;
	sdlToVersoKeyCodesMap[SDLK_CUT] = KeyCode::Cut;
	sdlToVersoKeyCodesMap[SDLK_COPY] = KeyCode::Copy;
	sdlToVersoKeyCodesMap[SDLK_PASTE] = KeyCode::Paste;
	sdlToVersoKeyCodesMap[SDLK_FIND] = KeyCode::Find;
	sdlToVersoKeyCodesMap[SDLK_MUTE] = KeyCode::Mute;
	sdlToVersoKeyCodesMap[SDLK_VOLUMEUP] = KeyCode::VolumeUp;
	sdlToVersoKeyCodesMap[SDLK_VOLUMEDOWN] = KeyCode::VolumeDown;
	sdlToVersoKeyCodesMap[SDLK_KP_COMMA] = KeyCode::KpComma;
	sdlToVersoKeyCodesMap[SDLK_KP_EQUALSAS400] = KeyCode::KpEqualsAs400;
	sdlToVersoKeyCodesMap[SDLK_ALTERASE] = KeyCode::Alterase;
	sdlToVersoKeyCodesMap[SDLK_SYSREQ] = KeyCode::SysReq;
	sdlToVersoKeyCodesMap[SDLK_CANCEL] = KeyCode::Cancel;
	sdlToVersoKeyCodesMap[SDLK_CLEAR] = KeyCode::Clear;
	sdlToVersoKeyCodesMap[SDLK_PRIOR] = KeyCode::Prior;
	sdlToVersoKeyCodesMap[SDLK_RETURN2] = KeyCode::Return2;
	sdlToVersoKeyCodesMap[SDLK_SEPARATOR] = KeyCode::Separator;
	sdlToVersoKeyCodesMap[SDLK_OUT] = KeyCode::Out;
	sdlToVersoKeyCodesMap[SDLK_OPER] = KeyCode::Oper;
	sdlToVersoKeyCodesMap[SDLK_CLEARAGAIN] = KeyCode::ClearAgain;
	sdlToVersoKeyCodesMap[SDLK_CRSEL] = KeyCode::CrSel;
	sdlToVersoKeyCodesMap[SDLK_EXSEL] = KeyCode::ExSel;
	sdlToVersoKeyCodesMap[SDLK_KP_00] = KeyCode::Kp00;
	sdlToVersoKeyCodesMap[SDLK_KP_000] = KeyCode::Kp000;
	sdlToVersoKeyCodesMap[SDLK_THOUSANDSSEPARATOR] = KeyCode::ThousandsSeparator;
	sdlToVersoKeyCodesMap[SDLK_DECIMALSEPARATOR] = KeyCode::DecimalSeparator;
	sdlToVersoKeyCodesMap[SDLK_CURRENCYUNIT] = KeyCode::CurrencyUnit;
	sdlToVersoKeyCodesMap[SDLK_CURRENCYSUBUNIT] = KeyCode::CurrencySubUnit;
	sdlToVersoKeyCodesMap[SDLK_KP_LEFTPAREN] = KeyCode::KpLeftParen;
	sdlToVersoKeyCodesMap[SDLK_KP_RIGHTPAREN] = KeyCode::KpRightParen;
	sdlToVersoKeyCodesMap[SDLK_KP_LEFTBRACE] = KeyCode::KpLeftBrace;
	sdlToVersoKeyCodesMap[SDLK_KP_RIGHTBRACE] = KeyCode::KpRightBrace;
	sdlToVersoKeyCodesMap[SDLK_KP_TAB] = KeyCode::KpTab;
	sdlToVersoKeyCodesMap[SDLK_KP_BACKSPACE] = KeyCode::KpBackspace;
	sdlToVersoKeyCodesMap[SDLK_KP_A] = KeyCode::KpA;
	sdlToVersoKeyCodesMap[SDLK_KP_B] = KeyCode::KpB;
	sdlToVersoKeyCodesMap[SDLK_KP_C] = KeyCode::KpC;
	sdlToVersoKeyCodesMap[SDLK_KP_D] = KeyCode::KpD;
	sdlToVersoKeyCodesMap[SDLK_KP_E] = KeyCode::KpE;
	sdlToVersoKeyCodesMap[SDLK_KP_F] = KeyCode::KpF;
	sdlToVersoKeyCodesMap[SDLK_KP_XOR] = KeyCode::KpXor;
	sdlToVersoKeyCodesMap[SDLK_KP_POWER] = KeyCode::KpPower;
	sdlToVersoKeyCodesMap[SDLK_KP_PERCENT] = KeyCode::KpPercent;
	sdlToVersoKeyCodesMap[SDLK_KP_LESS] = KeyCode::KpLess;
	sdlToVersoKeyCodesMap[SDLK_KP_GREATER] = KeyCode::KpGreater;
	sdlToVersoKeyCodesMap[SDLK_KP_AMPERSAND] = KeyCode::KpAmpersand;
	sdlToVersoKeyCodesMap[SDLK_KP_DBLAMPERSAND] = KeyCode::KpDblAmpersand;
	sdlToVersoKeyCodesMap[SDLK_KP_VERTICALBAR] = KeyCode::KpVerticalBar;
	sdlToVersoKeyCodesMap[SDLK_KP_DBLVERTICALBAR] = KeyCode::KpDblVerticalBar;
	sdlToVersoKeyCodesMap[SDLK_KP_COLON] = KeyCode::KpColon;
	sdlToVersoKeyCodesMap[SDLK_KP_HASH] = KeyCode::KpHash;
	sdlToVersoKeyCodesMap[SDLK_KP_SPACE] = KeyCode::KpSpace;
	sdlToVersoKeyCodesMap[SDLK_KP_AT] = KeyCode::KpAt;
	sdlToVersoKeyCodesMap[SDLK_KP_EXCLAM] = KeyCode::KpExclam;
	sdlToVersoKeyCodesMap[SDLK_KP_MEMSTORE] = KeyCode::KpMemStore;
	sdlToVersoKeyCodesMap[SDLK_KP_MEMRECALL] = KeyCode::KpMemRecall;
	sdlToVersoKeyCodesMap[SDLK_KP_MEMCLEAR] = KeyCode::KpMemClear;
	sdlToVersoKeyCodesMap[SDLK_KP_MEMADD] = KeyCode::KpMemAdd;
	sdlToVersoKeyCodesMap[SDLK_KP_MEMSUBTRACT] = KeyCode::KpMemSubtract;
	sdlToVersoKeyCodesMap[SDLK_KP_MEMMULTIPLY] = KeyCode::KpMemMultiply;
	sdlToVersoKeyCodesMap[SDLK_KP_MEMDIVIDE] = KeyCode::KpMemDivide;
	sdlToVersoKeyCodesMap[SDLK_KP_PLUSMINUS] = KeyCode::KpPlusMinus;
	sdlToVersoKeyCodesMap[SDLK_KP_CLEAR] = KeyCode::KpClear;
	sdlToVersoKeyCodesMap[SDLK_KP_CLEARENTRY] = KeyCode::KpClearEntry;
	sdlToVersoKeyCodesMap[SDLK_KP_BINARY] = KeyCode::KpBinary;
	sdlToVersoKeyCodesMap[SDLK_KP_OCTAL] = KeyCode::KpOctal;
	sdlToVersoKeyCodesMap[SDLK_KP_DECIMAL] = KeyCode::KpDecimal;
	sdlToVersoKeyCodesMap[SDLK_KP_HEXADECIMAL] = KeyCode::KpHexadecimal;
	sdlToVersoKeyCodesMap[SDLK_LCTRL] = KeyCode::LCtrl;
	sdlToVersoKeyCodesMap[SDLK_LSHIFT] = KeyCode::LShift;
	sdlToVersoKeyCodesMap[SDLK_LALT] = KeyCode::LAlt;
	sdlToVersoKeyCodesMap[SDLK_LGUI] = KeyCode::LGui;
	sdlToVersoKeyCodesMap[SDLK_RCTRL] = KeyCode::RCtrl;
	sdlToVersoKeyCodesMap[SDLK_RSHIFT] = KeyCode::RShift;
	sdlToVersoKeyCodesMap[SDLK_RALT] = KeyCode::RAlt;
	sdlToVersoKeyCodesMap[SDLK_RGUI] = KeyCode::RGui;
	sdlToVersoKeyCodesMap[SDLK_MODE] = KeyCode::Mode;
	sdlToVersoKeyCodesMap[SDLK_AUDIONEXT] = KeyCode::AudioNext;
	sdlToVersoKeyCodesMap[SDLK_AUDIOPREV] = KeyCode::AudioPrev;
	sdlToVersoKeyCodesMap[SDLK_AUDIOSTOP] = KeyCode::AudioStop;
	sdlToVersoKeyCodesMap[SDLK_AUDIOPLAY] = KeyCode::AudioPlay;
	sdlToVersoKeyCodesMap[SDLK_AUDIOMUTE] = KeyCode::AudioMute;
	sdlToVersoKeyCodesMap[SDLK_MEDIASELECT] = KeyCode::MediaSelect;
	sdlToVersoKeyCodesMap[SDLK_WWW] = KeyCode::Www;
	sdlToVersoKeyCodesMap[SDLK_MAIL] = KeyCode::Mail;
	sdlToVersoKeyCodesMap[SDLK_CALCULATOR] = KeyCode::Calculator;
	sdlToVersoKeyCodesMap[SDLK_COMPUTER] = KeyCode::Computer;
	sdlToVersoKeyCodesMap[SDLK_AC_SEARCH] = KeyCode::AcSearch;
	sdlToVersoKeyCodesMap[SDLK_AC_HOME] = KeyCode::AcHome;
	sdlToVersoKeyCodesMap[SDLK_AC_BACK] = KeyCode::AcBack;
	sdlToVersoKeyCodesMap[SDLK_AC_FORWARD] = KeyCode::AcForward;
	sdlToVersoKeyCodesMap[SDLK_AC_STOP] = KeyCode::AcStop;
	sdlToVersoKeyCodesMap[SDLK_AC_REFRESH] = KeyCode::AcRefresh;
	sdlToVersoKeyCodesMap[SDLK_AC_BOOKMARKS] = KeyCode::AcBookmarks;
	sdlToVersoKeyCodesMap[SDLK_BRIGHTNESSDOWN] = KeyCode::BrightnessDown;
	sdlToVersoKeyCodesMap[SDLK_BRIGHTNESSUP] = KeyCode::BrightnessUp;
	sdlToVersoKeyCodesMap[SDLK_DISPLAYSWITCH] = KeyCode::DisplaySwitch;
	sdlToVersoKeyCodesMap[SDLK_KBDILLUMTOGGLE] = KeyCode::KbdIllumToggle;
	sdlToVersoKeyCodesMap[SDLK_KBDILLUMDOWN] = KeyCode::KbdIllumDown;
	sdlToVersoKeyCodesMap[SDLK_KBDILLUMUP] = KeyCode::KbdIllumUp;
	sdlToVersoKeyCodesMap[SDLK_EJECT] = KeyCode::Eject;
	sdlToVersoKeyCodesMap[SDLK_SLEEP] = KeyCode::Sleep;
}


void KeyCodeConverterSdl2::createSdlToVersoKeymodifiersMap()
{
	sdlToVersoKeymodifiersMap[KMOD_NONE] = Keymodifier::None;
	sdlToVersoKeymodifiersMap[KMOD_LSHIFT] = Keymodifier::LShift;
	sdlToVersoKeymodifiersMap[KMOD_RSHIFT] = Keymodifier::RShift;
	sdlToVersoKeymodifiersMap[KMOD_LCTRL] = Keymodifier::LCtrl;
	sdlToVersoKeymodifiersMap[KMOD_RCTRL] = Keymodifier::RCtrl;
	sdlToVersoKeymodifiersMap[KMOD_LALT] = Keymodifier::LAlt;
	sdlToVersoKeymodifiersMap[KMOD_RALT] = Keymodifier::RAlt;
	sdlToVersoKeymodifiersMap[KMOD_LGUI] = Keymodifier::LGui;
	sdlToVersoKeymodifiersMap[KMOD_RGUI] = Keymodifier::RGui;
	sdlToVersoKeymodifiersMap[KMOD_NUM] = Keymodifier::Num;
	sdlToVersoKeymodifiersMap[KMOD_CAPS] = Keymodifier::Caps;
	sdlToVersoKeymodifiersMap[KMOD_MODE] = Keymodifier::Mode;
	sdlToVersoKeymodifiersMap[KMOD_RESERVED] = Keymodifier::Reserved;
	sdlToVersoKeymodifiersMap[static_cast<SDL_Keymod>(KMOD_LSHIFT | KMOD_RSHIFT)] = Keymodifier::AnyShift;
	sdlToVersoKeymodifiersMap[static_cast<SDL_Keymod>(KMOD_LCTRL | KMOD_RCTRL)] = Keymodifier::AnyCtrl;
	sdlToVersoKeymodifiersMap[static_cast<SDL_Keymod>(KMOD_LALT | KMOD_RALT)] = Keymodifier::AnyAlt;
	sdlToVersoKeymodifiersMap[static_cast<SDL_Keymod>(KMOD_LGUI | KMOD_RGUI)] = Keymodifier::AnyGui;
	sdlToVersoKeymodifiersMap[static_cast<SDL_Keymod>(KMOD_NUM | KMOD_CAPS | KMOD_MODE)] = Keymodifier::NoneOrAnyNumCapsMode;
	sdlToVersoKeymodifiersMap[static_cast<SDL_Keymod>(KMOD_RESERVED*2)] = Keymodifier::NoneOrAny;
}


void KeyCodeConverterSdl2::createSdlToVersoKeyScancodesMap()
{
	sdlToVersoScancodesMap[SDL_SCANCODE_UNKNOWN] = KeyScancode::Unknown;

	sdlToVersoScancodesMap[SDL_SCANCODE_A] = KeyScancode::A;
	sdlToVersoScancodesMap[SDL_SCANCODE_B] = KeyScancode::B;
	sdlToVersoScancodesMap[SDL_SCANCODE_C] = KeyScancode::C;
	sdlToVersoScancodesMap[SDL_SCANCODE_D] = KeyScancode::D;
	sdlToVersoScancodesMap[SDL_SCANCODE_E] = KeyScancode::E;
	sdlToVersoScancodesMap[SDL_SCANCODE_F] = KeyScancode::F;
	sdlToVersoScancodesMap[SDL_SCANCODE_G] = KeyScancode::G;
	sdlToVersoScancodesMap[SDL_SCANCODE_H] = KeyScancode::H;
	sdlToVersoScancodesMap[SDL_SCANCODE_I] = KeyScancode::I;
	sdlToVersoScancodesMap[SDL_SCANCODE_J] = KeyScancode::J;
	sdlToVersoScancodesMap[SDL_SCANCODE_K] = KeyScancode::K;
	sdlToVersoScancodesMap[SDL_SCANCODE_L] = KeyScancode::L;
	sdlToVersoScancodesMap[SDL_SCANCODE_M] = KeyScancode::M;
	sdlToVersoScancodesMap[SDL_SCANCODE_N] = KeyScancode::N;
	sdlToVersoScancodesMap[SDL_SCANCODE_O] = KeyScancode::O;
	sdlToVersoScancodesMap[SDL_SCANCODE_P] = KeyScancode::P;
	sdlToVersoScancodesMap[SDL_SCANCODE_Q] = KeyScancode::Q;
	sdlToVersoScancodesMap[SDL_SCANCODE_R] = KeyScancode::R;
	sdlToVersoScancodesMap[SDL_SCANCODE_S] = KeyScancode::S;
	sdlToVersoScancodesMap[SDL_SCANCODE_T] = KeyScancode::T;
	sdlToVersoScancodesMap[SDL_SCANCODE_U] = KeyScancode::U;
	sdlToVersoScancodesMap[SDL_SCANCODE_V] = KeyScancode::V;
	sdlToVersoScancodesMap[SDL_SCANCODE_W] = KeyScancode::W;
	sdlToVersoScancodesMap[SDL_SCANCODE_X] = KeyScancode::X;
	sdlToVersoScancodesMap[SDL_SCANCODE_Y] = KeyScancode::Y;
	sdlToVersoScancodesMap[SDL_SCANCODE_Z] = KeyScancode::Z;

	sdlToVersoScancodesMap[SDL_SCANCODE_1] = KeyScancode::N1;
	sdlToVersoScancodesMap[SDL_SCANCODE_2] = KeyScancode::N2;
	sdlToVersoScancodesMap[SDL_SCANCODE_3] = KeyScancode::N3;
	sdlToVersoScancodesMap[SDL_SCANCODE_4] = KeyScancode::N4;
	sdlToVersoScancodesMap[SDL_SCANCODE_5] = KeyScancode::N5;
	sdlToVersoScancodesMap[SDL_SCANCODE_6] = KeyScancode::N6;
	sdlToVersoScancodesMap[SDL_SCANCODE_7] = KeyScancode::N7;
	sdlToVersoScancodesMap[SDL_SCANCODE_8] = KeyScancode::N8;
	sdlToVersoScancodesMap[SDL_SCANCODE_9] = KeyScancode::N9;
	sdlToVersoScancodesMap[SDL_SCANCODE_0] = KeyScancode::N0;

	sdlToVersoScancodesMap[SDL_SCANCODE_RETURN] = KeyScancode::Return;
	sdlToVersoScancodesMap[SDL_SCANCODE_ESCAPE] = KeyScancode::Escape;
	sdlToVersoScancodesMap[SDL_SCANCODE_BACKSPACE] = KeyScancode::Backspace;
	sdlToVersoScancodesMap[SDL_SCANCODE_TAB] = KeyScancode::Tab;
	sdlToVersoScancodesMap[SDL_SCANCODE_SPACE] = KeyScancode::Space;
	sdlToVersoScancodesMap[SDL_SCANCODE_MINUS] = KeyScancode::Minus;
	sdlToVersoScancodesMap[SDL_SCANCODE_EQUALS] = KeyScancode::Equals;
	sdlToVersoScancodesMap[SDL_SCANCODE_LEFTBRACKET] = KeyScancode::LeftBracket;
	sdlToVersoScancodesMap[SDL_SCANCODE_RIGHTBRACKET] = KeyScancode::RightBracket;
	sdlToVersoScancodesMap[SDL_SCANCODE_BACKSLASH] = KeyScancode::Backslash;
	sdlToVersoScancodesMap[SDL_SCANCODE_NONUSHASH] = KeyScancode::NonUsHash;
	sdlToVersoScancodesMap[SDL_SCANCODE_SEMICOLON] = KeyScancode::Semicolon;
	sdlToVersoScancodesMap[SDL_SCANCODE_APOSTROPHE] = KeyScancode::Apostrophe;
	sdlToVersoScancodesMap[SDL_SCANCODE_GRAVE] = KeyScancode::Grave;
	sdlToVersoScancodesMap[SDL_SCANCODE_COMMA] = KeyScancode::Comma;
	sdlToVersoScancodesMap[SDL_SCANCODE_PERIOD] = KeyScancode::Period;
	sdlToVersoScancodesMap[SDL_SCANCODE_SLASH] = KeyScancode::Slash;
	sdlToVersoScancodesMap[SDL_SCANCODE_CAPSLOCK] = KeyScancode::CapsLock;
	sdlToVersoScancodesMap[SDL_SCANCODE_F1] = KeyScancode::F1;
	sdlToVersoScancodesMap[SDL_SCANCODE_F2] = KeyScancode::F2;
	sdlToVersoScancodesMap[SDL_SCANCODE_F3] = KeyScancode::F3;
	sdlToVersoScancodesMap[SDL_SCANCODE_F4] = KeyScancode::F4;
	sdlToVersoScancodesMap[SDL_SCANCODE_F5] = KeyScancode::F5;
	sdlToVersoScancodesMap[SDL_SCANCODE_F6] = KeyScancode::F6;
	sdlToVersoScancodesMap[SDL_SCANCODE_F7] = KeyScancode::F7;
	sdlToVersoScancodesMap[SDL_SCANCODE_F8] = KeyScancode::F8;
	sdlToVersoScancodesMap[SDL_SCANCODE_F9] = KeyScancode::F9;
	sdlToVersoScancodesMap[SDL_SCANCODE_F10] = KeyScancode::F10;
	sdlToVersoScancodesMap[SDL_SCANCODE_F11] = KeyScancode::F11;
	sdlToVersoScancodesMap[SDL_SCANCODE_F12] = KeyScancode::F12;
	sdlToVersoScancodesMap[SDL_SCANCODE_PRINTSCREEN] = KeyScancode::PrintScreen;
	sdlToVersoScancodesMap[SDL_SCANCODE_SCROLLLOCK] = KeyScancode::ScrollLock;
	sdlToVersoScancodesMap[SDL_SCANCODE_PAUSE] = KeyScancode::Pause;
	sdlToVersoScancodesMap[SDL_SCANCODE_INSERT] = KeyScancode::Insert;
	sdlToVersoScancodesMap[SDL_SCANCODE_HOME] = KeyScancode::Home;
	sdlToVersoScancodesMap[SDL_SCANCODE_PAGEUP] = KeyScancode::PageUp;
	sdlToVersoScancodesMap[SDL_SCANCODE_DELETE] = KeyScancode::Del;
	sdlToVersoScancodesMap[SDL_SCANCODE_END] = KeyScancode::End;
	sdlToVersoScancodesMap[SDL_SCANCODE_PAGEDOWN] = KeyScancode::PageDown;
	sdlToVersoScancodesMap[SDL_SCANCODE_RIGHT] = KeyScancode::Right;
	sdlToVersoScancodesMap[SDL_SCANCODE_LEFT] = KeyScancode::Left;
	sdlToVersoScancodesMap[SDL_SCANCODE_DOWN] = KeyScancode::Down;
	sdlToVersoScancodesMap[SDL_SCANCODE_UP] = KeyScancode::Up;
	sdlToVersoScancodesMap[SDL_SCANCODE_NUMLOCKCLEAR] = KeyScancode::NumLockClear;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_DIVIDE] = KeyScancode::KpDivide;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MULTIPLY] = KeyScancode::KpMultiply;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MINUS] = KeyScancode::KpMinus;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_PLUS] = KeyScancode::KpPlus;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_ENTER] = KeyScancode::KpEnter;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_1] = KeyScancode::Kp1;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_2] = KeyScancode::Kp2;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_3] = KeyScancode::Kp3;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_4] = KeyScancode::Kp4;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_5] = KeyScancode::Kp5;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_6] = KeyScancode::Kp6;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_7] = KeyScancode::Kp7;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_8] = KeyScancode::Kp8;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_9] = KeyScancode::Kp9;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_0] = KeyScancode::Kp0;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_PERIOD] = KeyScancode::KpPeriod;
	sdlToVersoScancodesMap[SDL_SCANCODE_NONUSBACKSLASH] = KeyScancode::NonUsBackslash;
	sdlToVersoScancodesMap[SDL_SCANCODE_APPLICATION] = KeyScancode::Application;
	sdlToVersoScancodesMap[SDL_SCANCODE_POWER] = KeyScancode::Power;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_EQUALS] = KeyScancode::KpEquals;
	sdlToVersoScancodesMap[SDL_SCANCODE_F13] = KeyScancode::F13;
	sdlToVersoScancodesMap[SDL_SCANCODE_F14] = KeyScancode::F14;
	sdlToVersoScancodesMap[SDL_SCANCODE_F15] = KeyScancode::F15;
	sdlToVersoScancodesMap[SDL_SCANCODE_F16] = KeyScancode::F16;
	sdlToVersoScancodesMap[SDL_SCANCODE_F17] = KeyScancode::F17;
	sdlToVersoScancodesMap[SDL_SCANCODE_F18] = KeyScancode::F18;
	sdlToVersoScancodesMap[SDL_SCANCODE_F19] = KeyScancode::F19;
	sdlToVersoScancodesMap[SDL_SCANCODE_F20] = KeyScancode::F20;
	sdlToVersoScancodesMap[SDL_SCANCODE_F21] = KeyScancode::F21;
	sdlToVersoScancodesMap[SDL_SCANCODE_F22] = KeyScancode::F22;
	sdlToVersoScancodesMap[SDL_SCANCODE_F23] = KeyScancode::F23;
	sdlToVersoScancodesMap[SDL_SCANCODE_F24] = KeyScancode::F24;
	sdlToVersoScancodesMap[SDL_SCANCODE_EXECUTE] = KeyScancode::Execute;
	sdlToVersoScancodesMap[SDL_SCANCODE_HELP] = KeyScancode::Help;
	sdlToVersoScancodesMap[SDL_SCANCODE_MENU] = KeyScancode::Menu;
	sdlToVersoScancodesMap[SDL_SCANCODE_SELECT] = KeyScancode::Select;
	sdlToVersoScancodesMap[SDL_SCANCODE_STOP] = KeyScancode::Stop;
	sdlToVersoScancodesMap[SDL_SCANCODE_AGAIN] = KeyScancode::Again;
	sdlToVersoScancodesMap[SDL_SCANCODE_UNDO] = KeyScancode::Undo;
	sdlToVersoScancodesMap[SDL_SCANCODE_CUT] = KeyScancode::Cut;
	sdlToVersoScancodesMap[SDL_SCANCODE_COPY] = KeyScancode::Copy;
	sdlToVersoScancodesMap[SDL_SCANCODE_PASTE] = KeyScancode::Paste;
	sdlToVersoScancodesMap[SDL_SCANCODE_FIND] = KeyScancode::Find;
	sdlToVersoScancodesMap[SDL_SCANCODE_MUTE] = KeyScancode::Mute;
	sdlToVersoScancodesMap[SDL_SCANCODE_VOLUMEUP] = KeyScancode::VolumeUp;
	sdlToVersoScancodesMap[SDL_SCANCODE_VOLUMEDOWN] = KeyScancode::VolumeDown;
	// SDL comment: not sure whether there's a reason to enable these
	//	sdlToVersoScancodesMap[SDL_SCANCODE_LOCKINGCAPSLOCK] = KeyScancode::LockingCapsLock;
	//	sdlToVersoScancodesMap[SDL_SCANCODE_LOCKINGNUMLOCK] = KeyScancode::LockingNumLock;
	//  sdlToVersoScancodesMap[SDL_SCANCODE_LOCKINGSCROLLLOCK] = KeyScancode::LockingScrollLock;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_COMMA] = KeyScancode::KpComma;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_EQUALSAS400] = KeyScancode::KpEqualsAs400;

	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL1] = KeyScancode::International1;
	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL2] = KeyScancode::International2;
	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL3] = KeyScancode::International3;
	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL4] = KeyScancode::International4;
	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL5] = KeyScancode::International5;
	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL6] = KeyScancode::International6;
	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL7] = KeyScancode::International7;
	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL8] = KeyScancode::International8;
	sdlToVersoScancodesMap[SDL_SCANCODE_INTERNATIONAL9] = KeyScancode::International9;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG1] = KeyScancode::Lang1;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG2] = KeyScancode::Lang2;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG3] = KeyScancode::Lang3;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG4] = KeyScancode::Lang4;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG5] = KeyScancode::Lang5;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG6] = KeyScancode::Lang6;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG7] = KeyScancode::Lang7;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG8] = KeyScancode::Lang8;
	sdlToVersoScancodesMap[SDL_SCANCODE_LANG9] = KeyScancode::Lang9;

	sdlToVersoScancodesMap[SDL_SCANCODE_ALTERASE] = KeyScancode::Alterase;
	sdlToVersoScancodesMap[SDL_SCANCODE_SYSREQ] = KeyScancode::SysReq;
	sdlToVersoScancodesMap[SDL_SCANCODE_CANCEL] = KeyScancode::Cancel;
	sdlToVersoScancodesMap[SDL_SCANCODE_CLEAR] = KeyScancode::Clear;
	sdlToVersoScancodesMap[SDL_SCANCODE_PRIOR] = KeyScancode::Prior;
	sdlToVersoScancodesMap[SDL_SCANCODE_RETURN2] = KeyScancode::Return2;
	sdlToVersoScancodesMap[SDL_SCANCODE_SEPARATOR] = KeyScancode::Separator;
	sdlToVersoScancodesMap[SDL_SCANCODE_OUT] = KeyScancode::Out;
	sdlToVersoScancodesMap[SDL_SCANCODE_OPER] = KeyScancode::Oper;
	sdlToVersoScancodesMap[SDL_SCANCODE_CLEARAGAIN] = KeyScancode::ClearAgain;
	sdlToVersoScancodesMap[SDL_SCANCODE_CRSEL] = KeyScancode::CrSel;
	sdlToVersoScancodesMap[SDL_SCANCODE_EXSEL] = KeyScancode::ExSel;

	sdlToVersoScancodesMap[SDL_SCANCODE_KP_00] = KeyScancode::Kp00;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_000] = KeyScancode::Kp000;
	sdlToVersoScancodesMap[SDL_SCANCODE_THOUSANDSSEPARATOR] = KeyScancode::ThousandsSeparator;
	sdlToVersoScancodesMap[SDL_SCANCODE_DECIMALSEPARATOR] = KeyScancode::DecimalSeparator;
	sdlToVersoScancodesMap[SDL_SCANCODE_CURRENCYUNIT] = KeyScancode::CurrencyUnit;
	sdlToVersoScancodesMap[SDL_SCANCODE_CURRENCYSUBUNIT] = KeyScancode::CurrencySubUnit;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_LEFTPAREN] = KeyScancode::KpLeftParen;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_RIGHTPAREN] = KeyScancode::KpRightParen;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_LEFTBRACE] = KeyScancode::KpLeftBrace;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_RIGHTBRACE] = KeyScancode::KpRightBrace;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_TAB] = KeyScancode::KpTab;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_BACKSPACE] = KeyScancode::KpBackspace;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_A] = KeyScancode::KpA;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_B] = KeyScancode::KpB;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_C] = KeyScancode::KpC;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_D] = KeyScancode::KpD;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_E] = KeyScancode::KpE;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_F] = KeyScancode::KpF;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_XOR] = KeyScancode::KpXor;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_POWER] = KeyScancode::KpPower;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_PERCENT] = KeyScancode::KpPercent;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_LESS] = KeyScancode::KpLess;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_GREATER] = KeyScancode::KpGreater;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_AMPERSAND] = KeyScancode::KpAmpersand;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_DBLAMPERSAND] = KeyScancode::KpDblAmpersand;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_VERTICALBAR] = KeyScancode::KpVerticalBar;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_DBLVERTICALBAR] = KeyScancode::KpDblVerticalBar;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_COLON] = KeyScancode::KpColon;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_HASH] = KeyScancode::KpHash;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_SPACE] = KeyScancode::KpSpace;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_AT] = KeyScancode::KpAt;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_EXCLAM] = KeyScancode::KpExclam;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MEMSTORE] = KeyScancode::KpMemStore;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MEMRECALL] = KeyScancode::KpMemRecall;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MEMCLEAR] = KeyScancode::KpMemClear;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MEMADD] = KeyScancode::KpMemAdd;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MEMSUBTRACT] = KeyScancode::KpMemSubtract;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MEMMULTIPLY] = KeyScancode::KpMemMultiply;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_MEMDIVIDE] = KeyScancode::KpMemDivide;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_PLUSMINUS] = KeyScancode::KpPlusMinus;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_CLEAR] = KeyScancode::KpClear;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_CLEARENTRY] = KeyScancode::KpClearEntry;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_BINARY] = KeyScancode::KpBinary;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_OCTAL] = KeyScancode::KpOctal;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_DECIMAL] = KeyScancode::KpDecimal;
	sdlToVersoScancodesMap[SDL_SCANCODE_KP_HEXADECIMAL] = KeyScancode::KpHexadecimal;

	sdlToVersoScancodesMap[SDL_SCANCODE_LCTRL] = KeyScancode::LCtrl;
	sdlToVersoScancodesMap[SDL_SCANCODE_LSHIFT] = KeyScancode::LShift;
	sdlToVersoScancodesMap[SDL_SCANCODE_LALT] = KeyScancode::LAlt;
	sdlToVersoScancodesMap[SDL_SCANCODE_LGUI] = KeyScancode::LGui;
	sdlToVersoScancodesMap[SDL_SCANCODE_RCTRL] = KeyScancode::RCtrl;
	sdlToVersoScancodesMap[SDL_SCANCODE_RSHIFT] = KeyScancode::RShift;
	sdlToVersoScancodesMap[SDL_SCANCODE_RALT] = KeyScancode::RAlt;
	sdlToVersoScancodesMap[SDL_SCANCODE_RGUI] = KeyScancode::RGui;

	sdlToVersoScancodesMap[SDL_SCANCODE_MODE] = KeyScancode::Mode;

	sdlToVersoScancodesMap[SDL_SCANCODE_AUDIONEXT] = KeyScancode::AudioNext;
	sdlToVersoScancodesMap[SDL_SCANCODE_AUDIOPREV] = KeyScancode::AudioPrev;
	sdlToVersoScancodesMap[SDL_SCANCODE_AUDIOSTOP] = KeyScancode::AudioStop;
	sdlToVersoScancodesMap[SDL_SCANCODE_AUDIOPLAY] = KeyScancode::AudioPlay;
	sdlToVersoScancodesMap[SDL_SCANCODE_AUDIOMUTE] = KeyScancode::AudioMute;
	sdlToVersoScancodesMap[SDL_SCANCODE_MEDIASELECT] = KeyScancode::MediaSelect;
	sdlToVersoScancodesMap[SDL_SCANCODE_WWW] = KeyScancode::Www;
	sdlToVersoScancodesMap[SDL_SCANCODE_MAIL] = KeyScancode::Mail;
	sdlToVersoScancodesMap[SDL_SCANCODE_CALCULATOR] = KeyScancode::Calculator;
	sdlToVersoScancodesMap[SDL_SCANCODE_COMPUTER] = KeyScancode::Computer;
	sdlToVersoScancodesMap[SDL_SCANCODE_AC_SEARCH] = KeyScancode::AcSearch;
	sdlToVersoScancodesMap[SDL_SCANCODE_AC_HOME] = KeyScancode::AcHome;
	sdlToVersoScancodesMap[SDL_SCANCODE_AC_BACK] = KeyScancode::AcBack;
	sdlToVersoScancodesMap[SDL_SCANCODE_AC_FORWARD] = KeyScancode::AcForward;
	sdlToVersoScancodesMap[SDL_SCANCODE_AC_STOP] = KeyScancode::AcStop;
	sdlToVersoScancodesMap[SDL_SCANCODE_AC_REFRESH] = KeyScancode::AcRefresh;
	sdlToVersoScancodesMap[SDL_SCANCODE_AC_BOOKMARKS] = KeyScancode::AcBookmarks;

	sdlToVersoScancodesMap[SDL_SCANCODE_BRIGHTNESSDOWN] = KeyScancode::BrightnessDown;
	sdlToVersoScancodesMap[SDL_SCANCODE_BRIGHTNESSUP] = KeyScancode::BrightnessUp;
	sdlToVersoScancodesMap[SDL_SCANCODE_DISPLAYSWITCH] = KeyScancode::DisplaySwitch;
	sdlToVersoScancodesMap[SDL_SCANCODE_KBDILLUMTOGGLE] = KeyScancode::KbdIllumToggle;
	sdlToVersoScancodesMap[SDL_SCANCODE_KBDILLUMDOWN] = KeyScancode::KbdIllumDown;
	sdlToVersoScancodesMap[SDL_SCANCODE_KBDILLUMUP] = KeyScancode::KbdIllumUp;
	sdlToVersoScancodesMap[SDL_SCANCODE_EJECT] = KeyScancode::Eject;
	sdlToVersoScancodesMap[SDL_SCANCODE_SLEEP] = KeyScancode::Sleep;

	sdlToVersoScancodesMap[SDL_SCANCODE_APP1] = KeyScancode::App1;
	sdlToVersoScancodesMap[SDL_SCANCODE_APP2] = KeyScancode::App2;
}


void KeyCodeConverterSdl2::createNamesToKeyCodesMap()
{
	for (auto it : sdlToVersoKeyCodesMap) {
		UString keyName(SDL_GetKeyName(it.first));
		if (it.second == KeyCode::None) { // a.k.a. First & Unknown
			keyName = "None";
		}

		// Fix duplicated string key from SDL2
		if (it.first == SDLK_RETURN2) {
			keyName = "Return2";
		}
		else if (it.first == SDLK_APPLICATION) {  // windows contextual menu, compose
			keyName = "Application";
		}

		// Sanity check for the keyName
		if (keyName.isEmpty()) {
			UString errorInfo("SDL KeyCode=");
			errorInfo.append2(static_cast<Sint32>(it.first));
			errorInfo += ", Verso KeyCode=";
			errorInfo.append2(static_cast<uint32_t>(it.second));
			VERSO_ERROR("verso-2d", "Trying to add empty keyName to nameToKeyCodeMap.", errorInfo.c_str());
		}

		// Check for duplicates before adding
		auto findIt = nameToKeyCodeMap.find(keyName);
		if (findIt == nameToKeyCodeMap.end()) {
			nameToKeyCodeMap[keyName] = it.second;
		}
		else {
			UString errorInfo("key=\"");
			errorInfo += keyName;
			errorInfo += "\", SDL KeyCode=";
			errorInfo.append2(static_cast<Sint32>(it.first));
			errorInfo += ", Verso KeyCode=";
			errorInfo.append2(static_cast<uint32_t>(it.second));
			errorInfo += ". Was already added with KeyCode=";
			errorInfo.append2(static_cast<uint32_t>(findIt->second));
			VERSO_ERROR("verso-2d", "Trying to add duplicate key to nameToKeyCodeMap.", errorInfo.c_str());
		}
	}
}


void KeyCodeConverterSdl2::createKeyCodesToNamesMap()
{
	// Populate values from nameToKeyCode
	for (auto& it : nameToKeyCodeMap) {
		keyCodeToNameMap[it.second] = it.first;
	}
}


void KeyCodeConverterSdl2::createNamesToKeymodifierMap()
{
	nameToKeymodifierMap["None"] = Keymodifier::None;
	nameToKeymodifierMap["Left Shift"] = Keymodifier::LShift;
	nameToKeymodifierMap["Right Shift"] = Keymodifier::RShift;
	nameToKeymodifierMap["Left Control"] = Keymodifier::LCtrl;
	nameToKeymodifierMap["Right Control"] = Keymodifier::RCtrl;
	nameToKeymodifierMap["Left Alt"] = Keymodifier::LAlt;
	nameToKeymodifierMap["Right Alt"] = Keymodifier::RAlt;
	nameToKeymodifierMap["Left GUI"] = Keymodifier::LGui;
	nameToKeymodifierMap["Right GUI"] = Keymodifier::RGui;
	nameToKeymodifierMap["Numlock"] = Keymodifier::Num;
	nameToKeymodifierMap["Capslock"] = Keymodifier::Caps;
	nameToKeymodifierMap["Mode"] = Keymodifier::Mode;
	nameToKeymodifierMap["Shift"] = Keymodifier::AnyShift;
	nameToKeymodifierMap["Control"] = Keymodifier::AnyCtrl;
	nameToKeymodifierMap["Alt"] = Keymodifier::AnyAlt;
	nameToKeymodifierMap["Gui"] = Keymodifier::AnyGui;
	nameToKeymodifierMap["None or any Numlock-Capslock-Mode"] = Keymodifier::NoneOrAnyNumCapsMode;
	nameToKeymodifierMap["None or any"] = Keymodifier::NoneOrAny;
}


void KeyCodeConverterSdl2::createKeymodifierToNamesMap()
{
	for (auto it : nameToKeymodifierMap) {
		auto findIt = keymodifierToNameMap.find(it.second);
		if (findIt == keymodifierToNameMap.end()) {
			keymodifierToNameMap[it.second] = it.first;
		}
		else {
			UString errorInfo("key modifier=\"");
			errorInfo.append2(static_cast<uint32_t>(it.second));
			errorInfo += "\", name=";
			errorInfo += it.first;
			VERSO_ERROR("verso-2d", "Trying to add duplicate key modifier to keymodifierToNameMap.", errorInfo.c_str());
		}
	}
}


void KeyCodeConverterSdl2::createNamesToKeyScancodeMap()
{
	for (auto it : sdlToVersoScancodesMap) {
		UString keyName = SDL_GetScancodeName(it.first);
		if (it.second == KeyScancode::None) { // a.k.a. First & Unknown
			keyName = "None";
		}
		else if (it.second == KeyScancode::Application) {  // windows contextual menu, compose
			keyName = "Application";
		}
		else if (it.second == KeyScancode::NonUsBackslash) {
			keyName = "Non-US backslash";
		}
		else if (it.second == KeyScancode::International1) {
			keyName = "International 1";
		}
		else if (it.second == KeyScancode::International2) {
			keyName = "International 2";
		}
		else if (it.second == KeyScancode::International3) {
			keyName = "International 3 (Yen)";
		}
		else if (it.second == KeyScancode::International4) {
			keyName = "International 4";
		}
		else if (it.second == KeyScancode::International5) {
			keyName = "International 5";
		}
		else if (it.second == KeyScancode::International6) {
			keyName = "International 6";
		}
		else if (it.second == KeyScancode::International7) {
			keyName = "International 7";
		}
		else if (it.second == KeyScancode::International8) {
			keyName = "International 8";
		}
		else if (it.second == KeyScancode::International9) {
			keyName = "International 9";
		}
		else if (it.second == KeyScancode::Lang1) {
			keyName = "Lang 1 (Hangul/English toggle)";
		}
		else if (it.second == KeyScancode::Lang2) {
			keyName = "Lang 2 (Hanja conversion)";
		}
		else if (it.second == KeyScancode::Lang3) {
			keyName = "Lang 3 (Katakana)";
		}
		else if (it.second == KeyScancode::Lang4) {
			keyName = "Lang 4 (Hiragana)";
		}
		else if (it.second == KeyScancode::Lang5) {
			keyName = "Lang 5 (Zenkaku/Hankaku)";
		}
		else if (it.second == KeyScancode::Lang6) {
			keyName = "Lang 6 (reserved)";
		}
		else if (it.second == KeyScancode::Lang7) {
			keyName = "Lang 7 (reserved)";
		}
		else if (it.second == KeyScancode::Lang8) {
			keyName = "Lang 8 (reserved)";
		}
		else if (it.second == KeyScancode::Lang9) {
			keyName = "Lang 9 (reserved)";
		}
		else if (it.second == KeyScancode::Return2) {
			keyName = "Return2";
		}

		// Sanity check for the keyName
		if (keyName.isEmpty()) {
			UString errorInfo("scancode=");
			errorInfo.append2(static_cast<uint32_t>(it.second));
			VERSO_ERROR("verso-2d", "Trying to add empty keyName to nameToKeyScancodeMap.", errorInfo.c_str());
		}

		// Check for duplicates before adding
		auto findIt = nameToKeyScancodeMap.find(keyName);
		if (findIt == nameToKeyScancodeMap.end()) {
			nameToKeyScancodeMap[keyName] = it.second;
		}
		else {
			UString errorInfo("keyName=\"");
			errorInfo += keyName;
			errorInfo += "\", scancode=";
			errorInfo.append2(static_cast<uint32_t>(it.second));
			errorInfo += ". Was already added with KeyScancode=";
			errorInfo.append2(static_cast<uint32_t>(findIt->second));
			VERSO_ERROR("verso-2d", "Trying to add duplicate keyName to nameToKeyScancodeMap.", errorInfo.c_str());
		}
	}
}


void KeyCodeConverterSdl2::createKeyScancodeToNamesMap()
{
	// Populate values from nameToKeyScancode
	for (auto it : nameToKeyScancodeMap) {
		keyScancodeToNameMap[it.second] = it.first;
	}
}


} // End namespace Verso

