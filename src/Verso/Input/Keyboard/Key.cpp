
//#include <Verso/Input/Keyboard/Key.hpp>
#include <Verso/Input/Keyboard/KeyCodeConverterSdl2.hpp>
#include <Verso/Input/Keyboard/KeySymbol.hpp>

namespace Verso {


UString keyCodeToString(KeyCode keyCode)
{
	return KeyCodeConverterSdl2::instance().keyCodeToName(keyCode);
}


std::ostream& operator <<(std::ostream& ost, KeyCode right)
{
	return ost << keyCodeToString(right);
}


UString keymodifierToString(Keymodifier keymodifier)
{
	return KeyCodeConverterSdl2::instance().keymodifierToName(keymodifier);
}


UString keymodifiersToString(Keymodifiers keymodifiers, bool humanReadable)
{
	UString str;
	bool firstModifier = true;

	if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::NoneOrAny)) == static_cast<Keymodifiers>(Keymodifier::NoneOrAny)) {
		return "[any]";
	}

	else if (keymodifiers == static_cast<Keymodifiers>(Keymodifier::None)) {
		if (!humanReadable) {
			return "[none]";
		}
		else {
			return "";
		}
	}

	else {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode)) == static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode)) {
			if (!humanReadable) {
				firstModifier = false;
				str += "[any Num/Caps/Mode]";
			}
			keymodifiers ^= static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode);
		}

		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyShift)) == static_cast<Keymodifiers>(Keymodifier::AnyShift)) {
			if (firstModifier == false) {
				str += " + ";
			}
			else {
				firstModifier = false;
			}
			str += "[Shift]";
			keymodifiers ^= static_cast<Keymodifiers>(Keymodifier::AnyShift);
		}

		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyCtrl)) == static_cast<Keymodifiers>(Keymodifier::AnyCtrl)) {
			if (firstModifier == false) {
				str += " + ";
			}
			else {
				firstModifier = false;
			}
			str += "[Ctrl]";
			keymodifiers ^= static_cast<Keymodifiers>(Keymodifier::AnyCtrl);
		}

		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyAlt)) == static_cast<Keymodifiers>(Keymodifier::AnyAlt)) {
			if (firstModifier == false) {
				str += " + ";
			}
			else {
				firstModifier = false;
			}
			str += "[Alt]";
			keymodifiers ^= static_cast<Keymodifiers>(Keymodifier::AnyAlt);
		}

		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyGui)) == static_cast<Keymodifiers>(Keymodifier::AnyGui)) {
			if (firstModifier == false) {
				str += " + ";
			}
			else {
				firstModifier = false;
			}
			str += "[Gui]";
			keymodifiers ^= static_cast<Keymodifiers>(Keymodifier::AnyGui);
		}
	}

	std::vector<Keymodifier> allKeymods = getAllKeymodifiers();
	for (auto it = allKeymods.begin(); it != allKeymods.end(); ++it) {
		if ((keymodifiers & static_cast<Keymodifiers>(*it)) == static_cast<Keymodifiers>(*it)) {
			if (firstModifier == false) {
				str += " + ";
			}
			else {
				firstModifier = false;
			}

			str += "[";
			str += keymodifierToString(*it);
			str += "]";
		}
	}

	return str;
}


bool keyModifiersMatchRules(Keymodifiers input, Keymodifiers rules)
{
	//std::cout << "keyModifiersMatchRules(Keymodifiers input="<<static_cast<uint32_t>(input)<<", Keymodifiers rules="<<static_cast<uint32_t>(rules)<<")" << std::endl;

	// None or any modifier
	if ((rules & static_cast<Keymodifiers>(Keymodifier::NoneOrAny)) == static_cast<Keymodifiers>(Keymodifier::NoneOrAny)) {
		//std::cout << "  => rules == static_cast<Keymodifiers>(Keymodifier::NoneOrAny) => return true" << std::endl;
		return true;
	}

	// None or any numlock & capslock & Mode
	if ((rules & static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode)) == static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode)) {
		//std::cout << "  => (rules & static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode)) == static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode)" << std::endl;

		if ((input & static_cast<Keymodifiers>(Keymodifier::Num)) == static_cast<Keymodifiers>(Keymodifier::Num)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::Num)) == static_cast<Keymodifiers>(Keymodifier::Num)" << std::endl;
			//std::cout << "      => input befor ^= Num: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::Num);
			//std::cout << "      => input after ^= Num: "<<static_cast<uint32_t>(input) << std::endl;
		}

		if ((input & static_cast<Keymodifiers>(Keymodifier::Caps)) == static_cast<Keymodifiers>(Keymodifier::Caps)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::Caps)) == static_cast<Keymodifiers>(Keymodifier::Caps)" << std::endl;
			//std::cout << "      => input befor ^= Caps: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::Caps);
			//std::cout << "      => input after ^= Caps: "<<static_cast<uint32_t>(input) << std::endl;
		}

		if ((input & static_cast<Keymodifiers>(Keymodifier::Mode)) == static_cast<Keymodifiers>(Keymodifier::Mode)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::Mode) == static_cast<Keymodifiers>(Keymodifier::Mode)" << std::endl;
			//std::cout << "      => input befor ^= Mode: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::Mode);
			//std::cout << "      => input after ^= Mode: "<<static_cast<uint32_t>(input) << std::endl;
		}

		//std::cout << "    => rules befor ^= NoneOrAnyNumCapsMode: "<<static_cast<uint32_t>(rules) << std::endl;
		rules ^= static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode);
		//std::cout << "    => rules after ^= NoneOrAnyNumCapsMode: "<<static_cast<uint32_t>(rules) << std::endl;
		if (input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None)) {
			//std::cout << "    => input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None) => return true" << std::endl;
			return true;
		}
	}

	// Any shift
	if ((rules & static_cast<Keymodifiers>(Keymodifier::AnyShift)) == static_cast<Keymodifiers>(Keymodifier::AnyShift)) {
		//std::cout << "  => (rules & static_cast<Keymodifiers>(Keymodifier::AnyShift)) == static_cast<Keymodifiers>(Keymodifier::AnyShift)" << std::endl;
		bool match = false;

		if ((input & static_cast<Keymodifiers>(Keymodifier::LShift)) == static_cast<Keymodifiers>(Keymodifier::LShift)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::LShift)) == static_cast<Keymodifiers>(Keymodifier::LShift)" << std::endl;
			//std::cout << "      => input befor ^= LShift: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::LShift);
			//std::cout << "      => input after ^= LShift: "<<static_cast<uint32_t>(input) << std::endl;
			match = true;
		}

		if ((input & static_cast<Keymodifiers>(Keymodifier::RShift)) == static_cast<Keymodifiers>(Keymodifier::RShift)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::RShift)) == static_cast<Keymodifiers>(Keymodifier::RShift)" << std::endl;
			//std::cout << "      => input befor ^= RShift: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::RShift);
			//std::cout << "      => input after ^= RShift: "<<static_cast<uint32_t>(input) << std::endl;
			match = true;
		}

		if (match == true) {
			//std::cout << "    => rules befor ^= AnyShift: "<<static_cast<uint32_t>(rules) << std::endl;
			rules ^= static_cast<Keymodifiers>(Keymodifier::AnyShift);
			//std::cout << "    => rules after ^= AnyShift: "<<static_cast<uint32_t>(rules) << std::endl;
			if (input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None)) {
				//std::cout << "    => input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None) => return true" << std::endl;
				return true;
			}
		}
	}

	// Any ctrl
	if ((rules & static_cast<Keymodifiers>(Keymodifier::AnyCtrl)) == static_cast<Keymodifiers>(Keymodifier::AnyCtrl)) {
		//std::cout << "  => (rules & static_cast<Keymodifiers>(Keymodifier::AnyCtrl)) == static_cast<Keymodifiers>(Keymodifier::AnyCtrl)" << std::endl;
		bool match = false;

		if ((input & static_cast<Keymodifiers>(Keymodifier::LCtrl)) == static_cast<Keymodifiers>(Keymodifier::LCtrl)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::LCtrl)) == static_cast<Keymodifiers>(Keymodifier::LCtrl)" << std::endl;
			//std::cout << "      => input befor ^= LCtrl: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::LCtrl);
			//std::cout << "      => input after ^= LCtrl: "<<static_cast<uint32_t>(input) << std::endl;
			match = true;
		}

		if ((input & static_cast<Keymodifiers>(Keymodifier::RCtrl)) == static_cast<Keymodifiers>(Keymodifier::RCtrl)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::RCtrl)) == static_cast<Keymodifiers>(Keymodifier::RCtrl)" << std::endl;
			//std::cout << "      => input befor ^= RCtrl: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::RCtrl);
			//std::cout << "      => input after ^= RCtrl: "<<static_cast<uint32_t>(input) << std::endl;
			match = true;
		}

		if (match == true) {
			//std::cout << "    => rules befor ^= AnyCtrl: "<<static_cast<uint32_t>(rules) << std::endl;
			rules ^= static_cast<Keymodifiers>(Keymodifier::AnyCtrl);
			//std::cout << "    => rules after ^= AnyCtrl: "<<static_cast<uint32_t>(rules) << std::endl;

			if (input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None)) {
				//std::cout << "    => input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None) => return true" << std::endl;
				return true;
			}
		}
	}

	// Any alt
	if ((rules & static_cast<Keymodifiers>(Keymodifier::AnyAlt)) == static_cast<Keymodifiers>(Keymodifier::AnyAlt)) {
		//std::cout << "  => (rules & static_cast<Keymodifiers>(Keymodifier::AnyAlt)) == static_cast<Keymodifiers>(Keymodifier::AnyAlt)" << std::endl;
		bool match = false;

		if ((input & static_cast<Keymodifiers>(Keymodifier::LAlt)) == static_cast<Keymodifiers>(Keymodifier::LAlt)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::LAlt)) == static_cast<Keymodifiers>(Keymodifier::LAlt)" << std::endl;
			//std::cout << "      => input befor ^= LAlt: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::LAlt);
			//std::cout << "      => input after ^= LAlt: "<<static_cast<uint32_t>(input) << std::endl;
			match = true;
		}

		if ((input & static_cast<Keymodifiers>(Keymodifier::RAlt)) == static_cast<Keymodifiers>(Keymodifier::RAlt)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::RAlt)) == static_cast<Keymodifiers>(Keymodifier::RAlt)" << std::endl;
			//std::cout << "      => input befor ^= RAlt: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::RAlt);
			//std::cout << "      => input after ^= RAlt: "<<static_cast<uint32_t>(input) << std::endl;
			match = true;
		}

		if (match == true) {
			//std::cout << "    => rules befor ^= AnyAlt: "<<static_cast<uint32_t>(rules) << std::endl;
			rules ^= static_cast<Keymodifiers>(Keymodifier::AnyAlt);
			//std::cout << "    => rules after ^= AnyAlt: "<<static_cast<uint32_t>(rules) << std::endl;

			if (input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None)) {
				//std::cout << "    => input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None) => return true" << std::endl;
				return true;
			}
		}
	}

	// Any gui
	if ((rules & static_cast<Keymodifiers>(Keymodifier::AnyGui)) == static_cast<Keymodifiers>(Keymodifier::AnyGui)) {
		//std::cout << "  => (rules & static_cast<Keymodifiers>(Keymodifier::AnyGui)) == static_cast<Keymodifiers>(Keymodifier::AnyGui)" << std::endl;
		bool match = false;

		if ((input & static_cast<Keymodifiers>(Keymodifier::LGui)) == static_cast<Keymodifiers>(Keymodifier::LGui)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::LGui)) == static_cast<Keymodifiers>(Keymodifier::LGui)" << std::endl;
			//std::cout << "      => input befor ^= LGui: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::LGui);
			//std::cout << "      => input after ^= LGui: "<<static_cast<uint32_t>(input) << std::endl;
			match = true;
		}

		if ((input & static_cast<Keymodifiers>(Keymodifier::RGui)) == static_cast<Keymodifiers>(Keymodifier::RGui)) {
			//std::cout << "    => (input & static_cast<Keymodifiers>(Keymodifier::RGui)) == static_cast<Keymodifiers>(Keymodifier::RGui)" << std::endl;
			//std::cout << "      => input befor ^= RGui: "<<static_cast<uint32_t>(input) << std::endl;
			input ^= static_cast<Keymodifiers>(Keymodifier::RGui);
			//std::cout << "      => input after ^= RGui: "<<static_cast<uint32_t>(input) << std::endl;
			match = true;
		}

		if (match == true) {
			//std::cout << "    => rules befor ^= AnyGui: "<<static_cast<uint32_t>()rules) << std::endl;
			rules ^= static_cast<Keymodifiers>(Keymodifier::AnyGui);
			//std::cout << "    => rules after ^= AnyGui: "<<static_cast<uint32_t>(rules) << std::endl;

			if (input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None)) {
				//std::cout << "    => input == static_cast<Keymodifiers>(Keymodifier::None) && rules == static_cast<Keymodifiers>(Keymodifier::None) => return true" << std::endl;
				return true;
			}
		}
	}

	if ((input & rules) == rules && (input ^ rules) == static_cast<Keymodifiers>(Keymodifier::None)) {
		//std::cout << "  => (input & rules) == rules && (input ^ rules) == static_cast<Keymodifiers>(Keymodifier::None) => return true" << std::endl;
		return true;
	}
	else {
		//std::cout << "  => !((input & rules) == rules && (input ^ rules) == static_cast<Keymodifiers>(Keymodifier::None)) => return false" << std::endl;
		return false;
	}
}


std::vector<Keymodifier> getAllKeymodifiers()
{
	std::vector<Keymodifier> keymodifiers;
	keymodifiers.push_back(Keymodifier::LShift);
	keymodifiers.push_back(Keymodifier::RShift);
	keymodifiers.push_back(Keymodifier::LCtrl);
	keymodifiers.push_back(Keymodifier::RCtrl);
	keymodifiers.push_back(Keymodifier::LAlt);
	keymodifiers.push_back(Keymodifier::RAlt);
	keymodifiers.push_back(Keymodifier::LGui);
	keymodifiers.push_back(Keymodifier::RGui);
	keymodifiers.push_back(Keymodifier::Num);
	keymodifiers.push_back(Keymodifier::Caps);
	keymodifiers.push_back(Keymodifier::Mode);
	keymodifiers.push_back(Keymodifier::AnyShift);
	keymodifiers.push_back(Keymodifier::AnyCtrl);
	keymodifiers.push_back(Keymodifier::AnyAlt);
	keymodifiers.push_back(Keymodifier::AnyGui);
	keymodifiers.push_back(Keymodifier::NoneOrAnyNumCapsMode);
	keymodifiers.push_back(Keymodifier::NoneOrAny);

	return keymodifiers;
}


std::ostream& operator <<(std::ostream& ost, Keymodifier right)
{
	return ost << keymodifierToString(right);
}


UString keyScancodeToString(KeyScancode keyScancode)
{
	return KeyCodeConverterSdl2::instance().keyScancodeToName(keyScancode);
}


std::ostream& operator <<(std::ostream& ost, KeyScancode right)
{
	return ost << keyScancodeToString(right);
}


UString keySymbolToString(const KeySymbol& keySymbol)
{
	KeyCodeConverterSdl2& kcc = KeyCodeConverterSdl2::instance();
	UString s = "KeySymbol[ "+kcc.keyCodeToName(keySymbol.keyCode)+", ";
	s += keyScancodeToString(keySymbol.scancode)+", ";
	s += keymodifiersToString(keySymbol.keymodifiers, false)+" ]";
	return s;
}


std::ostream& operator <<(std::ostream& ost, const KeySymbol& right)
{
	return ost << keySymbolToString(right);
}


UString keyCodeAndKeymodifiersToString(KeyCode keyCode, Keymodifiers keymodifiers, bool humanReadable)
{
	UString str(keymodifiersToString(keymodifiers, humanReadable));
	if (str.size() != 0) {
		str += " + ";
	}

	str += keyCodeToString(keyCode);

	return str;
}


bool keymodifiersContainKeyCode(Keymodifiers keymodifiers, KeyCode keyCode)
{
	if (keymodifiers == static_cast<Keymodifiers>(Keymodifier::None)) {
		return false;
	}

	if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::NoneOrAny)) == static_cast<Keymodifiers>(Keymodifier::NoneOrAny)) {
		return false;
	}

	if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode)) == static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode)) {
		keymodifiers ^= static_cast<Keymodifiers>(Keymodifier::NoneOrAnyNumCapsMode);
	}

	if (keyCode == KeyCode::LShift) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::LShift)) == static_cast<Keymodifiers>(Keymodifier::LShift) ||
				(keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyShift)) != static_cast<Keymodifiers>(Keymodifier::None)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::RShift) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::RShift)) == static_cast<Keymodifiers>(Keymodifier::RShift) ||
				(keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyShift)) != static_cast<Keymodifiers>(Keymodifier::None)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::LCtrl) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::LCtrl)) == static_cast<Keymodifiers>(Keymodifier::LCtrl) ||
				(keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyCtrl)) != static_cast<Keymodifiers>(Keymodifier::None)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::RCtrl) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::RCtrl)) == static_cast<Keymodifiers>(Keymodifier::RCtrl) ||
				(keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyCtrl)) != static_cast<Keymodifiers>(Keymodifier::None)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::LAlt) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::LAlt)) == static_cast<Keymodifiers>(Keymodifier::LAlt) ||
				(keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyAlt)) != static_cast<Keymodifiers>(Keymodifier::None)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::RAlt) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::RAlt)) == static_cast<Keymodifiers>(Keymodifier::RAlt) ||
				(keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyAlt)) != static_cast<Keymodifiers>(Keymodifier::None)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::LGui) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::LGui)) == static_cast<Keymodifiers>(Keymodifier::LGui) ||
				(keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyGui)) != static_cast<Keymodifiers>(Keymodifier::None)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::RGui) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::RGui)) == static_cast<Keymodifiers>(Keymodifier::RGui) ||
				(keymodifiers & static_cast<Keymodifiers>(Keymodifier::AnyGui)) != static_cast<Keymodifiers>(Keymodifier::None)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::NumLockClear) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::Num)) == static_cast<Keymodifiers>(Keymodifier::Num)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::CapsLock) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::Caps)) == static_cast<Keymodifiers>(Keymodifier::Caps)) {
			return true;
		}
	}

	else if (keyCode == KeyCode::Mode) {
		if ((keymodifiers & static_cast<Keymodifiers>(Keymodifier::Mode)) == static_cast<Keymodifiers>(Keymodifier::Mode)) {
			return true;
		}
	}

	return false;

	/*
	None = KMOD_NONE,		//!< No key modifiers active
	LShift = KMOD_LSHIFT,	//!< Left shift
	RShift = KMOD_RSHIFT,	//!< Right shift
	LCtrl = KMOD_LCTRL,		//!< Left control
	RCtrl = KMOD_RCTRL,		//!< Right control
	LAlt = KMOD_LALT,		//!< Left alt
	RAlt = KMOD_RALT,		//!< Right alt
	LGui = KMOD_LGUI,		//!< Left meta/logo
	RGui = KMOD_RGUI,		//!< Right meta/logo
	Num = KMOD_NUM,			//!< Numlock
	Caps = KMOD_CAPS,		//!< Capslock
	Mode = KMOD_MODE,
	Reserved = KMOD_RESERVED,
	AnyShift = LShift | RShift,					//!< Any shift
	AnyCtrl = LCtrl | RCtrl,					//!< Any control
	AnyAlt = LAlt | RAlt,						//!< Any alt
	AnyGui = LGui | RGui,						//!< Any meta/logo
	NoneOrAnyNumCapsMode = Num | Caps | Mode,	//!< Any stateful (numlock, capslock, mode) button
	NoneOrAny = KMOD_RESERVED*2,				//!< None or any modifier active
*/
}


} // End namespace Verso

