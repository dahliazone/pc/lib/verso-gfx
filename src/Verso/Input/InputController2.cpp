
#include <Verso/Input/InputController2.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/System/Logger.hpp>

// \TODO: check through code to match Verso standards!!!

namespace Verso {


InputController2::InputController2(const UString& name) :
	mouseEvents(),
	mouseX(-1),
	mouseY(-1),
	mouseMotionX(0),
	mouseMotionY(0),
	mouseMotionXRel(0.0f),
	mouseMotionYRel(0.0f),
	mouseButtonState(),
	mouseWheelMotionX(0),
	mouseWheelMotionY(0),
	mouseWheelMotionPreciseX(0.0f),
	mouseWheelMotionPreciseY(0.0f),
	mouseWheelMotionXRel(0.0f),
	mouseWheelMotionYRel(0.0f),

	name(name),
	keyboardToButton(),
	//mouseAxisToAxis(),
	mouseButtonToButton(),
	gameControllerButtonToAxis(),
	gameControllerButtonToButton(),
	joystickAxisToAxis(),
	joystickButtonToButton(),
	axisDatas(),
	axisDescriptions(),
	buttonDatas(),
	buttonDescriptions()
{
}


InputController2::~InputController2()
{
}


void InputController2::processEvent(const Event& event)
{
	//VERSO_LOG_DEBUG("verso-gfx", "Processing event: "<<event.toString());

	// \TODO: abstract event input system?
	// \TODO: include windowId & timestamp

	switch (event.type)
	{
		case EventType::Keyboard:
		{
			const KeyboardEvent& keyboard = event.keyboard;
			if (keyboard.type == KeyboardEventType::Key) {
				//VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "Processing key: ", keySymbolToString(keyboard.key));

				// Check for released keymodifiers
				if (keyboard.state == ButtonState::Released) {
					for (const auto& it : keyboardToButton) {
						const std::map<Keymodifiers, ButtonIndex>& keyModsMap = it.second;
						for (const auto& itMod : keyModsMap) {
							ButtonIndex buttonIndex = itMod.second;
							if (getButtonState(buttonIndex) == ButtonState::Pressed || getButtonPressedDown(buttonIndex) == true) {
								if (keymodifiersContainKeyCode(itMod.first, keyboard.key.keyCode)) {
									auto& buttonData = buttonDatas[buttonIndex];
									buttonData.state = buttonStateToWeightedButtonState(ButtonState::Released);
									buttonData.pressedDown = false;
									buttonData.releasedUp = true;
								}
							}
						}
					}
				}

				auto it = keyboardToButton.find(keyboard.key.keyCode);
				if (it != keyboardToButton.end()) {
					//VERSO_LOG_DEBUG("verso-gfx", "  => Found keyCode! now trying to match modifiers...");

					std::map<Keymodifiers, ButtonIndex>& keyModsMap = it->second;
					for (const auto& itMod : keyModsMap) {
						//VERSO_LOG_DEBUG("verso-gfx", "       => checking:"
						//                <<keymodifiersToString(keyboard.key.keymodifiers, false)<<"\"("<<(keyboard.key.keymodifiers)
						//                <<") against \""
						//                <<keymodifiersToString(itMod.first, false)<<"\"("<<(itMod.first)<<"): ");
						if (keyModifiersMatchRules(keyboard.key.keymodifiers, itMod.first)) {
							//VERSO_LOG_DEBUG("verso-gfx", "          %%%=> MATCHES!");
							ButtonIndex buttonIndex = itMod.second;
							auto& buttonData = buttonDatas[buttonIndex];
							buttonData.state = buttonStateToWeightedButtonState(keyboard.state);
							if (keyboard.state == ButtonState::Released) {
								buttonData.pressedDown = false;
								buttonData.releasedUp = true;
							}
							else {
								buttonData.pressedDown = true;
								buttonData.releasedUp = false;
							}
							//uint8_t repeat; // non-zero if this is a key repeat // \TODO: use here also?
						}
						//else
						//	VERSO_LOG_DEBUG("verso-gfx", "          %%%=> NOPE!");
					}
				}
				//else {
				//	VERSO_LOG_DEBUG("verso-gfx", "  => didn't find it from:");
				//	for (const auto it : keyboardToButton) {
				//		for (const auto& itMod : it.second) {
				//			KeyCode keyCode = it.first;
				//			Keymodifiers keymods = itMod.first;
				//			VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "", keyCodeAndKeymodifiersToString(keyCode, keymods).c_str());
				//		}
				//	}
				//}
			}
			break;
		}

		case EventType::Mouse:
		{
			// \TODO: Take windowId and mouseId to account here!
			const MouseEvent& mouse = event.mouse;
			if (mouse.type == MouseEventType::Motion) {
				mouseX = mouse.x; //int32_t
				mouseY = mouse.y;
				mouseMotionX = mouse.motionX;
				mouseMotionY = mouse.motionY;
				mouseMotionXRel = mouse.motionXRel;
				mouseMotionYRel = mouse.motionYRel;
			}

			else if (mouse.type == MouseEventType::Button) {
				for (int i = 0; i < static_cast<int>(MouseButton::Size); ++i) {
					mouseButtonState[i] = mouse.mouseButtons[i];
				}
			}

			else if (mouse.type == MouseEventType::Wheel) {
				mouseWheelMotionX = mouse.wheelMotionX;
				mouseWheelMotionY = mouse.wheelMotionY;
				mouseWheelMotionPreciseX = mouse.wheelMotionPreciseX;
				mouseWheelMotionPreciseY = mouse.wheelMotionPreciseY;
				mouseWheelMotionXRel = mouse.wheelMotionXRel;
				mouseWheelMotionYRel = mouse.wheelMotionYRel;
			}
			break;
		}

		case EventType::GameController:
		{
			//const GameControllerEvent& gameController = event.gameController;
			//VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_case__EventType_GameController
			break;
		}

		case EventType::Joystick:
		{
			const JoystickEvent& joystick = event.joystick;

			if (joystick.type == JoystickEventType::AxisMotion) {
				auto it = joystickAxisToAxis.find(joystick.axis);
				if  (it != joystickAxisToAxis.end()) {
					AxisIndex axisIndex = it->second;
					axisDatas[axisIndex] = joystick.value;
				}
			}

			else if (joystick.type == JoystickEventType::Button) {
				auto it = joystickButtonToButton.find(joystick.button);
				if  (it != joystickButtonToButton.end()) {
					ButtonIndex buttonIndex = it->second;
					auto& buttonData = buttonDatas[buttonIndex];
					buttonData.state = buttonStateToWeightedButtonState(joystick.state); // \TODO: support weighted buttons!
					if (joystick.state == ButtonState::Released) {
						buttonData.pressedDown = false;
						buttonData.releasedUp = true;
					}
					else {
						buttonData.pressedDown = true;
						buttonData.releasedUp = false;
					}
				}
			}
			break;
		}

		case EventType::GainedFocus:
			break;

		case EventType::LostFocus:
			break;

		case EventType::Resized:
			break;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Unknown EventType="<<event.type);
			break;
		}
	}
}


void InputController2::resetBoth()
{
	for (auto& button : buttonDatas) {
		button.pressedDown = false;
		button.releasedUp = false;
	}
}


void InputController2::resetPressedDown()
{
	for (auto& button : buttonDatas) {
		button.pressedDown = false;
	}
}


void InputController2::resetReleasedUp()
{
	for (auto& button : buttonDatas) {
		button.releasedUp = false;
	}
}


AxisState InputController2::getAxisState(AxisIndex axisIndex) const
{
	try {
		return axisDatas.at(axisIndex);
	} catch (const std::out_of_range& e) {
		UString resources("axisIndex=");
		resources.append2(axisIndex);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'axisIndex' is not valid", resources.c_str());
	}
}


ButtonData InputController2::getButtonData(ButtonIndex buttonIndex) const
{
	try {
		return buttonDatas.at(buttonIndex);
	}
	catch (const std::out_of_range& e) {
		UString resources("buttonIndex=");
		resources.append2(buttonIndex);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'buttonIndex' is not valid", resources.c_str());
	}
}


ButtonState InputController2::getButtonState(ButtonIndex buttonIndex) const
{
	return getButtonData(buttonIndex).state.state;
}


WeightedButtonState InputController2::getWeightedButtonState(ButtonIndex buttonIndex) const
{
	return getButtonData(buttonIndex).state;
}


bool InputController2::getButtonPressedDown(ButtonIndex buttonIndex) const
{
	return getButtonData(buttonIndex).pressedDown;
}


bool InputController2::getButtonReleasedUp(ButtonIndex buttonIndex) const
{
	return getButtonData(buttonIndex).releasedUp;
}


void InputController2::setButtonState(ButtonIndex buttonIndex, WeightedButtonState state)
{
	try {
		buttonDatas.at(buttonIndex).state = state;
	}
	catch (const std::out_of_range& e) {
		UString resources("buttonIndex=");
		resources.append2(buttonIndex);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'buttonIndex' is not valid", resources.c_str());
	}
}


void InputController2::setButtonPressed(ButtonIndex buttonIndex, ButtonState pressed)
{
	try {
		buttonDatas.at(buttonIndex).state = WeightedButtonState(pressed);
	}
	catch (const std::out_of_range& e) {
		UString resources("buttonIndex=");
		resources.append2(buttonIndex);
		VERSO_ILLEGALPARAMETERS("verso-gfx", "Given 'buttonIndex' is not valid", resources.c_str());
	}
}


void InputController2::mapKeyboardToButton(ButtonIndex buttonIndex, KeyMapping keyMapping, const UString& buttonName, const UString& buttonDescription)
{
	//std::map<KeyCode, std::map<Keymodifiers, ButtonIndex>> keyboardToButton;
	//std::vector<ButtonData> buttonDatas;
	//std::vector<ControlDescription> buttonDescriptions;

	// make sure buttonDatas and buttonDescriptions are big enough to set buttonIndex
	if (buttonDatas.size() <= buttonIndex) {
		buttonDatas.resize(buttonIndex + 1, ButtonData());
	}
	if (buttonDescriptions.size() <= buttonIndex) {
		buttonDescriptions.resize(buttonIndex + 1, ControlDescription());
	}

	auto& description = buttonDescriptions[buttonIndex];
	description.name = buttonName;
	description.description = buttonDescription;

	auto it = keyboardToButton.find(keyMapping.key);
	if  (it == keyboardToButton.end()) {
		std::map<Keymodifiers, ButtonIndex> newMap;
		newMap[keyMapping.modifiers] = buttonIndex;
		keyboardToButton[keyMapping.key] = newMap;
	}
	else {
		std::map<Keymodifiers, ButtonIndex>& current = it->second;
		auto itMod = current.find(keyMapping.modifiers);
		if (itMod == current.end()) {
			current[keyMapping.modifiers] = buttonIndex;
		}
		else {
			VERSO_LOG_WARN("verso-gfx", "Replacing assigned 'key'+'modifier' with new 'buttonIndex'");
			VERSO_LOG_WARN_VARIABLE("verso-gfx", "buttonIndex", buttonIndex);
			VERSO_LOG_WARN_VARIABLE("verso-gfx", "buttonName", buttonName.c_str());
			VERSO_LOG_WARN_VARIABLE("verso-gfx", "buttonDescription", buttonDescription.c_str());
			VERSO_LOG_WARN_VARIABLE("verso-gfx", "key", keyCodeToString(keyMapping.key).c_str());
			VERSO_LOG_WARN_VARIABLE("verso-gfx", "modifiers", keymodifiersToString(keyMapping.modifiers).c_str());
			current[keyMapping.modifiers] = buttonIndex;
		}
	}
}

/*
//AxisIndex InputController2::mapMouseAxisToAxis(const UString& axisName, MouseAxis mouseAxis) {}


ButtonIndex InputController2::mapMouseButtonToButton(const UString& buttonName, MouseButton mouseButton)
{
	(void)mouseButton; (void)buttonName;
	VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_method__InputController2_mapMouseButtonToButton
}


AxisIndex InputController2::mapGameControllerButtonToAxis(const UString& axisName, GameControllerAxis gameControllerAxis)
{
	(void)gameControllerAxis; (void)axisName;
	VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_method__InputController2_mapGameControllerButtonToAxis
}


ButtonIndex InputController2::mapGameControllerButtonToButton(const UString& buttonName, GameControllerButton gameControllerButton)
{
	(void)gameControllerButton; (void)buttonName;
	VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_method__InputController2_mapGameControllerButtonToButton
}


UString InputController2::findAxisNameByAxisType(JoystickAxis joystickAxis)
{
	//JoystickAxis -> AxisIndex -> UString
	//std::map<JoystickAxis, AxisIndex> joystickAxisToAxis;
	//std::map<UString, AxisIndex> axisIndexes;

	const auto& itAxis = joystickAxisToAxis.find(joystickAxis);
	if (itAxis == joystickAxisToAxis.end()) {
		UString resources("JoystickAxis="+joystickAxisToString(joystickAxis));
		VERSO_OBJECTNOTFOUND("verso-gfx", "Given JoystickAxis not mapped.", resources.c_str());
	}
	const AxisIndex& axisIndex = itAxis->second;

	for (const auto& itAxisIndex : axisIndexes) {
		if (itAxisIndex.second == axisIndex) {
			return itAxisIndex.first;
		}
	}

	UString resources("JoystickAxis=" + joystickAxisToString(joystickAxis));
	VERSO_OBJECTNOTFOUND("verso-gfx", "Given JoystickAxis not mapped.", resources.c_str());
}


UString InputController2::findButtonNameByButtonType(JoystickButton joystickButton)
{
	const auto& itButton = joystickButtonToButton.find(joystickButton);
	if (itButton == joystickButtonToButton.end()) {
		UString resources("JoystickButton="+joystickButtonToString(joystickButton));
		VERSO_OBJECTNOTFOUND("verso-gfx", "Given JoystickButton not mapped.", resources.c_str());
	}
	const ButtonIndex& buttonIndex = itButton->second;

	for (const auto& itButtonIndex : buttonIndexes) {
		if (itButtonIndex.second == buttonIndex) {
			return itButtonIndex.first;
		}
	}

	UString resources("JoystickButton=" + joystickButtonToString(joystickButton));
	VERSO_OBJECTNOTFOUND("verso-gfx", "Given JoystickButton not mapped.", resources.c_str());
}


AxisIndex InputController2::mapJoystickAxisToAxis(const UString& axisName, JoystickAxis joystickAxis)
{
	AxisIndex axisIndex = getAxisIndex(axisName);

	auto it = joystickAxisToAxis.find(joystickAxis);
	if  (it == joystickAxisToAxis.end()) {
		joystickAxisToAxis[joystickAxis] = axisIndex;
	}
	else {
		VERSO_LOG_WARN("verso-gfx", "Replacing already defined joystick axis to a new name");
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "axisName", axisName);
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "joystickAxis", joystickAxisToString(joystickAxis));
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "previous axisName", findAxisNameByAxisType(joystickAxis));
		joystickAxisToAxis[joystickAxis] = axisIndex;
	}

	return axisIndex;
}


ButtonIndex InputController2::mapJoystickButtonToButton(const UString& buttonName, JoystickButton joystickButton)
{
	ButtonIndex buttonIndex = getButtonIndex(buttonName);

	auto it = joystickButtonToButton.find(joystickButton);
	if  (it == joystickButtonToButton.end()) {
		joystickButtonToButton[joystickButton] = buttonIndex;
	}
	else {
		VERSO_LOG_WARN("verso-gfx", "Replacing already defined joystick button to a new name");
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "buttonName", buttonName);
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "joystickButton", joystickButtonToString(joystickButton));
		VERSO_LOG_WARN_VARIABLE("verso-gfx", "previous buttonName", findButtonNameByButtonType(joystickButton));
		joystickButtonToButton[joystickButton] = buttonIndex;
	}

	return buttonIndex;
}


ButtonIndex InputController2::mapJoystickHatToButton(const UString& buttonName, JoystickHat joystickHat, JoystickHatDirection joystickDirection)
{
	(void)joystickHat; (void)joystickDirection; (void)buttonName;
	VERSO_FAIL("verso-gfx"); // \TODO: unimplemented_method__InputController2_mapJoystickHatToButton
}
*/

} // End namespace Verso

