#include <Verso/Input/InputManager.hpp>
#include <Verso/System/Exception.hpp>
#include <Verso/Display/Display.hpp>
#include <Verso/System/InitWrapperSdl2.hpp>

namespace Verso {


InputManager::InputManager() :
	created(false),
	inputControllers(),
	inputControllers2(),
	sdlJoysticks(),
	sdlGameControllers(),
	mouseXNormalizer(),
	mouseYNormalizer(),
	mouseMotionXNormalizer(),
	mouseMotionYNormalizer(),
	mouseWheelMotionXNormalizer(Range<Sint32>(-1, 1), Range<Sint32>(0, 0)),
	mouseWheelMotionYNormalizer(Range<Sint32>(-1, 1), Range<Sint32>(0, 0)),
	gameControllerNormalizer(Range<Sint16>(-32768, 32767), Range<Sint16>(0, 0)), // \TODO: is range ok?
	joystickAxisNormalizer(Range<Sint16>(-32768, 32767), Range<Sint16>(0, 0)), // \TODO: is range ok?
	joystickTrackballNormalizer(Range<Sint16>(-32768, 32767), Range<Sint16>(0, 0)) // \TODO: is range ok?
{
	create();
}


InputManager& InputManager::instance()
{
	static InputManager inputManager;
	return inputManager;
}


InputManager::~InputManager()
{
	destroy();
}


void InputManager::create()
{
	if (isCreated() == true) {
		return;
	}

	KeyCodeConverterSdl2::instance();
	// \TODO: remove or implement updateDisplayResolution //updateDisplayResolution(window);
	created = true;
}


void InputManager::destroy() VERSO_NOEXCEPT
{
	if (isCreated() == false) {
		return;
	}

	for (auto& it : inputControllers2) {
		if (it.second != nullptr) {
			delete it.second;
			it.second = nullptr;
		}
	}
	inputControllers2.clear();

	for (auto& it : inputControllers) {
		if (it.second != nullptr) {
			delete it.second;
			it.second = nullptr;
		}
	}
	inputControllers.clear();

	disableJoysticks();
	disableGameControllers();

	created = false;
}


bool InputManager::isCreated() const
{
	return created;
}


/*void updateDisplayResolution(const AppWindow* window) // \TODO: remove or implement
{
	if (window == nullptr)
		return;

	mouseXNormalizer = Normalizer<Sint32>(Range<Sint32>(0, window->getWidth()), Range<Sint32>(0, 0));
	mouseYNormalizer = Normalizer<Sint32>(Range<Sint32>(0, window->getHeight()), Range<Sint32>(0, 0));
	mouseMotionXNormalizer = Normalizer<Sint32>(Range<Sint32>(-window->getWidth(), window->getWidth()), Range<Sint32>(0, 0));
	mouseMotionYNormalizer = Normalizer<Sint32>(Range<Sint32>(-window->getHeight(), window->getHeight()), Range<Sint32>(0, 0));
	mouseWheelMotionXNormalizer = Normalizer<Sint32>(Range<Sint32>(-window->getWidth(), window->getWidth()), Range<Sint32>(0, 0));
	mouseWheelMotionYNormalizer = Normalizer<Sint32>(Range<Sint32>(-window->getHeight(), window->getHeight()), Range<Sint32>(0, 0));
}*/


void InputManager::enableJoysticksAndGameControllers()
{
	SDL_Joystick* joy = nullptr;
	SDL_GameController* controller = nullptr;

	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::Joystick);
	InitWrapperSdl2::instance().createSubsystem(SubsystemSdl2::GameController);

	for (int i=0; i<SDL_NumJoysticks(); ++i) {

		if (SDL_IsGameController(i)) {
			controller = SDL_GameControllerOpen(i);
			if (controller) {
				VERSO_LOG_INFO("verso-gfx", "Opened["<<i<<"] Xbox 360 Game Controller=\""<<SDL_GameControllerNameForIndex(i)<<"\" (axes="<<SDL_JoystickNumAxes(joy)<<", buttons="<<SDL_JoystickNumButtons(joy)<<", trackballs="<<SDL_JoystickNumBalls(joy)<<")");
				sdlGameControllers.push_back(controller);
			}
			else {
				VERSO_LOG_INFO("verso-gfx", "Couldn't open Xbox 360 Game Controller["<<i<<"]");
			}
		}
		else {
			joy = SDL_JoystickOpen(i);
			if (joy) {
				VERSO_LOG_INFO("verso-gfx", "Opened["<<i<<"] Joystick=\""<<SDL_JoystickNameForIndex(i)<<"\" (axes="<<SDL_JoystickNumAxes(joy)<<", buttons="<<SDL_JoystickNumButtons(joy)<<", trackballs="<<SDL_JoystickNumBalls(joy)<<")");
				sdlJoysticks.push_back(joy);
			}
			else {
				VERSO_LOG_INFO("verso-gfx", "Couldn't open Joystick["<<i<<"]");
			}
		}
	}
}


void InputManager::disableJoysticks()
{
	for (auto it : sdlJoysticks) {
		if (SDL_JoystickGetAttached(it)) {
			SDL_JoystickClose(it);
		}
	}

	InitWrapperSdl2::instance().destroySubsystem(SubsystemSdl2::Joystick);

}


void InputManager::disableGameControllers()
{
	for (auto it : sdlGameControllers) {
		if (SDL_GameControllerGetAttached(it)) {
			SDL_GameControllerClose(it);
		}
	}

	InitWrapperSdl2::instance().destroySubsystem(SubsystemSdl2::GameController);
}


void InputManager::enableMidi()
{
	VERSO_FAIL("verso-gfx"); // \TODO: InputManager::EnableMidi(): NOT IMPLEMENTED!
}


void InputManager::disableMidi()
{
	VERSO_FAIL("verso-gfx"); // \TODO: InputManager::DisableMidi(): NOT IMPLEMENTED!
}


size_t InputManager::getGameControllerCount() const
{
	size_t count = 0;
	for (int i=0; i<SDL_NumJoysticks(); ++i) {
		if (SDL_IsGameController(i)) {
			count++;
		}
	}
	return count;
}


size_t InputManager::getNonGameControllerCount() const
{
	size_t count = 0;
	for (int i=0; i<SDL_NumJoysticks(); ++i) {
		if (!SDL_IsGameController(i)) {
			count++;
		}
	}
	return count;
}


UString InputManager::getJoystickInfo() const
{
	UString s = "Found ";
	s.append2(getGameControllerCount());
	s += " game controllers and ";
	s.append2(getNonGameControllerCount());
	s += " joysticks.";
	return s;
}


// Note, getMouseState() will not update the windowId, and x,y are relative to currently active window
MouseState InputManager::getMouseState() const
{
	MouseState state;

	// variable is unknown so set it to zero
	state.windowId = 0;

	// Location relative to current window
	int mX = -1, mY = -1;
	std::uint32_t buttonState = SDL_GetMouseState(&mX, &mY);
	state.x = static_cast<std::int32_t>(mX);
	state.y = static_cast<std::int32_t>(mY);

	// Location relative to desktop
	mX = -1; mY = -1;
	SDL_GetGlobalMouseState(&mX, &mY);
	state.desktopX = static_cast<std::int32_t>(mX);
	state.desktopY = static_cast<std::int32_t>(mY);

	// State of mouse buttons
	state.buttons[static_cast<int8_t>(MouseButton::Left)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_LEFT);
	state.buttons[static_cast<int8_t>(MouseButton::Middle)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_MIDDLE);
	state.buttons[static_cast<int8_t>(MouseButton::Right)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_RIGHT);
	state.buttons[static_cast<int8_t>(MouseButton::X1)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_X1);
	state.buttons[static_cast<int8_t>(MouseButton::X2)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_X2);

	return state;
}


size_t InputManager::getMouseActiveDisplayIndex(const std::vector<Display>& displays) const
{
	MouseState mouseState = getMouseState();
	Vector2i mouseDesktopPos = Vector2i(mouseState.desktopX, mouseState.desktopY);

	size_t mouseAtDisplayIndex = 0;
	for (size_t i=0; i<displays.size(); ++i) {
		if (displays[i].rect.isInside(mouseDesktopPos)) {
			mouseAtDisplayIndex = i;
			break;
		}
	}

	return mouseAtDisplayIndex;
}


InputController* InputManager::createInputController(const UString& name)
{
	if (getInputController(name) != nullptr) {
		VERSO_ILLEGALPARAMETERS("verso-gfx", "There's already an InputController with the given name", name.c_str());
	}

	InputController* ic = new InputController(name);
	inputControllers[name] = ic;
	return ic;
}


InputController* InputManager::getInputController(const UString& name)
{
	std::map<UString, InputController*>::const_iterator it = inputControllers.find(name);
	if (it == inputControllers.end()) {
		return nullptr;
	}
	else {
		return it->second;
	}
}


InputController2* InputManager::createInputController2(const UString& name)
{
	if (getInputController2(name) != nullptr) {
		VERSO_ILLEGALPARAMETERS("verso-gfx", "There's already an InputController2 with the given name", name.c_str());
	}

	InputController2* ic = new InputController2(name);
	inputControllers2[name] = ic;
	return ic;
}


InputController2* InputManager::getInputController2(const UString& name)
{
	std::map<UString, InputController2*>::const_iterator it = inputControllers2.find(name);
	if (it == inputControllers2.end()) {
		return nullptr;
	}
	else {
		return it->second;
	}
}


bool InputManager::pollEvent(Event& event)
{
	SDL_Event sdlEvent;
	sdlEvent.type = SDL_FIRSTEVENT;
	if (SDL_PollEvent(&sdlEvent)) {
		while (true) {
			if (processSdlEvent(sdlEvent, event) == true) {
				return true;
			}
			else {
				sdlEvent.type = SDL_FIRSTEVENT;
				if (!SDL_PollEvent(&sdlEvent)) {
					return false;
				}
			}
		}
	}
	return false;
}


bool InputManager::waitEvent(Event& event)
{
	(void)event;
	VERSO_FAIL("verso-gfx"); // \TODO: Implement InputManager::waitEvent(Event& event)
}


void InputManager::broadcastEvent(const Event& event)
{
	for (auto& it : inputControllers) {
		it.second->processEvent(event);
	}

	for (auto& it : inputControllers2) {
		it.second->processEvent(event);
	}
}


///////////////////////////////////////////////////////////////////////////////////////////
// Implementation specific
///////////////////////////////////////////////////////////////////////////////////////////

bool InputManager::processSdlEvent(const SDL_Event& sdlEvent, Event& event)
{
	switch (sdlEvent.type)
	{
		case SDL_QUIT:
		{
			event.type = EventType::Quit;
			broadcastEvent(event);
			return true;
		}

		case SDL_WINDOWEVENT:
		{
			switch (sdlEvent.window.event) {
				/*case SDL_WINDOWEVENT_SHOWN:
			SDL_Log("Window %d shown", sdlEvent.window.windowID);
			break;
		case SDL_WINDOWEVENT_HIDDEN:
			SDL_Log("Window %d hidden", sdlEvent.window.windowID);
			break;
		case SDL_WINDOWEVENT_EXPOSED:
			SDL_Log("Window %d exposed", sdlEvent.window.windowID);
			break;
		case SDL_WINDOWEVENT_MOVED:
			SDL_Log("Window %d moved to %d,%d", sdlEvent.window.windowID, sdlEvent.window.data1, sdlEvent.window.data2);
			break;*/
				case SDL_WINDOWEVENT_RESIZED:
				{
					//SDL_Log("Window %d resized to %dx%d", sdlEvent.window.windowID, sdlEvent.window.data1, sdlEvent.window.data2);
					event.type = EventType::Resized;
					event.resolution.x = sdlEvent.window.data1;
					event.resolution.y = sdlEvent.window.data2;
					// \TODO: needs window id matched to window! (sdl window id=sdlEvent.window.windowID)
					broadcastEvent(event);
					return true;
				}
					/*case SDL_WINDOWEVENT_MINIMIZED:
			SDL_Log("Window %d minimized", sdlEvent.window.windowID);
			break;
		case SDL_WINDOWEVENT_MAXIMIZED:
			SDL_Log("Window %d maximized", sdlEvent.window.windowID);
			break;
		case SDL_WINDOWEVENT_RESTORED:
			SDL_Log("Window %d restored", sdlEvent.window.windowID);
			break;
		case SDL_WINDOWEVENT_ENTER:
			SDL_Log("Mouse entered window %d", sdlEvent.window.windowID);
			break;
		case SDL_WINDOWEVENT_LEAVE:
			SDL_Log("Mouse left window %d", sdlEvent.window.windowID);
			break;*/
				case SDL_WINDOWEVENT_FOCUS_GAINED:
				{
					event.type = EventType::GainedFocus;
					// \TODO: needs window id matched to window! (sdl window id=sdlEvent.window.windowID)
					broadcastEvent(event);
					break;
				}
				case SDL_WINDOWEVENT_FOCUS_LOST:
				{
					event.type = EventType::LostFocus;
					// \TODO: needs window id matched to window! (sdl window id=sdlEvent.window.windowID)
					broadcastEvent(event);
					break;
				}
				case SDL_WINDOWEVENT_CLOSE:
				{
					//SDL_Log("Window %d closed", sdlEvent.window.windowID);
					event.type = EventType::Closed;
					// \TODO: needs window id matched to window! (sdl window id=sdlEvent.window.windowID)
					broadcastEvent(event);
					return true;
				}
				default:
					//SDL_Log("Window %d got unknown event %d", sdlEvent.window.windowID, sdlEvent.window.event);
					break;
			}
			break;
		}

		case SDL_KEYUP:
		case SDL_KEYDOWN:
		{
			// ignore repeated key events
			if (sdlEvent.key.repeat != 0) {
				break;
			}

			event.type = EventType::Keyboard;
			event.keyboard.type = KeyboardEventType::Key;
			event.keyboard.timestamp = sdlEvent.key.timestamp;
			event.keyboard.windowId = sdlEvent.key.windowID;
			event.keyboard.state = convertSdlToButtonState(sdlEvent.key.state);
			event.keyboard.repeat = sdlEvent.key.repeat;
			event.keyboard.key = sdlToKeySymbol(sdlEvent.key.keysym);

			//VERSO_LOG_DEBUG("verso-gfx", "SDL2 KEY event");
			//VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "type", (Uint32)sdlEvent.type);
			//VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "repeat", (bool)sdlEvent.key.repeat);
			//VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "sym", (Sint32)sdlEvent.key.keysym.sym);
			//VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "scancode", (Uint32)sdlEvent.key.keysym.scancode);
			//VERSO_LOG_DEBUG_VARIABLE("verso-gfx", "mod", (Uint16)sdlEvent.key.keysym.mod);

			broadcastEvent(event);
			return true;
		}

		case SDL_TEXTINPUT:
		{
			event.type = EventType::Keyboard;
			event.keyboard.type = KeyboardEventType::TextInput;
			event.keyboard.timestamp = sdlEvent.text.timestamp;
			event.keyboard.windowId = sdlEvent.text.windowID;

			// Only defined for type == TextInput
			// none

			// Only defined for type == TextEditing or TextInput
			event.keyboard.text = sdlEvent.text.text;

			broadcastEvent(event);
			return true;
		}

		case SDL_TEXTEDITING:
		{
			event.type = EventType::Keyboard;
			event.keyboard.type = KeyboardEventType::TextEditing;
			event.keyboard.timestamp = sdlEvent.edit.timestamp;
			event.keyboard.windowId = sdlEvent.edit.windowID;

			// Only defined for type == TextEditing
			event.keyboard.start = sdlEvent.edit.start;
			event.keyboard.length = sdlEvent.edit.length;

			// Only defined for type == TextEditing or TextInput
			event.keyboard.text = sdlEvent.edit.text;

			broadcastEvent(event);
			return true;
		}

		case SDL_MOUSEMOTION:
		{
			event.type = EventType::Mouse;

			// For all types except None
			event.mouse.type = MouseEventType::Motion;
			event.mouse.timestamp = sdlEvent.motion.timestamp;
			event.mouse.windowId = sdlEvent.motion.windowID;
			event.mouse.mouseId = sdlEvent.motion.which;
			updateSdlMouseInfoToEvent(event.mouse);
			event.mouse.x = sdlEvent.motion.x;
			event.mouse.y = sdlEvent.motion.y;

			// \TODO: input normalizing probably doesnt work

			event.mouse.xRel = mouseXNormalizer.normalize(sdlEvent.motion.x); // range in theory: signed 32-bit int
			event.mouse.yRel = mouseYNormalizer.normalize(sdlEvent.motion.y); // range in theory: signed 32-bit int

			// Only defined for type == Motion
			event.mouse.motionX = sdlEvent.motion.xrel;
			event.mouse.motionY = sdlEvent.motion.yrel;
			event.mouse.motionXRel = mouseMotionXNormalizer.normalize(sdlEvent.motion.xrel); // range in theory: signed 32-bit int
			event.mouse.motionYRel = mouseMotionYNormalizer.normalize(sdlEvent.motion.yrel); // range in theory: signed 32-bit int

			broadcastEvent(event);
			return true;
		}

		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
		{
			event.type = EventType::Mouse;

			// For all types except None
			event.mouse.type = MouseEventType::Button;
			event.mouse.timestamp = sdlEvent.button.timestamp;
			event.mouse.windowId = sdlEvent.button.windowID;
			event.mouse.mouseId = sdlEvent.button.which;
			updateSdlMouseInfoToEvent(event.mouse);
			event.mouse.x = sdlEvent.button.x;
			event.mouse.y = sdlEvent.button.y;

			// \TODO: input normalizing probably doesnt work

			event.mouse.xRel = mouseXNormalizer.normalize(sdlEvent.button.x); // range in theory: signed 32-bit int
			event.mouse.yRel = mouseYNormalizer.normalize(sdlEvent.button.y); // range in theory: signed 32-bit int

			// Only defined for type == Button
			event.mouse.changedButton = convertSdlToMouseButton(sdlEvent.button.button);
			event.mouse.state = convertSdlToButtonState(sdlEvent.button.state);
			event.mouse.clicks = sdlEvent.button.clicks;

			broadcastEvent(event);
			return true;
		}

		case SDL_MOUSEWHEEL:
		{
			event.type = EventType::Mouse;

			// For all types except None
			event.mouse.type = MouseEventType::Wheel;
			event.mouse.timestamp = sdlEvent.wheel.timestamp;
			event.mouse.windowId = sdlEvent.wheel.windowID;
			event.mouse.mouseId = sdlEvent.wheel.which;
			updateSdlMouseInfoToEvent(event.mouse);

			uint32_t buttonState = SDL_GetMouseState(&event.mouse.x, &event.mouse.y);
			event.mouse.mouseButtons[static_cast<int8_t>(MouseButton::Left)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_LEFT);
			event.mouse.mouseButtons[static_cast<int8_t>(MouseButton::Middle)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_MIDDLE);
			event.mouse.mouseButtons[static_cast<int8_t>(MouseButton::Right)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_RIGHT);
			event.mouse.mouseButtons[static_cast<int8_t>(MouseButton::X1)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_X1);
			event.mouse.mouseButtons[static_cast<int8_t>(MouseButton::X2)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_X2);

			// Only defined for type == Wheel
			event.mouse.wheelMotionX = sdlEvent.wheel.x;
			event.mouse.wheelMotionY = sdlEvent.wheel.y;
			event.mouse.wheelMotionPreciseX = sdlEvent.wheel.preciseX;
			event.mouse.wheelMotionPreciseY = sdlEvent.wheel.preciseY;
			event.mouse.wheelMotionXRel = mouseWheelMotionXNormalizer.normalize(sdlEvent.wheel.x); // range in theory: signed 32-bit int
			event.mouse.wheelMotionYRel = mouseWheelMotionYNormalizer.normalize(sdlEvent.wheel.y); // range in theory: signed 32-bit int

			broadcastEvent(event);
			return true;
		}

		case SDL_CONTROLLERAXISMOTION:
		{
			event.type = EventType::GameController;
			event.gameController.type = GameControllerEventType::AxisMotion;
			event.gameController.timestamp = sdlEvent.caxis.timestamp;
			event.gameController.joystickId = sdlEvent.caxis.which;

			// Only defined for type == AxisMotion
			event.gameController.axis = convertSdlToGameControllerAxis(sdlEvent.caxis.axis);
			event.gameController.value = gameControllerNormalizer.normalize(sdlEvent.caxis.value); // range: -32768 to 32767

			broadcastEvent(event);
			return true;
		}

		case SDL_CONTROLLERBUTTONDOWN:
		case SDL_CONTROLLERBUTTONUP:
		{
			event.type = EventType::GameController;
			event.gameController.type = GameControllerEventType::Button;
			event.gameController.timestamp = sdlEvent.cbutton.timestamp;
			event.gameController.joystickId = sdlEvent.cbutton.which;

			// Only defined for type == Button
			event.gameController.button = convertSdlToGameControllerButton(sdlEvent.cbutton.button);
			event.gameController.state = convertSdlToButtonState(sdlEvent.cbutton.state);

			broadcastEvent(event);
			return true;
		}

		case SDL_JOYAXISMOTION:
		{
			event.type = EventType::Joystick;
			event.joystick.type = JoystickEventType::AxisMotion;
			event.joystick.timestamp = sdlEvent.jaxis.timestamp;
			event.joystick.joystickId = sdlEvent.jaxis.which;

			// Only defined for type == AxisMotion
			event.joystick.axis = convertSdlToJoystickAxis(sdlEvent.jaxis.axis);
			event.joystick.value = joystickAxisNormalizer.normalize(sdlEvent.jaxis.value); // range: -32768 to 32767

			broadcastEvent(event);
			return true;
		}

		case SDL_JOYBALLMOTION:
		{
			event.type = EventType::Joystick;
			event.joystick.type = JoystickEventType::TrackballMotion;
			event.joystick.timestamp = sdlEvent.jball.timestamp;
			event.joystick.joystickId = sdlEvent.jball.which;

			// Only defined for type == TrackballMotion
			event.joystick.trackball = convertSdlToJoystickTrackball(sdlEvent.jball.ball);
			event.joystick.xRel = joystickTrackballNormalizer.normalize(sdlEvent.jball.xrel); // range in theory: -32768 to 32767
			event.joystick.yRel = joystickTrackballNormalizer.normalize(sdlEvent.jball.yrel); // range in theory: -32768 to 32767

			broadcastEvent(event);
			return true;
		}

		case SDL_JOYHATMOTION:
		{
			event.type = EventType::Joystick;
			event.joystick.type = JoystickEventType::HatMotion;
			event.joystick.timestamp = sdlEvent.jhat.timestamp;
			event.joystick.joystickId = sdlEvent.jhat.which;

			// Only defined for type == HatMotion
			event.joystick.hat = convertSdlToJoystickHat(sdlEvent.jhat.hat);
			event.joystick.hatDirection = convertSdlToJoystickHatDirection(sdlEvent.jhat.value);

			broadcastEvent(event);
			return true;
		}

		case SDL_JOYBUTTONDOWN:
		case SDL_JOYBUTTONUP:
		{
			event.type = EventType::Joystick;
			event.joystick.type = JoystickEventType::Button;
			event.joystick.timestamp = sdlEvent.jbutton.timestamp;
			event.joystick.joystickId = sdlEvent.jbutton.which;

			// Only defined for type == Button
			event.joystick.button = convertSdlToJoystickButton(sdlEvent.jbutton.button);
			event.joystick.state = convertSdlToButtonState(sdlEvent.jbutton.state);

			broadcastEvent(event);
			return true;
		}

			// \TODO: SDL_HINT_MAC_CTRL_CLICK_EMULATE_RIGHT_CLICK
			// \TODO: Add support for this? Some kind of callback event to input system user to map relevant controls.
			//case SDL_CONTROLLERDEVICEADDED: // SDL_ControllerDeviceEvent cdevice
			//case SDL_CONTROLLERDEVICEREMOVED:
			//case SDL_CONTROLLERDEVICEREMAPPED:
			//{
			//	event.type = EventType::GAMECONTROLLER;
			//	int unfinished_case_branch = 1;
			//	broadcastEvent(event);
			//	return true;
			//}
	}

	return false;
}


///////////////////////////////////////////////////////////////////////////////////////////
// Static
///////////////////////////////////////////////////////////////////////////////////////////

KeySymbol InputManager::sdlToKeySymbol(const SDL_Keysym& sdlKeySym)
{
	KeyCodeConverterSdl2& kcc = KeyCodeConverterSdl2::instance();
	return KeySymbol(kcc.sdlToVersoKeyCode(sdlKeySym.sym),
					 kcc.sdlToVersoKeyScancode(sdlKeySym.scancode),
					 kcc.sdlToVersoKeymodifiers(sdlKeySym.mod));
}


ButtonState InputManager::convertSdlToButtonState(Uint8 state)
{
	switch (state) {
		case SDL_RELEASED:
			return ButtonState::Released;

		case SDL_PRESSED:
			return ButtonState::Pressed;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid Uint8 state="<<state<<" given.");
			return ButtonState::Unset;
		}
	}
}


MouseButton InputManager::convertSdlToMouseButton(Uint8 button)
{
	switch (button) {
		case SDL_BUTTON_LEFT: return MouseButton::Left;
		case SDL_BUTTON_MIDDLE: return MouseButton::Middle;
		case SDL_BUTTON_RIGHT: return MouseButton::Right;
		case SDL_BUTTON_X1: return MouseButton::X1;
		case SDL_BUTTON_X2: return MouseButton::X2;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid Uint8 button="<<button<<" given.");
			return MouseButton::Unset;
		}
	}
}


void InputManager::updateSdlMouseInfoToEvent(MouseEvent& mouse) const
{
	// \TODO: input normalizing probably doesnt work

	uint32_t buttonState = SDL_GetMouseState(&mouse.x, &mouse.y);
	mouse.xRel = mouseXNormalizer.normalize(mouse.x); // range in theory: signed 32-bit int
	mouse.yRel = mouseYNormalizer.normalize(mouse.y); // range in theory: signed 32-bit int

	mouse.mouseButtons[static_cast<int8_t>(MouseButton::Left)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_LEFT);
	mouse.mouseButtons[static_cast<int8_t>(MouseButton::Middle)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_MIDDLE);
	mouse.mouseButtons[static_cast<int8_t>(MouseButton::Right)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_RIGHT);
	mouse.mouseButtons[static_cast<int8_t>(MouseButton::X1)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_X1);
	mouse.mouseButtons[static_cast<int8_t>(MouseButton::X2)] = convertSdlToMouseButtonState(buttonState, SDL_BUTTON_X2);
}


ButtonState InputManager::convertSdlToMouseButtonState(uint32_t sdlButtonState, uint8_t sdlButton)
{
	if (sdlButtonState & SDL_BUTTON(sdlButton)) {
		return ButtonState::Pressed;
	}
	else {
		return ButtonState::Released;
	}
}


GameControllerAxis InputManager::convertSdlToGameControllerAxis(Uint8 axis)
{
	switch (axis) {
		case SDL_CONTROLLER_AXIS_LEFTX: return GameControllerAxis::LeftX;
		case SDL_CONTROLLER_AXIS_LEFTY: return GameControllerAxis::LeftY;
		case SDL_CONTROLLER_AXIS_RIGHTX: return GameControllerAxis::RightX;
		case SDL_CONTROLLER_AXIS_RIGHTY: return GameControllerAxis::RightY;
		case SDL_CONTROLLER_AXIS_TRIGGERLEFT: return GameControllerAxis::TriggerLeft;
		case SDL_CONTROLLER_AXIS_TRIGGERRIGHT: return GameControllerAxis::TriggerRight;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid axis="<<axis<<" given.");
			return GameControllerAxis::Unset;
		}
	}
}


GameControllerButton InputManager::convertSdlToGameControllerButton(Uint8 button)
{
	switch (button) {
		case SDL_CONTROLLER_BUTTON_A: return GameControllerButton::A;
		case SDL_CONTROLLER_BUTTON_B: return GameControllerButton::B;
		case SDL_CONTROLLER_BUTTON_X: return GameControllerButton::X;
		case SDL_CONTROLLER_BUTTON_Y: return GameControllerButton::Y;
		case SDL_CONTROLLER_BUTTON_BACK: return GameControllerButton::Back;
		case SDL_CONTROLLER_BUTTON_GUIDE: return GameControllerButton::Guide;
		case SDL_CONTROLLER_BUTTON_START: return GameControllerButton::Start;
		case SDL_CONTROLLER_BUTTON_LEFTSTICK: return GameControllerButton::LeftStick;
		case SDL_CONTROLLER_BUTTON_RIGHTSTICK: return GameControllerButton::RightStick;
		case SDL_CONTROLLER_BUTTON_LEFTSHOULDER: return GameControllerButton::LeftShoulder;
		case SDL_CONTROLLER_BUTTON_RIGHTSHOULDER: return GameControllerButton::RightShoulder;
		case SDL_CONTROLLER_BUTTON_DPAD_UP: return GameControllerButton::DpadUp;
		case SDL_CONTROLLER_BUTTON_DPAD_DOWN: return GameControllerButton::DpadDown;
		case SDL_CONTROLLER_BUTTON_DPAD_LEFT: return GameControllerButton::DpadLeft;
		case SDL_CONTROLLER_BUTTON_DPAD_RIGHT: return GameControllerButton::DpadRight;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid button="<<button<<" given.");
			return GameControllerButton::Unset;
		}
	}
}


JoystickAxis InputManager::convertSdlToJoystickAxis(Uint8 axis)
{
	switch (axis) {
		case 0: return JoystickAxis::Axis1;
		case 1: return JoystickAxis::Axis2;
		case 2: return JoystickAxis::Axis3;
		case 3: return JoystickAxis::Axis4;
		case 4: return JoystickAxis::Axis5;
		case 5: return JoystickAxis::Axis6;
		case 6: return JoystickAxis::Axis7;
		case 7: return JoystickAxis::Axis8;
		case 8: return JoystickAxis::Axis9;
		case 9: return JoystickAxis::Axis10;
		case 10: return JoystickAxis::Axis11;
		case 11: return JoystickAxis::Axis12;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid axis="<<axis<<" given.");
			return JoystickAxis::Unset;
		}
	}
}


JoystickTrackball InputManager::convertSdlToJoystickTrackball(Uint8 trackball)
{
	switch (trackball) {
		case 0: return JoystickTrackball::Trackball1;
		case 1: return JoystickTrackball::Trackball2;
		case 2: return JoystickTrackball::Trackball3;
		case 3: return JoystickTrackball::Trackball4;
		case 4: return JoystickTrackball::Trackball5;
		case 5: return JoystickTrackball::Trackball6;
		case 6: return JoystickTrackball::Trackball7;
		case 7: return JoystickTrackball::Trackball8;
		case 8: return JoystickTrackball::Trackball9;
		case 9: return JoystickTrackball::Trackball10;
		case 10: return JoystickTrackball::Trackball11;
		case 11: return JoystickTrackball::Trackball12;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid trackball="<<trackball<<" given.");
			return JoystickTrackball::Unset;
		}
	}
}


JoystickHat InputManager::convertSdlToJoystickHat(Uint8 hat)
{
	switch (hat) {
		case 0: return JoystickHat::Hat1;
		case 1: return JoystickHat::Hat2;
		case 2: return JoystickHat::Hat3;
		case 3: return JoystickHat::Hat4;
		case 4: return JoystickHat::Hat5;
		case 5: return JoystickHat::Hat6;
		case 6: return JoystickHat::Hat7;
		case 7: return JoystickHat::Hat8;
		case 8: return JoystickHat::Hat9;
		case 9: return JoystickHat::Hat10;
		case 10: return JoystickHat::Hat11;
		case 11: return JoystickHat::Hat12;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid hat="<<hat<<" given.");
			return JoystickHat::Unset;
		}
	}
}


JoystickHatDirection InputManager::convertSdlToJoystickHatDirection(Uint8 hatDirection)
{
	switch (hatDirection) {
		case SDL_HAT_LEFTUP: return JoystickHatDirection::LeftUp;
		case SDL_HAT_UP: return JoystickHatDirection::Up;
		case SDL_HAT_RIGHTUP: return JoystickHatDirection::RightUp;
		case SDL_HAT_LEFT: return JoystickHatDirection::Left;
		case SDL_HAT_CENTERED: return JoystickHatDirection::Centered;
		case SDL_HAT_RIGHT: return JoystickHatDirection::Right;
		case SDL_HAT_LEFTDOWN: return JoystickHatDirection::LeftDown;
		case SDL_HAT_DOWN: return JoystickHatDirection::Down;
		case SDL_HAT_RIGHTDOWN: return JoystickHatDirection::RightDown;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid hatDirection="<<hatDirection<<" given.");
			return JoystickHatDirection::Unset;
		}
	}
}


JoystickButton InputManager::convertSdlToJoystickButton(Uint8 button)
{
	switch (button) {
		case 0: return JoystickButton::Button1;
		case 1: return JoystickButton::Button2;
		case 2: return JoystickButton::Button3;
		case 3: return JoystickButton::Button4;
		case 4: return JoystickButton::Button5;
		case 5: return JoystickButton::Button6;
		case 6: return JoystickButton::Button7;
		case 7: return JoystickButton::Button8;
		case 8: return JoystickButton::Button9;
		case 9: return JoystickButton::Button10;
		case 10: return JoystickButton::Button11;
		case 11: return JoystickButton::Button12;
		case 12: return JoystickButton::Button13;
		case 13: return JoystickButton::Button14;
		case 14: return JoystickButton::Button15;
		case 15: return JoystickButton::Button16;
		case 16: return JoystickButton::Button17;
		case 17: return JoystickButton::Button18;
		case 18: return JoystickButton::Button19;
		case 19: return JoystickButton::Button20;
		case 20: return JoystickButton::Button21;
		case 21: return JoystickButton::Button22;
		case 22: return JoystickButton::Button23;
		case 23: return JoystickButton::Button24;
		case 24: return JoystickButton::Button25;
		case 25: return JoystickButton::Button26;
		case 26: return JoystickButton::Button27;
		case 27: return JoystickButton::Button28;
		case 28: return JoystickButton::Button29;
		case 29: return JoystickButton::Button30;

		default:
		{
			VERSO_LOG_WARN("verso-gfx", "Invalid button="<<button<<" given.");
			return JoystickButton::Unset;
		}
	}
}


} // End namespace Verso

