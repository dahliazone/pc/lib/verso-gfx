#!/bin/bash

echo "verso-gfx: Fetching externals from repositories..."

if [ ! -d ../SDL2 ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/SDL2.git ../SDL2
else
        pushd ../SDL2
        git pull --rebase
        popd
fi

if [ ! -d ../BASS ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/BASS.git ../BASS
else
        pushd ../BASS
        git pull --rebase
        popd
fi

if [ ! -d ../rtmidi ]; then
        git clone https://gitlab.com/dahliazone/pc/extlib/rtmidi.git ../rtmidi
else
        pushd ../rtmidi
        git pull --rebase
        popd
fi

if [ ! -d ../verso-base ]; then
        git clone https://gitlab.com/dahliazone/pc/lib/verso-base.git ../verso-base
else
        pushd ../verso-base
        git pull --rebase
        popd
fi

echo "verso-base: Resursing into..."
pushd ../verso-base
./fetch_parent_externals.sh
popd

echo "verso-gfx: All done"

